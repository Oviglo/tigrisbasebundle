import TigrisEvents from './tigris-events'
/**
 * https://gist.github.com/ziggi/2f15832b57398649ee9b
 * @param {*} selector 
 * @returns 
 */
Element.prototype.parents = function (selector) {
    var elements = [];
    var elem = this;
    var ishaveselector = selector !== undefined;

    while ((elem = elem.parentElement) !== null) {
        if (elem.nodeType !== Node.ELEMENT_NODE) {
            continue;
        }

        if (!ishaveselector || elem.matches(selector)) {
            elements.push(elem);
        }
    }

    return elements;
};

/* =============================
    STATS
================================ */
document.addEventListener(TigrisEvents.VUE_APP_MOUNTED, () => {
    const tabs = document.querySelectorAll('#statistics [data-bs-toggle="tab"]');
    for (const tab of tabs) {
        tab.addEventListener('shown.bs.tab', e => {
            const cur = document.querySelector(e.target.getAttribute('data-bs-target'));
            const event = new CustomEvent("tigris:chart-load", { detail: { name: cur.getAttribute('name') } });
            document.dispatchEvent(event);
        })
    }
})