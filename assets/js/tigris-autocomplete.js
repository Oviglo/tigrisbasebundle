import {debounce} from '../ts/utils';

export class TigrisAutocomplete {

    constructor(query, route, attrName = 'q') {
        this.input = document.querySelector(query);
        this.list = document.createElement('ul');
        this.list.classList.add('autocomplete-list');
        this.url = Routing.generate(route);
        this.onCallFunc = null;
        this.onClickItemFunc = null;
        this.result = [];

        this.input.closest('div').style.position = 'relative';
        this.input.closest('div').appendChild(this.list);
        this.list.style.display = 'none';

        if (this.input) {
            this.input.addEventListener("input", debounce((e) => {
                let xhttp = new XMLHttpRequest;
                xhttp.open('GET', this.url + '?' + attrName + '=' + e.target.value);
                xhttp.onload = (e) => {
                    this.list.innerHTML = '';
                    let json = JSON.parse(e.currentTarget.response);
                    if (typeof this.onCallFunc == 'function') {
                        this.result = this.onCallFunc(json);
                        this.list.style.display = 'block';
                        this.result.forEach((item) => {
                            this.addItem(item);
                        });
                    }
                }
                xhttp.send();

            }, 500));

            this.input.addEventListener('blur', (e) => {
                setTimeout(() => {
                    this.list.style.display = 'none';
                }, 500);

            });

            this.input.addEventListener('focus', (e) => {
                this.list.style.display = 'block';
            });
        }
    }

    onCall(callback) {
        this.onCallFunc = callback;
    }

    onClickItem(callback) {
        this.onClickItemFunc = callback;
    }

    addItem(item) {
        let listItem = document.createElement('li');
        if (typeof item === 'string') {
            listItem.innerHTML = item;
        } else if (typeof item === 'object') {
            listItem.innerHTML = item.value;
            listItem.data = item.data;
        }

        listItem.addEventListener('click', (e) => {
            this.input.value = listItem.innerHTML;
            if (typeof this.onClickItemFunc == 'function') {
                this.onClickItemFunc(listItem);
            }
        });

        this.list.appendChild(listItem);
    }
}