import tinymce from 'tinymce';
import TigrisEvents from '../tigris-events'
import 'tinymce/icons/default';

/* A theme is also required */
import 'tinymce/themes/silver';
import 'tinymce/models/dom/model'

import 'tinymce-i18n/langs/fr_FR'

/* Import plugins */


import { TINY_MCE_TOOLS_MAXI, TINY_MCE_TOOLS_MINI } from './tinymce-tools';

import contentCss from '!!raw-loader!tinymce/skins/content/default/content.min.css';
import contentDarkCss from '!!raw-loader!tinymce/skins/content/dark/content.min.css';
import contentUiCss from '!!raw-loader!tinymce/skins/ui/oxide/content.min.css';

export function init(element) {
    let tools = element.getAttribute('data-tools');

    let toolbar = ""
    let menu = {}
    let menubar = ""
    let quickBars = ""
    let plugins = ""
    let contextMenu = ""
    let contextToolbar = ""

    switch (tools) {
        case 'maxi':
            toolbar = TINY_MCE_TOOLS_MAXI.toolbar;
            menu = TINY_MCE_TOOLS_MAXI.menu;
            menubar = TINY_MCE_TOOLS_MAXI.menubar;
            quickBars = TINY_MCE_TOOLS_MAXI.quickBars;
            plugins = TINY_MCE_TOOLS_MAXI.plugins;
            contextMenu = TINY_MCE_TOOLS_MAXI.contextMenu;
            contextToolbar = TINY_MCE_TOOLS_MAXI.contextToolbar;

            break;
        case 'mini':
            toolbar = TINY_MCE_TOOLS_MINI.toolbar;
            menu = TINY_MCE_TOOLS_MINI.menu;
            menubar = TINY_MCE_TOOLS_MINI.menubar;
            quickBars = TINY_MCE_TOOLS_MINI.quickBars;
            plugins = TINY_MCE_TOOLS_MINI.plugins;
            contextMenu = TINY_MCE_TOOLS_MINI.contextMenu;
            contextToolbar = TINY_MCE_TOOLS_MINI.contextToolbar;

            break;
    }

    function initIntance() {

        tinymce.init({
            target: element,
            plugins: plugins,
            toolbar: toolbar,
            language: 'fr_FR',
            menu: menu,
            menubar: menubar,
            skin: false,
            content_css: false,
            content_style: [(document.querySelector('html').getAttribute('data-bs-theme') === 'dark' ? contentDarkCss : contentCss), contentUiCss].join('\n'),
            toolbar_sticky: true,
            image_caption: true,
            quickbars_selection_toolbar: quickBars,
            browser_spellcheck: true,
            contextmenu: contextMenu,
            contexttoolbar: contextToolbar,
            promotion: false,
            inline_boundaries: true,
            height: (20 + (parseInt(element.getAttribute('rows')) * 10)) ?? 200,
            license_key: 'gpl',
        }).then((editors) => {
            document.addEventListener(TigrisEvents.TOGGLE_THEME, () => {
                tinymce.activeEditor.destroy();

                initIntance()
            })
            document.addEventListener(TigrisEvents.CLOSE_DIALOG, (e) => {
                if (e.detail.element.querySelector('form') === element.closest('form')) {
                    tinymce.triggerSave(true, true)
                    if (null !== tinymce.activeEditor) {
                        tinymce.activeEditor.destroy();
                    }
                }
            })
            document.addEventListener(TigrisEvents.DIALOG_FORM_SUBMIT, (e) => {
                if (e.detail.form === element.closest('form')) {
                    tinymce.triggerSave(true, true)
                }
            })

            // https://www.tiny.cloud/docs/integrations/bootstrap/#usingtinymceinabootstrapdialog
            document.addEventListener('focusin', (e) => {
                if (e.target.closest(".tox-tinymce-aux, .moxman-window, .tam-assetmanager-root") !== null) {
                    e.stopImmediatePropagation();
                }
            });
        })
    }

    initIntance()
}