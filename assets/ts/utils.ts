import debounce from './util/debounce'
import throttle from './util/throttle'
import capitalize from './util/capitalize'
import parseIcons from './util/parseIcons'
import truncate from './util/truncate'
import formatDate from './util/formatDate'

module.exports = {
    debounce,
    throttle,
    capitalize,
    parseIcons,
    truncate,
    formatDate
}

export {
    debounce,
    throttle,
    capitalize,
    parseIcons,
    truncate,
    formatDate
}