import Translator from "./component/translator";

const translator = new Translator();
translator.initTranslation();

declare global {
    interface Window {
        translator: Translator;
    }
}

Window.prototype.translator = translator;