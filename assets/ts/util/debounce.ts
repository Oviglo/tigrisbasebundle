export default function debounce(fn: Function, wait: number): Function
{
    let timer: null|NodeJS.Timeout = null;
    return function(this: unknown, ...args: any[]){
        if(timer) {
            clearTimeout(timer); // clear any pre-existing timer
        }
        timer = setTimeout(()=>{
            fn.apply(this, args); // call the function if time expires
        }, wait);
    }
}