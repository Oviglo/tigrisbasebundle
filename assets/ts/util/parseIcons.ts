export default function parseIcons(value: string, fixWidth: boolean = false, iconStyle: string = ''): string 
{
    const regex: RegExp = /\.([a-z]+)-([a-z0-9+-]+)/;

    return value.replace(regex, function (matche: string, style: string, type: string): string {
        let icon: string = '';

        if (style == 'icon') {
            icon = 'fa' + iconStyle + ' fa-' + type;
        } else if (style == 'brand') {
            icon = 'fab fa-' + type;
        }

        return '<i class="' + icon + (fixWidth ? ' fa-fw' : '') + '"></i> ';
    });
}