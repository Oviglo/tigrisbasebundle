import moment from 'moment';
moment.locale('fr');

export default function formatDate(value: string, format: string = 'LLL', fromNow: boolean = false, fromNowMaxDays: number = 5): string {
    const dateValue = moment(value);
    const dateNow = moment();
    const nbDays = Math.abs(parseInt(String(dateValue.diff(dateNow, 'days'))));
    if ((format == 'fromNow' || fromNow) && nbDays <= fromNowMaxDays) {
        return dateValue.fromNow();
    }

    if (format == "fromNow") {
        format = "LLL";
    }

    return dateValue.format(format);
}