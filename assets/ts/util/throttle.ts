export default function throttle(fn: Function, wait: number): Function 
{
    let throttled: Boolean = false;
    return function(this: unknown, ...args: any[]){
        if(!throttled){
            fn.apply(this,args);
            throttled = true;
            setTimeout(()=>{
                throttled = false;
            }, wait);
        }
    }
}