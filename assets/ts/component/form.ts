import TigrisFormCollection from '../../js/tigris-form-collection';
import TigrisWysiwyg from '../../js/tigris-wysiwyg';
import TigrisFileUploadType from '../../js/tigris-file-upload-type';
import SlimSelect from 'slim-select';
import Inputmask from "inputmask";
import { AlertMessage } from './alert-message';
import {TigrisEvents} from '../enum/tigris-events';

interface FormError {
    field: HTMLElement;
    message: HTMLElement;
}

export class Form {
    private DOMElement: HTMLElement;
    private name: string;

    constructor(parentElement: HTMLElement) {
        this.DOMElement = parentElement;
        this.name = this.DOMElement.getAttribute('name') || '';
    }

    public init(): void {
        this.initCollection();
        this.initWysiwyg();
        this.initSelect();
        this.initInputMask();
        this.initFileSelect();

        // Init ajax form
        if (this.DOMElement.getAttribute('data-ajax')) {
            this.initAjaxForm();
        }

        document.dispatchEvent(new CustomEvent(TigrisEvents.FORM_LOADED, { detail: { form: this.DOMElement } }));
    }

    private initCollection(): void {
        const collection = this.DOMElement.querySelectorAll('[data-collection]');
        collection.forEach((element) => {
            const collection = new TigrisFormCollection(element);
            collection.init();
        });
    }

    private initWysiwyg(): void {
        const wysiwygFactory = new TigrisWysiwyg(this.DOMElement);
        wysiwygFactory.load();
    }

    private initSelect(): void {
        const selects = <NodeListOf<HTMLInputElement>>this.DOMElement.querySelectorAll('select.form-control, select.form-select');
        for (const selectElement of selects) {
            const placeholder = selectElement.getAttribute('data-none-selected-text') || '';
            new SlimSelect({
                select: selectElement,
                settings: {
                    showSearch: selectElement.querySelectorAll('option').length > 19,
                    placeholderText: placeholder,
                },
                events: {
                    afterChange: () => {
                        selectElement.dispatchEvent(new Event('change'))
                    }
                }
            });
            selectElement.classList.remove('form-control');
            selectElement.classList.remove('form-select');
        }
    }

    private initInputMask(): void {
        Inputmask().mask(document.querySelectorAll("input[data-inputmask]"));
    }

    private initFileSelect(): void {
        const elements = this.DOMElement.getElementsByClassName('fileupload-control');

        for (const element of elements) {
            new TigrisFileUploadType(element).init();
        }
    }

    private initAjaxForm(): void {
        const submitButton = this.DOMElement.querySelector('button[type="submit"]');
        const replaceByResponse = this.DOMElement.getAttribute('replace-by-response');
        if (submitButton && this.DOMElement) {
            this.DOMElement.addEventListener('submit', (event) => {
                submitButton.setAttribute('disabled', 'disabled');
                event.preventDefault();

                const formData = new FormData(<HTMLFormElement>this.DOMElement);
                const request = new Request(this.DOMElement.getAttribute('action') || '', {
                    method: this.DOMElement.getAttribute('method') || 'POST',
                    body: formData,
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest',
                    }
                });

                fetch(request)
                    .then((response) => {
                        if (response.ok) {
                            response.json().then((json) => {
                                if (replaceByResponse) {
                                    this.DOMElement.innerHTML = '';
                                }
        
                                new AlertMessage(json.message, json.status, -1).show(this.DOMElement);
        
                                submitButton.removeAttribute('disabled');
                            }); 
                        } else {
                            response.text().then((text) => {
                                this.resetFormErrors(this.DOMElement);
                                const formErrors = this.filterErrorsFromHtml(text);
                                for (const error of formErrors) {
                                    error.field.classList.add('is-invalid');
                                    error.field.insertAdjacentElement('afterend', error.message);
                                }
                                submitButton.removeAttribute('disabled');
                                document.dispatchEvent(new CustomEvent(TigrisEvents.SECURITY_BUTTON_RESET, { detail: { form: this.DOMElement } }));
                            });
                        }
                    })
                    .catch((error) => {
                        console.error('Error:', error);
                    });

                return false;
            });
        }
    }

    /**
     * Get html string error messages and return an array of FormError
     * 
     * @param html 
     * @returns 
     */
    private filterErrorsFromHtml(html: string): FormError[] {
        const parser = new DOMParser();
        const doc = parser.parseFromString(html, 'text/html');
        const errors = doc.querySelectorAll('.invalid-feedback');
        const errorMessages: FormError[] = [];
        for (const error of errors) {
            const parent = error.parentElement;
            if (null !== parent) {
                const forAttr = parent.getAttribute('for');
                if (null !== forAttr) {
                    errorMessages.push({
                        field: document.querySelector('#' + forAttr) || parent,
                        message: <HTMLElement>error,
                    });
                }
            }
        }

        return errorMessages;
    }

    /**
     * Remove all errors in a form
     * 
     * @param form 
     */
    private resetFormErrors(form: HTMLElement): void {
        const errors = form.querySelectorAll('.is-invalid');
        for (const error of errors) {
            error.classList.remove('is-invalid');
        }
        const errorMessages = form.querySelectorAll('.invalid-feedback');
        for (const errorMessage of errorMessages) {
            errorMessage.remove();
        }
    }
}