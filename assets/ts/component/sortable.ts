enum SortableAxis {
    X = 'x',
    Y = 'y',
    BOTH = 'both',
}

interface SortableOptions {
    onSort: null|((event: CustomEvent) => void);
    itemSelector: string;
    axis: SortableAxis;
    dragClass: string;
    itemClass: string;
}

export default class Sortable {

    private element: HTMLElement;
    private options: SortableOptions = {
        onSort: null,
        itemSelector: ':scope > div',
        axis: SortableAxis.BOTH,
        dragClass: 'dragging',
        itemClass: 'sortable-item',
    };
    private listItems: NodeListOf<HTMLElement>;
    private draggingItem: HTMLElement|null = null;
    private observer: MutationObserver|null = null;

    public constructor(element: HTMLElement, options: null|SortableOptions) {
        this.element = element;
        if (options !== null) {
            this.options = {...this.options, ...options};
        }

        this.listItems = this.element.querySelectorAll(this.options.itemSelector);

        this.observer = new MutationObserver(() => {
            this.domUpdated();
        });

        this.observer.observe(this.element, {attributes: false, childList: true, subtree: true});
        this.domUpdated();

        // Init drag events
        for (const listItem of this.listItems){
            listItem.addEventListener('drag', (event: DragEvent) => {
                event.stopPropagation();
            });

            listItem.addEventListener('dragstart', (event: DragEvent) => {
                event.stopPropagation();
                this.draggingItem = event.target as HTMLElement;
                // Limit the movement
                if (event.dataTransfer) {
                    event.dataTransfer.effectAllowed = 'move';
                    event.dataTransfer.setData('text/html', this.draggingItem.innerHTML);

                    this.element.addEventListener('dragover', (event: DragEvent) => this.onDragOver(event));
                    this.element.addEventListener('dragend', (event: DragEvent) => this.onDragEnd(event));
                }
                
            });
        }

    }

    public domUpdated() {
        this.listItems = this.element.querySelectorAll(this.options.itemSelector);
        for(const item of this.listItems) {
            if(!item.draggable) {
                item.draggable = true;
                item.classList.add(this.options.itemClass);
            }
        }
    }

    public onDragOver(event: DragEvent): void {
        event.preventDefault();
        event.stopPropagation();

        let target = event.target as HTMLElement;
        if (!this.draggingItem || !target || !this.isValidItem(target)) {
            return;
        }

        const middleX = this.getElementHorizontalCenter(target);
        const middleY = this.getElementVerticalCenter(target);
        const offset = this.getMouseOffset(event);

        if (this.options.axis === SortableAxis.X || this.options.axis === SortableAxis.BOTH) {
            if (offset.x > middleX) {
                target.parentNode?.insertBefore(this.draggingItem, target.nextSibling);
            } else {
                target.parentNode?.insertBefore(this.draggingItem, target);
            }
        } else if (this.options.axis === SortableAxis.Y || this.options.axis === SortableAxis.BOTH) {
            if (offset.y > middleY) {
                target.parentNode?.insertBefore(this.draggingItem, target.nextSibling);
            } else {
                target.parentNode?.insertBefore(this.draggingItem, target);
            }
        } else {
            return;
        }

        this.draggingItem?.classList.add(this.options.dragClass);
    }

    public onDragEnd(event: DragEvent): void {
        event.preventDefault();
        event.stopPropagation();

        this.draggingItem?.classList.remove(this.options.dragClass);

        this.element.removeEventListener('dragover', this.onDragOver);
        this.element.removeEventListener('dragend', this.onDragEnd);

        if (this.options.onSort) {
            this.options.onSort(new CustomEvent('sort'));
        }
    }

    private isValidItem(item: HTMLElement): boolean {
        for (const listItem of this.listItems) {
            if (listItem.isEqualNode(item) && item.parentNode && this.draggingItem && item.parentNode.isEqualNode(this.draggingItem.parentNode)) {
                return true;
            }
        }

        return false;
    }

    private getElementVerticalCenter(element: HTMLElement): number {
        const rect = element.getBoundingClientRect();

        return (rect.bottom - rect.top) / 2;
    }

    private getElementHorizontalCenter(element: HTMLElement): number {
        const rect = element.getBoundingClientRect();

        return (rect.right - rect.left) / 2;
    }

    private getMouseOffset(evt: MouseEvent): {x: number, y: number} {
        const target = evt.target as HTMLElement;
        if (!target) {
            return {x: 0, y: 0};
        }

        const targetRect = target.getBoundingClientRect();
        const offset = {
            x: evt.pageX - targetRect.left,
            y: evt.pageY - targetRect.top
        }

        return offset;
    }
}