import Alert from "bootstrap/js/dist/alert";

export const AlertType = {
    SUCCESS: 'success',
    DANGER: 'danger',
    WARNING: 'warning',
    INFO: 'info',
    ERROR: 'error'
} as const

type Type = typeof AlertType[keyof typeof AlertType];

export class AlertMessage {
    private duration: number;
    private DOMElement: HTMLElement;
    private closeButton: HTMLButtonElement;
    private alert: null|Alert;

    constructor(message: string, type: Type, duration: number = 5000) {
        this.duration = duration;

        this.DOMElement = document.createElement('div');
        this.DOMElement.classList.add('alert', 'alert-js', 'alert-dismissible', 'fade', 'in', 'mt-5');
        this.DOMElement.style.zIndex = '9999';
        this.DOMElement.setAttribute('role', 'alert');
        this.DOMElement.innerHTML = message;
        switch (type) {
            case AlertType.SUCCESS:
                this.DOMElement.classList.add('alert-success');
                break;
            case AlertType.DANGER:
                this.DOMElement.classList.add('alert-danger');
                break;
            case AlertType.WARNING:
                this.DOMElement.classList.add('alert-warning');
                break;
            case AlertType.INFO:
                this.DOMElement.classList.add('alert-info');
                break;
            case AlertType.ERROR:
                this.DOMElement.classList.add('alert-danger');
        }

        this.closeButton = document.createElement('button');
        this.closeButton.setAttribute('type', 'button');
        this.closeButton.setAttribute('data-bs-dismiss', 'alert');
        this.closeButton.setAttribute('aria-label', 'close');
        this.closeButton.className = 'btn-close';
        this.DOMElement.appendChild(this.closeButton);

        this.alert = null;
    }

    show(container: null|HTMLElement = null): void {
        if (container === null) {
            container = document.getElementById('alert');
            if (null === container) {
                container = document.body
                this.DOMElement.classList.add('fixed-top');
            }
            
        }
        container.appendChild(this.DOMElement)
        this.alert = new Alert(this.DOMElement);
        if (this.duration > 0) {
            setTimeout(() => {
                this.alert?.close();
            }, this.duration);
        }
        
    }

    life() {
        this.show();
    }
}