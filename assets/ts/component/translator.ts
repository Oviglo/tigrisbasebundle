import messages from '../../../translations/messages.fr.json';
import * as BazingaTranslator from 'bazinga-translator';

interface Translation {
    locale: string,
    translations: {
        [key: string]: string|object
    },
}

interface Translations {
    [key: string]: string|Translations
}

export default class Translator
{
    public initTranslation(): void {
        this.addTranslationJson(messages);
    }

    private addTranslationJson(translation: Translation): void {
        const translations = translation.translations
        const locale = translation.locale
        for (const key in translations) {
            if (Object.hasOwnProperty.call(translations, key)) {
                const element = translations[key] as Translations;

                this.addTranslation(key, element, locale);
            }
        }
    }

    public addTranslation(key: string, trans: string|Translations, locale: string): void {
        if (locale === null) {
            locale = "fr";
        }

        if (typeof trans === 'string') {
            BazingaTranslator.add(key, trans, locale);
            return
        }

        for (const subKey in trans) {
            if (Object.hasOwnProperty.call(trans, subKey)) {
                const element = trans[subKey];
                this.addTranslation(key + '.' + subKey, element, locale)
            }
        }
    }

    public trans(str: string, params: object = {}): string {
        return BazingaTranslator.trans(str, params);
    }
        
}