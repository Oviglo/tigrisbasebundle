import { capitalize, parseIcons, truncate, formatDate } from './../utils';

const filters = { 
    capitalize,
    truncate,
    formatDate,
    parseIcons(value: string, fixWidth: boolean = false, iconStyle: string = ''): string {
        return parseIcons(value, fixWidth, iconStyle);
    },
    currency(value: number|bigint, locale: string = "fr-FR", currency: string = "EUR") {

        return new Intl.NumberFormat(locale, { style: 'currency', currency: currency }).format(value);
    },

    textColor(hexcolor: string): string {
        let r = parseInt(hexcolor.substr(0, 2), 16);
        let g = parseInt(hexcolor.substr(2, 2), 16);
        let b = parseInt(hexcolor.substr(4, 2), 16);
        let yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;

        return (yiq >= 128) ? '#000' : '#fff';
    },
    toFixed(value: string, nbDigid: number): string {
        return Number.parseFloat(value).toFixed(nbDigid);
    },

    trans(value: string, params: any = {}): string {
        return window.translator.trans(value, params)
    }
};

export default filters;