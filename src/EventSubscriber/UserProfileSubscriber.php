<?php

namespace Tigris\BaseBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\UserProfileEvent;
use Tigris\BaseBundle\UserProfile\UserProfileTab;

class UserProfileSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::USER_PROFILE_SHOW => 'onUserProfileShow',
        ];
    }

    public function onUserProfileShow(UserProfileEvent $event): void
    {
        $tab = new UserProfileTab('main', '@TigrisBase/user/_main_tab.html.twig', menuLabel: 'user_profile.tab.main');

        $event->addTab($tab);
    }
}
