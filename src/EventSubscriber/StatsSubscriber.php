<?php

namespace Tigris\BaseBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\StatsEvent;
use Tigris\BaseBundle\Utils\Stats;

class StatsSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly array $configurations)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_STATS => 'onLoadStats',
        ];
    }

    public function onLoadStats(StatsEvent $event): void
    {
        if ($this->configurations['active_user_registration']) {
            $event->addStats(new Stats('menu.users', 'tigris_base_admin_user_chart'));
        }
    }
}
