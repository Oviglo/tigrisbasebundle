<?php

namespace Tigris\BaseBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\BaseBundle\Form\Type\ConfigType;

class ConfigSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_CONFIGS => 'onLoadConfigs',
        ];
    }

    public function onLoadConfigs(FormEvent $event): void
    {
        $form = $event->getForm();
        $form->add('TigrisBaseBundle', ConfigType::class, [
            'label' => 'config.base',
        ]);

        $event->setForm($form);
    }
}
