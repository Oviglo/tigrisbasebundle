<?php

namespace Tigris\BaseBundle\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\Notification;
use Tigris\BaseBundle\Event\Event;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\NotificationEvent;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Manager\NotificationManager;
use Tigris\BaseBundle\Repository\NotificationRepository;

class NotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface $translator,
        private readonly MailerInterface $mailer,
        private readonly ConfigManager $configManager,
        private readonly TokenStorageInterface $tokenStorage,
        private readonly NotificationManager $notificationManager,
        private readonly NotificationRepository $notificationRepository
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::NOTIFY => 'onNotify',
            Events::DELETE_NOTIFICATIONS => 'onDeleteNotifications',
            Events::AJAX_CRON => 'onAjaxCron',
        ];
    }

    public function onNotify(NotificationEvent $event): void
    {
        $data = $event->getData();
        $user = $event->getUser();
        $owner = $event->getOwner();

        if ($user === $owner) {
            return;
        }

        $notification = (new Notification())
            ->setUser($user)
            ->setOwner($owner)
            ->setData($data);

        $entity = $event->getEntity();
        if (is_object($entity) && method_exists($entity, 'getId')) {
            $notification->setEntityclass($entity::class)
                ->setEntityId($entity->getId())
            ;
        }

        $this->entityManager->persist($notification);
        $this->entityManager->flush();

        // noreply_mail
        $siteTitle = $this->configManager->getValue('TigrisBaseBundle.title', null);
        $notificationMail = $this->configManager->getValue('TigrisBaseBundle.noreply_mail', null);

        if ($user->isEnableNotificationEmail() && $event->isSendMail()) {
            $email = (new TemplatedEmail())
                ->from(new Address($notificationMail, $siteTitle))
                ->to(new Address($user->getEmail()))
                ->subject($this->translator->trans($data['title']))
                ->htmlTemplate('@TigrisBase/email/notification.html.twig')
                ->context([
                    'entity' => $user,
                    'data' => $data,
                ])
            ;
            $this->mailer->send($email);
        }
    }

    public function onDeleteNotifications(GenericEvent $event): void
    {
        $data = $event->getArguments();

        if (isset($data['entityClass']) && isset($data['entityId'])) {
            $notifications = $this->notificationRepository->findByEntity($data['entityClass'], $data['entityId']);

            foreach ($notifications as $notification) {
                $this->entityManager->remove($notification);
            }

            $this->entityManager->flush();
        }
    }

    public function onAjaxCron(Event $event): void
    {
        $data = $event->getData();
        $params = $event->getParams();
        $lastCronDate = $params['date'];

        $user = $this->tokenStorage->getToken()->getUser();

        $data['notifications'] = $this->notificationManager->findNew($user, $lastCronDate);

        $event->setData($data);
    }
}
