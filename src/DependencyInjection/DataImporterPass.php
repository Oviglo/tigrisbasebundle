<?php

namespace Tigris\BaseBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Tigris\BaseBundle\DataImporter\DataImporters;

class DataImporterPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        // always first check if the primary service is defined
        if (!$container->has(DataImporters::class)) {
            return;
        }

        $definition = $container->findDefinition(DataImporters::class);

        // find all service IDs with the tigris.data_importer tag
        $taggedServices = $container->findTaggedServiceIds('tigris.data_importer');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'addImporter',
                    [
                    new Reference($id),
                    $attributes['alias'], ]
                );
            }
        }
    }
}
