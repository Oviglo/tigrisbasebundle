<?php

namespace Tigris\BaseBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Tigris\BaseBundle\Reader\FileReaders;

class FileReaderPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        // always first check if the primary service is defined
        if (!$container->has(FileReaders::class)) {
            return;
        }

        $definition = $container->findDefinition(FileReaders::class);

        // find all service IDs with the tigris.file_reader tag
        $taggedServices = $container->findTaggedServiceIds('tigris.file_reader');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'addReader',
                    [
                    new Reference($id),
                    $attributes['format'], ]
                );
            }
        }
    }
}
