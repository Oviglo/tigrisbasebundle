<?php

namespace Tigris\BaseBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Tigris\BaseBundle\Dashboard\DashboardManager;

class DashboardPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(DashboardManager::class)) {
            return;
        }

        $definition = $container->findDefinition(DashboardManager::class);
        $taggedServices = $container->findTaggedServiceIds('tigris_base.dashboard_item');

        foreach (array_keys($taggedServices) as $id) {
            $definition->addMethodCall('addItem', [$container->getDefinition($id)]);
        }
    }
}
