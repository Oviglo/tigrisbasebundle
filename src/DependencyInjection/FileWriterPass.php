<?php

namespace Tigris\BaseBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Tigris\BaseBundle\Writer\FileWriters;

class FileWriterPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        // always first check if the primary service is defined
        if (!$container->has(FileWriters::class)) {
            return;
        }

        $definition = $container->findDefinition(FileWriters::class);

        // find all service IDs with the tigris.writer tag
        $taggedServices = $container->findTaggedServiceIds('tigris.file_writer');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'addWriter',
                    [
                    new Reference($id),
                    $attributes['format'], ]
                );
            }
        }
    }
}
