<?php

namespace Tigris\BaseBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Tigris\BaseBundle\DataExporter\DataExporters;

class DataExporterPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        // always first check if the primary service is defined
        if (!$container->has(DataExporters::class)) {
            return;
        }

        $definition = $container->findDefinition(DataExporters::class);

        // find all service IDs with the tigris.data_importer tag
        $taggedServices = $container->findTaggedServiceIds('tigris.data_exporter');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'addExporter',
                    [
                        new Reference($id),
                        $attributes['alias'], ]
                );
            }
        }
    }
}
