<?php

namespace Tigris\BaseBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;

class MenuBuilder
{
    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly TokenStorageInterface $tokenStorage,
        private readonly ParameterBagInterface $parameterBag,
    ) {
    }

    public function createMainMenu(): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->addChild('menu.home', ['route' => 'tigris_homepage']);

        $event = new MenuEvent($menu, ['type' => 'main']);
        $this->eventDispatcher->dispatch($event, Events::LOAD_MAIN_MENU);

        return $event->getMenu();
    }

    public function createAdminMenu(): ItemInterface
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $menu = $this->factory->createItem('root');

        if (!$user instanceof User) {
            return $menu;
        }

        if (!$user->hasRole('ROLE_SUPER_ADMIN') && !$user->hasRole('ROLE_ADMIN')) {
            return $menu;
        }

        $menu->addChild('menu.users', ['route' => 'tigris_base_admin_user_index']);

        $toolsMenu = $menu->addChild('menu.tools', ['uri' => '#']);

        $toolsMenu->addChild('menu.configs', ['route' => 'tigris_base_admin_config_index']);
        $toolsMenu->addChild('wording.menu', ['route' => 'tigris_base_admin_wording_index']);
        $toolsMenu->addChild('menu.sitemap', ['route' => 'tigris_base_admin_sitemap_index']);

        if ($this->parameterBag->has('tigris_base.enable_statistics') && $this->parameterBag->get('tigris_base.enable_statistics')) {
            $toolsMenu->addChild('menu.stats', ['route' => 'tigris_base_admin_stats_index']);
        }

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            $toolsMenu->addChild('menu.data_exporter', ['route' => 'tigris_base_admin_dataexporter_index']);
            $toolsMenu->addChild('menu.data_importer', ['route' => 'tigris_base_admin_dataimporter_index']);
            $toolsMenu->addChild('menu.ui_command', ['route' => 'tigris_base_admin_uicommand_index']);
        }

        $menu->addChild('menu.files', ['route' => 'tigris_base_admin_file_index']);

        $event = new MenuEvent($menu, ['type' => 'admin']);
        $this->eventDispatcher->dispatch($event, Events::LOAD_ADMIN_MENU);

        return $event->getMenu();
    }

    public function createUserMenu(): ItemInterface
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $menu = $this->factory->createItem('root');
        if (!$user instanceof User) {
            return $menu;
        }

        $parent = $menu->addChild($user->getUsername(), ['uri' => '#'])->setExtra('translation_domain', false);

        if ($user->hasRole('ROLE_ADMIN') || $user->isSuperAdmin()) {
            $parent->addChild('menu.admin', ['route' => 'tigris_base_admin_admin_dashboard']);
        }
        
        $parent->addChild('user_account.my_account', ['route' => 'tigris_base_user_myaccount']);

        $event = new MenuEvent($parent, ['type' => 'user']);
        $this->eventDispatcher->dispatch($event, Events::LOAD_USER_MENU);

        $parent = $event->getMenu();

        $parent->addChild('menu.security.logout', ['route' => 'tigris_base_security_logout']);

        return $menu;
    }

    public function myAccountMenu(): ItemInterface
    {
        $menu = $this->factory->createItem('roo');

        $menu->addChild('user_account.menu.main', ['route' => 'tigris_base_user_myaccount']);
        $menu->addChild('user_account.menu.email', ['route' => 'tigris_base_user_updateemail']);

        $event = new MenuEvent($menu, ['type' => 'myaccount']);
        $this->eventDispatcher->dispatch($event, Events::LOAD_MY_ACCOUNT_MENU);

        return $menu;
    }

    public function userProfileMenu(): ItemInterface
    {
        $menu = $this->factory->createItem('roo');

        $event = new MenuEvent($menu, ['type' => 'myaccount']);
        $this->eventDispatcher->dispatch($event, Events::LOAD_USER_PROFILE_MENU);

        return $menu;
    }
}
