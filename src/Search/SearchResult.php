<?php

namespace Tigris\BaseBundle\Search;

use Tigris\BaseBundle\Contracts\ArrayTransformable;

final class SearchResult implements ArrayTransformable
{
    private readonly string $title;

    private readonly string $route;

    public function __construct(
        string $title,
        private readonly string $content,
        string $route,
        private readonly array $routeParams = [],
        private readonly ?string $image = null
    ) {
        $this->title = trim($title);
        $this->route = trim($route);
    }

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'content' => mb_convert_encoding($this->content, 'UTF-8', 'UTF-8'),
            'route' => $this->route,
            'routeParams' => $this->routeParams,
            'id' => uniqid(),
            'image' => $this->image,
        ];
    }
}
