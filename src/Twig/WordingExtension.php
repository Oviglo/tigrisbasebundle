<?php

namespace Tigris\BaseBundle\Twig;

use Tigris\BaseBundle\Service\WordingService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class WordingExtension extends AbstractExtension
{
    public function __construct(private readonly WordingService $wordingService)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('wording', $this->wording(...), ['is_safe' => ['html']]),
        ];
    }

    public function wording(string $page, string $name): string
    {
        return $this->wordingService->content($page, $name);
    }
}
