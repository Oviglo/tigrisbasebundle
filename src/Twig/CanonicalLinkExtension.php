<?php

namespace Tigris\BaseBundle\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Tigris\BaseBundle\Service\CanonicalUrlGenerator;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CanonicalLinkExtension extends AbstractExtension
{
    public function __construct(private readonly CanonicalUrlGenerator $canonicalUrlGenerator, private readonly RequestStack $requestStack)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('canonical_url', $this->generateUrl(...)),
            new TwigFunction('canonical_link_tag', $this->renderLinkTag(...), [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function renderLinkTag(Environment $environment, string $href = null): string
    {
        return $environment->render('@TigrisBase/decorators/_canonical_link_tag.html.twig', [
            'href' => $href,
        ]);
    }

    public function generateUrl(string $route = null, string|array $parameters = []): string
    {
        if (0 === \func_num_args()) {
            $request = $this->requestStack->getCurrentRequest();

            $route = $request->attributes->get('_route');
            $parameters = $request->attributes->get('_route_params', []);
        }

        return $this->canonicalUrlGenerator->generate($route, $parameters);
    }
}
