<?php

namespace Tigris\BaseBundle\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Utils\Utils;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TigrisExtension extends AbstractExtension implements GlobalsInterface
{
    public function __construct(
        private readonly ConfigManager $configManager,
        private readonly ContainerBagInterface $params,
        private readonly string $fontwesomeStyle
    ) {
    }

    public function getGlobals(): array
    {
        return [
            'configs' => $this->configManager,
            'iconStyle' => $this->fontwesomeStyle,
            'registrationMailChecking' => $this->params->get('tigris_base.registration_mail_checking'),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter(
                'parse_icons',
                $this->parseIconsFilter(...),
                ['pre_escape' => 'html', 'is_safe' => ['html']]
            ),
            new TwigFilter(
                'camelize',
                $this->camelizeFilter(...)
            ),
            new TwigFilter(
                'slugify',
                $this->sluggifyFilter(...)
            ),
            new TwigFilter(
                'jsonDecode',
                $this->jsonDecodeFilter(...)
            ),
            new TwigFilter(
                'jsonEncode',
                $this->jsonEncodeFilter(...)
            ),
            new TwigFilter(
                'strPad',
                $this->strPad(...)
            ),

            new TwigFilter(
                'str_replace',
                $this->strReplace(...)
            ),

            new TwigFilter(
                'truncate',
                $this->truncateHtml(...)
            ),

            new TwigFilter(
                'age',
                $this->age(...)
            ),

            new TwigFilter(
                'stripAccent',
                $this->stripAccent(...)
            ),

            new TwigFilter(
                'parse_emoji',
                $this->parseEmoji(...)
            ),

            new TwigFilter(
                'parse_bbcode',
                $this->parseBbcodeFilter(...),
                ['pre_escape' => 'html', 'is_safe' => ['html']]
            ),

            new TwigFilter(
                'toPointCase',
                $this->toPointCase(...)
            ),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'icon',
                $this->iconFunction(...),
                ['pre_escape' => 'html', 'is_safe' => ['html']]
            ),

            new TwigFunction(
                'bundle_exists',
                $this->bundleExists(...)
            ),

            new TwigFunction(
                'get_config',
                $this->configManager->getConfig(...)
            ),
        ];
    }

    public function parseIconsFilter(string $text, bool $fixWidth = false): ?string
    {
        $fontwesomeStyle = $this->fontwesomeStyle;

        return preg_replace_callback(
            '/\.([a-z]+)-([a-z0-9+-]+)/',
            function (array $matches) use ($fontwesomeStyle, $fixWidth): string {
                if ('icon' === $matches[1]) {
                    $matches[1] = 'fa'.substr($fontwesomeStyle, 0, 1);
                } elseif ('brand' === $matches[1]) {
                    $matches[1] = 'fab';
                }

                return sprintf('<i class="%1$s fa-%2$s%3$s"></i> ', $matches[1], $matches[2], $fixWidth ? ' fa-fw' : '');
            },
            (string) $text
        );
    }

    public function iconFunction($icon, $iconSet = 'fas'): string
    {
        // $icon = str_replace('+', ' '.$iconSet.'-', $icon);
        return sprintf('<i class="%1$s fa-%2$s"></i> ', $iconSet, $icon);
    }

    public function camelizeFilter(string $value): string
    {
        return Utils::toCamelCase($value);
    }

    public function sluggifyFilter(string $value): string
    {
        return Utils::slugify($value);
    }

    public function jsonDecodeFilter($value): mixed
    {
        return json_decode((string) $value, true, 512, JSON_THROW_ON_ERROR);
    }

    public function jsonEncodeFilter($value): string
    {
        return json_encode($value, JSON_THROW_ON_ERROR);
    }

    public function strPad($value, $length, $padString = ' ', $padType = STR_PAD_RIGHT): string
    {
        return str_pad((string) $value, $length, $padString, $padType);
    }

    public function strReplace($subject, $search, $replace): string
    {
        return str_replace($search, $replace, (string) $subject);
    }

    public function bundleExists($bundle): bool
    {
        return array_key_exists(
            $bundle,
            $this->params->get('kernel.bundles')
        );
    }

    public function truncateHtml(string $text, int $length = 100, string $ending = '...'): string
    {
        return Utils::truncateHtml(\strip_tags($text), $length, $ending);
    }

    public function age(\DateTimeInterface $birthdate): string
    {
        $now = new \DateTime('now');
        $difference = $now->diff($birthdate);

        return $difference->format('%y');
    }

    public function stripAccent(string $str): string
    {
        return Utils::stripAccent($str);
    }

    public function parseEmoji(string $text): string
    {
        return Utils::loadEmojis($text);
    }

    public function parseBbcodeFilter(string $text): string
    {
        return Utils::showBBcodes($text);
    }

    public function toPointCase(string $text): string
    {
        return Utils::toPointCase($text);
    }
}
