<?php

namespace Tigris\BaseBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\DataExporter\DataExporters;
use Tigris\BaseBundle\Entity\DataExport;
use Tigris\BaseBundle\Repository\DataExportRepository;
use Tigris\BaseBundle\Writer\FileWriters;

class DataExportManager extends AbstractManager
{
    /**
     * @var DataExporters
     */
    public $dataExporters;

    public function __construct(EntityManagerInterface $em, DataExportRepository $repository, private readonly TranslatorInterface $translator, private readonly FileWriters $fileWriters, DataExporters $dataExporters)
    {
        $this->setManager($em);
        $this->setRepository($repository);
        $this->dataExporters = $dataExporters;
    }

    public function findData($criterias)
    {
        $entities = $this->getRepository()->findData($criterias);

        foreach ($entities as $entity) {
            $entity->setTypeLabel($this->translator->trans('data.exporter.type.'.$entity->getType()));
            $entity->setFormatLabel($this->translator->trans('data.exporter.'.$entity->getFormat()));
            $entity->setStatusLabel($this->translator->trans('data.exporter.status.'.$entity->getStatus()));
        }

        return $entities;
    }

    public function save(DataExport $entity): void
    {
        $writer = $this->fileWriters->getWriter($entity->getFormat());
        $exporter = $this->dataExporters->getExporter($entity->getType());
        $date = new \DateTime();

        try {
            $path = $writer->write($entity->getAbsoluteFolder(), $entity->getType().'_'.$date->format('Y-m-d-H-i-s'), $exporter->format($entity->getCriteria(), $writer->getName()));
            $entity->setFilePath($path);
            $entity->setStatus(DataExport::STATUS_FINISH);
        } catch (\Exception $e) {
            $entity->setFilePath('');
            $entity->setErrorMessage($e->getMessage());
            $entity->setStatus(DataExport::STATUS_ERROR);
        }
        
        $em = $this->getManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getExporters()
    {
        return $this->dataExporters->getExporters();
    }
}
