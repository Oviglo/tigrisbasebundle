<?php

namespace Tigris\BaseBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\UserEvent;
use Tigris\BaseBundle\Repository\UserRepository;

class UserManager extends AbstractManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly UserRepository $userRepository,
        private readonly string $userClass,
        private readonly UserPasswordHasherInterface $passwordEncoder,
        private readonly EventDispatcherInterface $eventDispatcher,
    ) {
    }

    public function createUser(): User
    {
        $user = $this->userClass;

        $entity = new $user();

        $event = new UserEvent($entity);
        $this->eventDispatcher->dispatch($event, Events::CREATE_USER);

        return $event->getUser();
    }

    public function updateUser(User $entity): void
    {
        if ('' !== $entity->getPlainPassword()) {
            $entity->setPassword($this->passwordEncoder->hashPassword(
                $entity,
                $entity->getPlainPassword()
            ));
        }

        $this->em->persist($entity);
        $this->em->flush();
    }

    public function findByUsername(string $username): ?User
    {
        return $this->findUserBy(['username' => $username]);
    }

    public function findUserBy(array $criteria): ?User
    {
        return $this->userRepository->findOneBy($criteria);
    }

    public function updateEmail(User $entity): bool
    {
        if ($this->passwordEncoder->isPasswordValid($entity, $entity->getPlainPassword())) {
            $this->em->flush();

            return true;
        }

        return false;
    }
}
