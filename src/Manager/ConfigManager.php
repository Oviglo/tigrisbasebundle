<?php

namespace Tigris\BaseBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Tigris\BaseBundle\Entity\Config;
use Tigris\BaseBundle\Repository\ConfigRepository;
use Tigris\BaseBundle\Repository\FileRepository;

class ConfigManager
{
    final public const TYPE_TEXT = 'text';

    final public const TYPE_DATE = 'date';

    final public const TYPE_TIME = 'time';

    final public const TYPE_DATETIME = 'datetime';

    final public const TYPE_CHECKBOX = 'checkbox';

    final public const TYPE_TABLE_COLLECTION = 'table_collection';

    final public const TYPE_COLLECTION = 'collection';

    final public const TYPE_UPLOADFILE = 'uploadfile';

    final public const TYPE_NUMBER = 'number';

    final public const TYPE_INTEGER = 'integer';

    final public const TYPE_EMAIL = 'email';

    final public const TYPE_MONEY = 'money';

    private ?array $configs = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ConfigRepository $configRepository,
        private readonly FileRepository $fileRepository
    ) {
    }

    public function getConfigs(): array
    {
        if (null === $this->configs) {
            $this->configs = $this->configRepository->findAllAsArray();
        }

        return $this->configs;
    }

    public function getConfig(string $name): array|Config|null
    {
        $configs = $this->getConfigs();

        $nameParts = explode('.', $name, 2);

        if (count($nameParts) > 1) {
            if (empty($configs[$nameParts[0]])) {
                return null;
            }

            return $this->getSubConfig($nameParts[1], $configs[$nameParts[0]]);
        }

        return $configs[$name] ?? null;
    }

    public function getSubConfig(string $name, array $configs): array|Config|null
    {
        $nameParts = explode('.', $name, 2);

        if (1 === count($nameParts)) {
            return $configs[$name] ?? null;
        }

        if (empty($configs[$nameParts[0]])) {
            return null;
        }

        if ($configs[$nameParts[0]] instanceof Config) {
            return $configs[$nameParts[0]];
        }

        return $this->getSubConfig($nameParts[1], $configs[$nameParts[0]]);
    }

    public function getGroupValue(string $name): ?array
    {
        $config = $this->getConfig($name);

        $groups = [];
        if (null === $config) {
            foreach ($this->configs as $key => $value) {
                if (str_starts_with($key, $name)) {
                    $groups[$key] = $value;
                }
            }

            $this->configs[$name] = $groups;

            return $groups;
        }

        return is_array($config) ? $config : [$config];
    }

    public function getValue(string $name, mixed $default = null): mixed
    {
        $config = $this->getConfig($name);

        return ($config instanceof Config && $config->getValue()) ? $this->format($config->getValue(), $config->getType()) : $default;
    }

    public function setConfig(string $bundle, string $name, mixed $value): bool
    {
        $config = $this->configRepository->getByName($bundle);
        if (is_object($config)) {
            $config->setValue($value);
            $this->entityManager->flush();
        }

        return false;
    }

    public function saveFromFormData(FormInterface $form, string $namePrefix = '', string $bundleName = '', ?array $entities = null): void
    {
        $children = $form->all();
        $type = $form->getConfig()->getType()->getInnerType()->getBlockPrefix();

        if (null === $entities) {
            $entities = $this->configRepository->findAll();
        }

        // Convert choice value type
        if ('choice' == $type) {
            // $choices = (array) $form->getConfig()->getOption('choices');
            $type = 'array'; // gettype(array_shift($choices));
            $children = [];
        }

        foreach ($children as $child) {
            $this->saveFromFormData($child, $namePrefix.$child->getName().'.', $bundleName, $entities);
        }

        if ([] === $children && 'submit' != $type) {
            $namePrefix = rtrim($namePrefix, '.');
            $entity = $this->searchConfig($entities, $bundleName, $namePrefix);
            if (!$entity instanceof Config) {
                $entity = (new Config())
                    ->setBundle($bundleName)
                    ->setName($namePrefix)
                ;

                $this->entityManager->persist($entity);
            }

            $entity->setType($type);
            $entity->setValue($form->getViewData());
        }
    }

    public function arrayToPaths(array $array): array
    {
        $paths = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $subPaths = $this->arrayToPaths($value);
                foreach ($subPaths as $subKey => $subValue) {
                    $paths[$key.'.'.$subKey] = $subValue;
                }
            } else {
                $paths[$key] = $value;
            }
        }

        return $paths;
    }

    public function formatFormData(Config $config, array &$data): void
    {
        $this->parsedNameData($config->getName(), $data, $config);
    }

    public function format($value, $type): mixed
    {
        switch ($type) {
            case self::TYPE_DATE:
            case self::TYPE_TIME:
            case self::TYPE_DATETIME:
                return new \DateTime($value);

            case self::TYPE_CHECKBOX:
                return 1 == $value;

            case self::TYPE_UPLOADFILE:
                if (is_array($value)) {
                    return $this->fileRepository->findByIds($value);
                }

                return $this->fileRepository->findOneById($value);

            case self::TYPE_NUMBER:
            case self::TYPE_MONEY:
                return (float) $value;

            case self::TYPE_INTEGER:
                return (int) $value;
        }

        return $value;
    }

    /**
     * Iteration on Config name to create a nested array.
     */
    private function parsedNameData(string $name, array &$data, Config $config): void
    {
        $parsedName = explode('.', $name, 2);

        if (count($parsedName) > 1) {
            if (!isset($data[$parsedName[0]])) {
                $data[$parsedName[0]] = [];
            }

            $this->parsedNameData($parsedName[1], $data[$parsedName[0]], $config);
        } else {
            $data[$parsedName[0]] = $this->format($config->getValue(), $config->getType());
        }
    }

    private function searchConfig(iterable $entities, string $bundle, string $name): ?Config
    {
        foreach ($entities as $entity) {
            if ($entity->getBundle() == $bundle && $entity->getName() == $name) {
                return $entity;
            }
        }

        return null;
    }
}
