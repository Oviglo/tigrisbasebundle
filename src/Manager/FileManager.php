<?php

namespace Tigris\BaseBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\FileFolder;
use Tigris\BaseBundle\Repository\FileFolderRepository;
use Tigris\BaseBundle\Repository\FileRepository;

class FileManager
{
    public function __construct(
        private readonly FileRepository $fileRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface $translator,
        private readonly FileFolderRepository $fileFolderRepository
    ) {
    }

    public function moveFolderFiles(FileFolder $fromFolder, FileFolder $toFolder): void
    {
        $files = $this->fileRepository->findByFolder($fromFolder);

        foreach ($files as $file) {
            $file->setFolder($toFolder);
        }

        $this->entityManager->flush();
    }

    /**
     * Get user folder, for non admin user, there are specific folder for user in "Users" folder.
     */
    public function getUserFolder(UserInterface $user): FileFolder
    {
        $folder = $this->fileFolderRepository->findOneBy(['user' => $user, 'name' => $user->getUsername()]);

        if (null === $folder) {
            $userFolder = $this->fileFolderRepository->findOneByName($this->translator->trans('user.users'));
            if (null === $userFolder) {
                $userFolder = (new FileFolder())
                    ->setName($this->translator->trans('user.users'))
                ;

                $this->entityManager->persist($userFolder);
            }

            $folder = (new FileFolder())
                ->setName($user->getUsername())
                ->setParent($userFolder)
                ->setUser($user)
            ;

            $this->entityManager->persist($folder);
            $this->entityManager->flush();
        }

        return $folder;
    }
}
