<?php

namespace Tigris\BaseBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\DataImporter\AbstractImporter;
use Tigris\BaseBundle\DataImporter\DataImporters;
use Tigris\BaseBundle\Entity\DataImport;
use Tigris\BaseBundle\Entity\DataImportField;
use Tigris\BaseBundle\Reader\FileReaders;
use Tigris\BaseBundle\Reader\ReaderInterface;
use Tigris\BaseBundle\Repository\DataImportRepository;

class DataImportManager extends AbstractManager
{
    public function __construct(
        EntityManagerInterface $em,
        private readonly DataImportRepository $dataImportRepository,
        private readonly TranslatorInterface $translator,
        private readonly FileReaders $fileReaders,
        private readonly DataImporters $dataImporters
    ) {
        $this->setManager($em);
    }

    public function save(DataImport $entity): void
    {
        $file = $entity->getFile();
        if (!$file instanceof File) {
            throw new \Exception('File not found');
        }
        
        $ext = $file->guessExtension();
        if ('txt' == $ext) {
            $ext = 'csv';
        }
        
        $reader = $this->fileReaders->getReader($ext);

        if (!$reader instanceof ReaderInterface) {
            throw new \Exception(sprintf('File reader for %s format not found', $ext));
        }

        $type = $entity->getType();
        $dataImporter = $this->dataImporters->getImporter($type);
        $dataImporter->setImportEntity($entity);

        if (!$dataImporter instanceof AbstractImporter) {
            throw new \Exception(sprintf('Data importer for %s type not found', $type));
        }

        if ([] === $entity->getMapping()) {
            $map = [];
            foreach ($dataImporter->getMapping() as $name => $format) {
                $map[] = ['from' => $name, 'to' => $name, 'format' => $format];
            }

            $entity->setMapping($map);
        }

        if (0 === count($entity->getFields())) {
            $fileData = $reader->read($file);
            $header = array_values($reader->getHeader());
            foreach ($fileData as $data) {
                if (!empty($data)) {
                    $fieldData = [];
                    foreach (array_values($data) as $key => $f) {
                        if (isset($header[$key])) {
                            $fieldData[$header[$key]] = $f;
                        }
                    }

                    $field = (new DataImportField())
                        ->setData($fieldData);

                    $entity->addField($field);
                }
            }
        }

        $em = $this->getManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getHeader(DataImport $entity): array
    {
        $file = new File($entity->getAbsolutePath());
        $ext = $file->guessExtension();
        if ('txt' == $ext) {
            $ext = 'csv';
        }
        
        $reader = $this->fileReaders->getReader($ext);

        if (!$reader instanceof ReaderInterface) {
            throw new \Exception(sprintf('File reader for %s format not found', $ext));
        }

        $reader->read($file);

        return $reader->getHeader();
    }

    public function findData(array $criteria): Paginator
    {
        $entities = $this->dataImportRepository->findData($criteria);

        foreach ($entities as $entity) {
            $entity->setTypeLabel($this->translator->trans('data.importer.type.'.$entity->getType()));
            $entity->setStatusLabel($this->translator->trans('data.importer.status.'.$entity->getStatus()));
        }

        return $entities;
    }

    public function execute(): void
    {
        $entities = $this->dataImportRepository->findPending();
        $em = $this->getManager();

        foreach ($entities as $entity) {
            $fields = $entity->getFields();
            $type = $entity->getType();
            $dataImporter = $this->dataImporters->getImporter($type);
            $dataImporter->setImportEntity($entity);
            // PrimaryKey is set to update existing entities
            $primaryKey = $entity->getPrimaryKey();

            if (!$dataImporter instanceof AbstractImporter) {
                continue;
            }

            $mapping = $entity->getMapping();
            $mappginColumns = array_column($mapping, 'from');
            foreach ($fields as $field) {
                if (!$field instanceof DataImportField) {
                    continue;
                }

                $data = [];
                $fieldData = $field->getData();
                $primaryKeyValue = null;

                foreach ($fieldData as $key => $element) {
                    $mappingKey = array_search($key, $mappginColumns, true);
                    if (false !== $mappingKey) {
                        $to = $mapping[$mappingKey]['to'];
                        $data[$to] = $element;

                        if ($to === $primaryKey) {
                            $primaryKeyValue = $element;
                        }
                    }
                }

                $createdEntity = null;
                if (null !== $primaryKey) {
                    $createdEntity = $dataImporter->getEntity($primaryKey, $primaryKeyValue, $data);
                }

                if (null === $createdEntity) {
                    $createdEntity = $dataImporter->createEntity($data);
                }

                if (null != $createdEntity) {
                    $em->persist($createdEntity);
                }

                $entity->removeField($field);
            }

            if (0 === (is_countable($entity->getFields()) ? count($entity->getFields()) : 0)) {
                $entity->setStatus(DataImport::STATUS_FINISH);
            }
        }

        $em->flush();
    }
}
