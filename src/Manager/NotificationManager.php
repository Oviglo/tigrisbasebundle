<?php

namespace Tigris\BaseBundle\Manager;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Contracts\Model\UserimageInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Entity\Notification;
use Tigris\BaseBundle\Repository\NotificationRepository;

class NotificationManager extends AbstractManager
{
    public function __construct(
        private readonly NotificationRepository $notificationRepository,
        private readonly RouterInterface $router,
        private readonly TranslatorInterface $translator
    ) {
    }

    public function findNew(User $user, \DateTime $date = null): \Generator
    {
        $notifications = $this->notificationRepository->findNew($user, $date);

        return $this->formatNotifications($notifications);
    }

    public function findLast(User $user): \Generator
    {
        $notifications = $this->notificationRepository->findLast($user);

        return $this->formatNotifications($notifications);
    }

    public function findData(array $criteria): array
    {
        $result = $this->notificationRepository->findData($criteria);

        $notifications = $this->formatNotifications(iterator_to_array($result->getIterator()));

        return ['entities' => $notifications, 'totalCount' => count($result)];
    }

    private function formatNotifications(array $notifications): \Generator
    {
        foreach ($notifications as $notification) {
            yield $this->formatNotification($notification);
        }
    }

    private function formatNotification(Notification $notification): array
    {
        $data = $notification->getData();
        $buttons = [];

        if (isset($data['buttons'])) {
            foreach ($data['buttons'] as $button) {
                $buttons[] = [
                    'label' => $this->translator->trans($button['label']),
                    'url' => $this->router->generate($button['route'], $button['routeParams']),
                ];
            }
        }

        $owner = $notification->getOwner();
        $icon = $data['icon'] ?? false;
        if (!$icon && $owner instanceof UserimageInterface && $owner->hasUserimage()) {
            $icon = $owner->getUserimage()->getWebPath();
        }

        return [
            'id' => $notification->getId(),
            'date' => $notification->getCreatedAt(),
            'title' => $this->translator->trans($data['title'], $data['titleParams'] ?? []),
            'content' => $this->translator->trans($data['message'], $data['messageParams'] ?? []),
            'icon' => $icon,
            'buttons' => $buttons,
        ];
    }

    public function read(User $user): void
    {
        $this->notificationRepository->read($user);
    }

    public function unreadCount(User $user): int
    {
        return $this->notificationRepository->unreadCount($user);
    }
}
