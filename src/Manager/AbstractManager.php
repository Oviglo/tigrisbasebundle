<?php

namespace Tigris\BaseBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractManager
{
    protected $entityManager;
    
    protected $repositories = [];

    public function setRepository($repository, string $name = 'main'): void
    {
        $this->repositories[$name] = $repository;
    }

    public function setManager(EntityManagerInterface $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    public function getManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function getRepository(string $name = 'main')
    {
        return $this->repositories[$name] ?? null;
    }
}
