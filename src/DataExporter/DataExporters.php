<?php

namespace Tigris\BaseBundle\DataExporter;

class DataExporters
{
    private array $exporters = [];

    public function addExporter(AbstractExporter $exporter, $name): void
    {
        if (!isset($this->exporters[$name])) {
            $exporter->setName($name);
            $this->exporters[$name] = $exporter;
        }
    }

    public function getExporter(string $name): ?AbstractExporter
    {
        return $this->exporters[$name] ?? null;
    }

    /**
     * @return array<AbstractExporter>
     */
    public function getExporters(): array
    {
        return $this->exporters;
    }
}
