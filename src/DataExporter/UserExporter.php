<?php

namespace Tigris\BaseBundle\DataExporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Repository\UserRepository;

class UserExporter extends AbstractExporter
{
    public function __construct(EntityManagerInterface $entityManager, array $configs, private readonly UserRepository $userRepository, TranslatorInterface $translator)
    {
        parent::__construct($entityManager, $configs, $translator);
    }

    public function format(array $criteria = [], string $writerName = null): array
    {
        $entities = $this->userRepository->findData($criteria);
        $data = [];
        $data[] = $this->generateHeader(['user.username', 'user.email', 'user.date.created']);

        foreach ($entities as $entity) {
            $data[] = $this->generateRow(['username', 'email', 'createdAt'], $entity);
        }

        return $data;
    }
}
