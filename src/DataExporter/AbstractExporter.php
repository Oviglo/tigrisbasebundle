<?php

namespace Tigris\BaseBundle\DataExporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractExporter
{
    protected $name;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        #[Autowire('%tigris_base.data_export%')]
        protected array $configs,
        protected TranslatorInterface $translator
    ) {}

    public function setName(string $name): AbstractExporter
    {
        $this->name = $name;

        return $this;
    }

    abstract public function format(array $criteria = [], ?string $writerName = null): array;

    /**
     * Return an array with heading colums.
     */
    protected function generateHeader(array $data, bool $translator = true): array
    {
        $header = [];
        foreach ($data as $col) {
            $header[] = ['value' => $translator ? $this->translator->trans($col) : $col];
        }

        // Load additionnals attributes from tigris_base.yaml config file
        $config = $this->configs[$this->name] ?? ['attr' => []];
        foreach ($config['attr'] as $label => $col) {
            $header[] = ['value' => $translator ? $this->translator->trans($label) : $label];
        }

        return $header;
    }

    protected function generateRow(array $map, $entity): array
    {
        $row = [];

        // Load additionnals attributes from tigris_base.yaml config file
        $config = $this->configs[$this->name] ?? ['attr' => []];
        foreach ($config['attr'] as $col) {
            $map[] = $col;
        }

        foreach ($map as $data) {
            $m = 'get'.ucfirst((string) $data);
            $mAlt = 'is'.ucfirst((string) $data);
            if (method_exists($entity, $m)) {
                $row[] = ['value' => $entity->{$m}()];
            } elseif (method_exists($entity, $mAlt)) {
                $row[] = ['value' => $entity->{$mAlt}()];
            } else {
                $row[] = ['value' => ''];
            }
        }

        return $row;
    }
}
