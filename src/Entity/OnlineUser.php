<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Repository\OnlineUserRepository;

#[ORM\Entity(repositoryClass: OnlineUserRepository::class)]
#[ORM\Table(name: 'base_online_user')]
class OnlineUser
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 20)]
    private string $ip;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $onlineAt;

    #[ORM\Column(type: Types::TEXT)]
    private string $url;

    public function __construct()
    {
        $this->onlineAt = new \DateTime();
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getOnlineAt(): \DateTimeInterface
    {
        return $this->onlineAt;
    }

    public function setOnlineAt(\DateTimeInterface $onlineAt): self
    {
        $this->onlineAt = $onlineAt;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }
}
