<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class DataImportField
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\ManyToOne(inversedBy: 'fields')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private DataImport|null $dataImport = null;

    #[ORM\Column(type: Types::JSON)]
    private array $data;

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getDataImport(): DataImport|null
    {
        return $this->dataImport;
    }

    public function setDataImport(DataImport|null $dataImport): self
    {
        $this->dataImport = $dataImport;

        return $this;
    }

    public function getData(): array
    {
        return $this->data ?? [];
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }
}
