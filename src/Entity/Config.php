<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Repository\ConfigRepository;

#[ORM\Entity(repositoryClass: ConfigRepository::class)]
#[ORM\Table(name: 'config')]
#[ORM\HasLifecycleCallbacks]
class Config implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 125)]
    private string $name;

    #[ORM\Column(length: 125)]
    private string $bundle;

    #[ORM\Column(nullable: true, type: Types::TEXT)]
    private ?string $value = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $type = null;

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setBundle(string $bundle): self
    {
        $this->bundle = $bundle;

        return $this;
    }

    public function getBundle(): string
    {
        return $this->bundle;
    }

    public function setValue(mixed $value = null): self
    {
        $this->value = $this->format($value);

        return $this;
    }

    private function format(mixed $value): string
    {
        // Test for object
        if (is_object($value)) {
            return $this->formatObject($value);
        } elseif (is_numeric($value)) {
            return (string) $value;
        } elseif (is_array($value)) {
            foreach ($value as $key => $val) {
                $value[$key] = $this->format($val);
            }

            return json_encode($value) ?? '';
        } /*elseif (is_string($value) && '' !== $value) {
            $json = json_decode($value, true, 512);
            if (null !== $json) {
                return $json;
            }
        }*/

        return (string) $value;
    }

    private function formatObject(object $object): mixed
    {
        if ($object instanceof \DateTimeInterface) {
            return $object->format('Y-m-d H:i:s');
        }

        if ($object instanceof ArrayCollection) {
            $ids = [];
            foreach ($object->toArray() as $element) {
                if (method_exists($element, 'getId')) {
                    $ids[] = $element->getId();
                }
            }

            return json_encode($ids);
        }

        if (method_exists($object, 'getId')) {
            return $object->getId();
        }

        if ($object instanceof \Stringable) {
            return (string) $object;
        }

        return '';
    }

    public function getValue(): string|array|null
    {
        $jsonValue = json_decode((string) $this->value, true);

        if (JSON_ERROR_NONE === json_last_error()) {
            if (is_array($jsonValue)) {
                $jsonValue = array_map(function ($data) {
                    $jsonData = null;
                    if (!empty($data) && is_string($data)) {
                        $jsonData = json_decode($data, true, 512);
                    }

                    return $jsonData ?? $data;
                }, $jsonValue);
            }

            return $jsonValue;
        }

        return $this->value;
    }

    public function getUnformattedValue(): null|string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
