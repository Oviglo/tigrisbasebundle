<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Tree\Traits\NestedSetEntity;
use JMS\Serializer\Annotation as JMS;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Repository\FileFolderRepository;
use Tigris\BaseBundle\Utils\Utils;

#[ORM\Entity(repositoryClass: FileFolderRepository::class)]
#[ORM\Table(name: 'base_file_folder')]
#[ORM\HasLifecycleCallbacks]
#[Gedmo\Tree(type: 'nested')]
class FileFolder implements \Stringable
{
    use TimestampableEntity;
    use NestedSetEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 80)]
    private string $name = '';

    #[ORM\Column(length: 120, unique: true)]
    #[Gedmo\Slug(fields: ['name'])]
    private ?string $slug = null;

    #[ORM\ManyToOne(inversedBy: 'children')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[Gedmo\TreeParent]
    #[Gedmo\SortableGroup]
    #[JMS\Exclude]
    private ?FileFolder $parent = null;

    #[ORM\OneToMany(targetEntity: FileFolder::class, mappedBy: 'parent')]
    #[ORM\OrderBy(['left' => 'ASC', 'name' => 'ASC'])]
    private Collection $children;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    #[JMS\Exclude]
    private ?User $user = null;

    #[JMS\AccessType(type: 'public_method')]
    #[JMS\Accessor(getter: 'getPath')]
    private ?string $path = null;

    private ?string $oldPath = null;

    #[ORM\OneToMany(targetEntity: File::class, mappedBy: 'folder', cascade: ['persist', 'remove'])]
    #[JMS\Exclude]
    private Collection $files;

    #[ORM\Column]
    #[Gedmo\SortablePosition]
    private int $position;

    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getPath(): string
    {
        $path = '';

        if ($this->parent instanceof FileFolder) {
            $path = $this->parent->getPath();
        }

        return $path.Utils::stripAccent(Utils::slugify($this->name)).DIRECTORY_SEPARATOR;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->oldPath = $this->getPath();
        $this->name = $name;
        $this->path = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getParent(): ?FileFolder
    {
        return $this->parent;
    }

    public function setParent(?FileFolder $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function setChildren(Collection $children): self
    {
        $this->children = $children;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user = null): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function setFiles(Collection $files): self
    {
        $this->files = $files;

        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $path = File::getPublicRootDir().$this->getPath();

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
    }

    #[ORM\PostUpdate]
    public function postUpdate(): void
    {
        $path = File::getPublicRootDir().$this->getPath();
        $oldPath = File::getPublicRootDir().$this->oldPath;

        if (\is_dir($oldPath)) {
            rename($oldPath, $path);
        }
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }
}
