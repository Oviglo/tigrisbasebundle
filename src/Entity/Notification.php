<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Notification\NotificationData;
use Tigris\BaseBundle\Repository\NotificationRepository;

#[ORM\Entity(repositoryClass: NotificationRepository::class)]
#[ORM\Table(name: 'notification')]
class Notification
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private User|null $user = null;

    #[ORM\Column(type: Types::JSON)]
    private array $data = [];

    #[ORM\Column]
    private bool $readed = false;

    #[ORM\Column(length: 180, nullable: true)]
    private string|null $entityClass = null;

    #[ORM\Column(nullable: true)]
    private int|null $entityId = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private User|null $owner = null;

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getUser(): User|null
    {
        return $this->user;
    }

    public function setUser(User $user = null): self
    {
        $this->user = $user;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array|NotificationData $data): self
    {
        $this->data = $data instanceof NotificationData ? $data->toArray() : $data;

        return $this;
    }

    public function getReaded(): bool
    {
        return $this->readed;
    }

    public function setReaded(bool $readed): self
    {
        $this->readed = $readed;

        return $this;
    }

    public function getEntityClass(): string|null
    {
        return $this->entityClass;
    }

    public function setEntityClass(string $entityClass = null): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    public function getEntityId(): int|null
    {
        return $this->entityId;
    }

    public function setEntityId(int $entityId = null): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getOwner(): User|null
    {
        return $this->owner;
    }

    public function setOwner(User $owner = null): self
    {
        $this->owner = $owner;

        return $this;
    }
}
