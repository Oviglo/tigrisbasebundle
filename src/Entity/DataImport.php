<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Order;
use Doctrine\Common\Collections\Selectable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Tigris\BaseBundle\Entity\Model\AbstractUpload;
use Tigris\BaseBundle\Repository\DataImportRepository;

#[ORM\Entity(repositoryClass: DataImportRepository::class)]
#[ORM\HasLifecycleCallbacks]
class DataImport extends AbstractUpload
{
    use TimestampableEntity;

    final public const STATUS_PENDING = 'pending';
    
    final public const STATUS_FINISH = 'finish';
    
    final public const STATUS_ERROR = 'error';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 80)]
    private string $type;

    private string $typeLabel;

    #[ORM\Column(length: 15)]
    private string $status = self::STATUS_PENDING;

    private string $statusLabel;

    #[ORM\Column]
    private int $offset = 0;

    #[ORM\Column(type: Types::JSON)]
    private array $mapping = [];

    #[ORM\OneToMany(targetEntity: DataImportField::class, mappedBy: 'dataImport', cascade: ['all'], orphanRemoval: true)]
    private Collection&Selectable $fields;

    #[ORM\Column(type: Types::JSON)]
    private array $options = [];

    #[ORM\Column(length: 30, nullable: true)]
    private null|string $primaryKey = null;

    public function __construct()
    {
        $this->fields = new ArrayCollection();
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    protected function getUploadDir(): string
    {
        return DIRECTORY_SEPARATOR . 'data_import' . DIRECTORY_SEPARATOR;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatusLabel(): string
    {
        return $this->statusLabel;
    }

    public function setStatusLabel(string $statusLabel): self
    {
        $this->statusLabel = $statusLabel;

        return $this;
    }

    public function getTypeLabel(): string
    {
        return $this->typeLabel;
    }

    public function setTypeLabel(string $typeLabel): self
    {
        $this->typeLabel = $typeLabel;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMapping(): array
    {
        return $this->mapping;
    }

    public function setMapping(array $mapping): self
    {
        $this->mapping = $mapping;

        return $this;
    }

    public function getFields(): ArrayCollection
    {
        $criteria = Criteria::create()
        //    ->orderBy(['id' => Order::Ascending])
        //    ->setMaxResults(60)
        ;

        return $this->fields->matching($criteria);
    }

    public function setFields(ArrayCollection $fields): self
    {
        $this->fields = $fields;

        return $this;
    }

    public function addField(DataImportField $field): self
    {
        if (!$this->fields->contains($field)) {
            $field->setDataImport($this);
            $this->fields[] = $field;
        }

        return $this;
    }

    public function removeField(DataImportField $field): self
    {
        if ($this->fields->contains($field)) {
            $this->fields->removeElement($field);
            $field->setDataImport(null);
        }

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getPrimaryKey(): null|string
    {
        return $this->primaryKey;
    }

    public function setPrimaryKey(null|string $primaryKey): self
    {
        $this->primaryKey = $primaryKey;
        return $this;
    }
}
