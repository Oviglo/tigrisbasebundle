<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gumlet\ImageResize;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File as SfFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Repository\FileRepository;

#[ORM\Entity(repositoryClass: FileRepository::class)]
#[ORM\Table(name: 'base_file')]
#[ORM\HasLifecycleCallbacks]
class File implements \Stringable
{
    use TimestampableEntity;

    final public const TYPE_IMAGE = 'image';
    
    final public const TYPE_GIF = 'gif';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    private string $name;

    #[ORM\Column(length: 180, unique: true)]
    #[Gedmo\Slug(fields: ['name'])]
    private string $slug;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[JMS\Exclude]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'files')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private ?FileFolder $folder = null;

    #[ORM\Column(length: 5, nullable: true)]
    private ?string $ext = null;

    #[ORM\Column]
    private int $downloadCount = 0;

    #[ORM\Column(length: 20)]
    private string $type = '';

    #[ORM\Column(nullable: true)]
    private ?string $path = null;

    #[ORM\Column(nullable: true)]
    private ?int $size = 0;

    #[ORM\Column(type: Types::JSON)]
    private ?array $options = [];

    #[Assert\File]
    protected ?SfFile $file = null;

    protected string $oldPath = '';

    private array $imageStyles = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user = null): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFolder(): FileFolder
    {
        return $this->folder;
    }

    public function setFolder(FileFolder $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function getFile(): SfFile
    {
        return $this->file;
    }

    public function setFile(SfFile $file): self
    {
        $this->oldPath = (null != $this->folder ? $this->folder->getPath() : '').$this->path; // Save old path to delete it
        $this->path = ''; // Force doctrine to update the File entity
        $this->file = $file;

        // File type
        if ($file instanceof UploadedFile) {
            $this->type = explode('/', $file->getClientMimeType())[0];
        } else {
            $this->type = explode('/', (string) $file->getMimeType())[0];
        }

        return $this;
    }

    public function isValid(): bool
    {
        return $this->file instanceof SfFile;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function generatePath(): void
    {
        if ($this->isValid()) {
            if ($this->file instanceof UploadedFile) {
                $this->path = time().'_'.$this->file->getClientOriginalName();
                $this->name = $this->file->getClientOriginalName();
                $pathParts = pathinfo($this->path);
                $this->ext = $pathParts['extension'];
            } else {
                // $this->path = time().'_'.$this->file->getFilename();
                // $pathParts = pathinfo($this->path);
                // $this->name = $pathParts['filename'];
                // $this->ext = $pathParts['extension'];
            }

            try {
                $this->size = $this->file->getSize();
            } catch (\Throwable) {
            }
        }
    }

    #[ORM\PostPersist]
    #[ORM\PostUpdate]
    public function upload(): void
    {
        $filesystem = new Filesystem();
        // Delete old file
        if (is_file(static::getPublicRootDir().$this->oldPath)) {
            unlink(static::getPublicRootDir().$this->oldPath);
        }

        // Move to the good folder dir
        $directory = null != $this->folder ? $this->folder->getPath() : '';
        if ($this->file instanceof SfFile) {
            $filesystem->chmod(static::getPublicRootDir().$directory, 0777, 0000, true);
            $this->file->move(static::getPublicRootDir().$directory, $this->path);
        }
    }

    #[ORM\PreRemove]
    public function removeFile(): void
    {
        $directory = static::getPublicRootDir().(null != $this->folder ? $this->folder->getPath() : '');
        if (is_file($directory.$this->path)) {
            unlink($directory.$this->path);
        }
    }

    /**
     * Get the absolute upload path.
     */
    public static function getPublicRootDir(): string
    {
        return __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'';
    }

    public function getAbsolutePath(?string $imageStyle = null): string
    {
        return __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$this->getWebPath($imageStyle);
    }

    public function __toString(): string
    {
        return $this->name;
    }

    #[JMS\VirtualProperty]
    public function getWebPath(?string $imageStyle = null): string
    {
        $path = '';
        $filesDir = 'files'.DIRECTORY_SEPARATOR;
        if ($this->folder instanceof FileFolder) {
            $path .= $this->folder->getPath();
        }

        if (null === $imageStyle && isset($this->imageStyles['default'])) {
            $imageStyle = 'default';
        }

        if (null !== $imageStyle && isset($this->imageStyles[$imageStyle]) && 'image' === $this->type && 'gif' !== $this->getExt()) {
            $stylePath = $path.sprintf('[%s]', $imageStyle).$this->getPath();
            $styleAbsolutePath = static::getPublicRootDir().$stylePath;
            if (file_exists($styleAbsolutePath)) {
                return $filesDir.$stylePath;
            }
            
            // Create new image with style
            if (!file_exists(static::getPublicRootDir().$path.$this->getPath())) {
                return '';
            }

            try {
                $image = new ImageResize(static::getPublicRootDir().$path.$this->getPath());
                foreach ($this->imageStyles[$imageStyle] as $action => $params) {
                    switch ($action) {
                        case 'crop':
                            $image->crop($params['width'], $params['height'], false);
                            break;

                        case 'resize':
                            if (isset($params['width']) && isset($params['height'])) {
                                $image->resize($params['width'], $params['height']);
                            } elseif (isset($params['width'])) {
                                $image->resizeToWidth($params['width']);
                            } elseif (isset($params['height'])) {
                                $image->resizeToHeight($params['height']);
                            }
                            
                            break;
                    }
                }
                
                $image->save($styleAbsolutePath);

                return $filesDir.$stylePath;
            } catch (\Throwable) {
                // Nothing to do
            }
        }

        return $filesDir.$path.$this->getPath();
    }

    #[JMS\VirtualProperty]
    public function getStyleWebPath(): array
    {
        $styles = $this->getImageStyles();
        $styleWebPaths = [];
        foreach (array_keys($styles) as $name) {
            $styleWebPaths[$name] = $this->getWebPath($name);
        }

        return $styleWebPaths;
    }

    public function getAbsoluteWebPath(): string
    {
        return (isset($_SERVER['HTTPS']) && 'on' === $_SERVER['HTTPS'] ? 'https' : 'http').sprintf('://%s/', $_SERVER[HTTP_HOST]).$this->getWebPath();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDownloadCount(): int
    {
        return $this->downloadCount;
    }

    public function setDownloadCount(int $downloadCount): self
    {
        $this->downloadCount = $downloadCount;

        return $this;
    }

    public function addDownload(): self
    {
        ++$this->downloadCount;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getExt(): string
    {
        return $this->ext;
    }

    public function setExt(string $ext): self
    {
        $this->ext = $ext;

        return $this;
    }

    public function getPath(): string
    {
        if (null == $this->path) {
            $this->path = $this->slug.'.'.$this->ext;
        }

        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getSize(): int
    {
        if (null === $this->size) {
            $this->size = filesize($this->getAbsolutePath());
        }

        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getSizeString(): string
    {
        $size = $this->getSize();

        if ($size > (1024 * 1024)) {
            return number_format($size / (1024 * 1024), 3, ',', ' ').' Mo';
        } elseif ($size > 1024) {
            return number_format($size / 1024, 0, ',', ' ').' Ko';
        }

        return $size.' o';
    }

    public function getImageStyles(): array
    {
        return $this->imageStyles ?? [];
    }

    public function setImageStyles(array $imageStyles): self
    {
        $this->imageStyles = $imageStyles;

        return $this;
    }

    public function removeImageStyles(): void
    {
        foreach (array_keys($this->imageStyles) as $name) {
            if (\file_exists($this->getAbsolutePath($name))) {
                unlink($this->getAbsolutePath($name));
            }
        }
    }

    public function getOptions(): array
    {
        return $this->options ?? [];
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getOption(string $name): ?string
    {
        if (null === $this->options) {
            return null;
        }

        return $this->options[$name] ?? null;
    }

    public function setOption(string $name, string $value): self
    {
        $this->options[$name] = trim($value);

        return $this;
    }

    public function setBase64(string $base64): self
    {
        $fileData = explode(',', $base64);
        $fileType = explode('/', $fileData[0]);
        $fileExt = str_replace(';base64', '', $fileType[1]);

        $this->setName(\uniqid('img_').'.'.$fileExt);
        $this->setExt($fileExt);

        $filePath = self::getPublicRootDir().$this->name;

        $this->file = new SfFile($filePath, false);
        $file = $this->file->openFile('a+');
        $file->fwrite(base64_decode($fileData[1]));
        
        $this->path = $this->name;
        $this->setType(self::TYPE_IMAGE);

        return $this;
    }

    #[JMS\VirtualProperty()]
    public function getImageOrientation(): int
    {
        if (self::TYPE_IMAGE !== $this->type) {
            return 0;
        }

        // If exif_read_data exists
        if (!\function_exists('exif_read_data')) {
            return 0;
        }

        try {
            $exif = \exif_read_data($this->getAbsolutePath());
            if (isset($exif['Orientation'])) {
                return $exif['Orientation'];
            }
        } catch (\Throwable) {
            return 0;
        }

        return 0;
    }
}
