<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Repository\SitemapUrlRepository;

#[ORM\Entity(repositoryClass: SitemapUrlRepository::class)]
class SitemapUrl implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column]
    #[Assert\Url(protocols: ['http', 'https'])]
    private string $loc;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\LessThanOrEqual('today')]
    private \DateTimeInterface $lastMod;

    #[ORM\Column(type: Types::DECIMAL, precision: 4, scale: 2)]
    #[Assert\Range(min: 0, max: 1)]
    private string $priority = '1';

    public function __construct()
    {
        $this->lastMod = new \DateTime();
    }

    public function __toString(): string
    {
        return $this->loc;
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getLoc(): string
    {
        return $this->loc ?? '';
    }

    public function setLoc(string $loc): self
    {
        $this->loc = $loc;

        return $this;
    }

    public function getLastMod(): \DateTimeInterface
    {
        return $this->lastMod;
    }

    public function setLastMod(\DateTimeInterface $lastMod): self
    {
        $this->lastMod = $lastMod;

        return $this;
    }

    public function getPriority(): float
    {
        return (float) $this->priority;
    }

    public function setPriority(float $priority): self
    {
        $this->priority = (string) $priority;

        return $this;
    }
}
