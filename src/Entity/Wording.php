<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Repository\WordingRepository;

#[ORM\Entity(repositoryClass: WordingRepository::class)]
#[ORM\Table(name: 'base_wording')]
class Wording implements \Stringable
{
    public const TYPE_TEXT = 'text';
    
    public const TYPE_LONG_TEXT = 'long-text';
    
    public const TYPE_HTML = 'html';
    
    public const TYPE_NUMBER = 'number';

    public static function getTypes(): array
    {
        return [
            self::TYPE_TEXT,
            self::TYPE_LONG_TEXT,
            self::TYPE_HTML,
            self::TYPE_NUMBER,
        ];
    }

    #[ORM\Id]
    #[ORM\Column(length: 100)]
    private string $page;

    #[ORM\Id]
    #[ORM\Column(length: 100)]
    private string $name;

    #[ORM\Column(length: 12)]
    private string $type;

    #[ORM\Column(type: Types::TEXT, name: 'text')]
    #[Assert\NotBlank()]
    private null|string $content = null;

    public function __toString(): string
    {
        return $this->content ?? '';
    }

    public function getPage(): string
    {
        return $this->page;
    }

    public function setPage(string $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): null|string
    {
        return $this->content;
    }

    public function setContent(null|string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
