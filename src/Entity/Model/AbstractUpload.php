<?php

namespace Tigris\BaseBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Attribute\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\HasLifecycleCallbacks]
abstract class AbstractUpload
{
    #[ORM\Column(nullable: true)]
    protected ?string $filePath = '';

    protected bool $deleteFile = false;

    #[Assert\File(maxSize: '20M')]
    #[JMS\Exclude()]
    #[Ignore]
    protected UploadedFile|File|null $file = null;

    protected ?string $tmpPath = null;

    #[JMS\Accessor(getter: 'getWebPath')]
    protected $webPath;

    abstract protected function getUploadDir(): string;

    public function getWebPath()
    {
        return $this->getUploadDir().$this->getFilePath();
    }

    public function setFile(UploadedFile|File|null $file): self
    {
        if ($file instanceof UploadedFile) {
            $this->tmpPath = $this->filePath;
            $this->filePath = '';
            $this->file = $file;
        }

        return $this;
    }

    public function getFile(): ?File
    {
        if (!$this->file instanceof UploadedFile && is_file($this->getAbsolutePath())) {
            return new File($this->getAbsolutePath());
        }

        return $this->file;
    }

    protected function getPublicRootDir(): string
    {
        return __DIR__.'/../../../../../../public/';
    }

    protected function getFileRootDir(): string
    {
        return $this->getPublicRootDir().$this->getUploadDir();
    }

    public function getAbsolutePath(): string
    {
        return $this->getFileRootDir().$this->filePath;
    }

    public function getFilePath(): string
    {
        return $this->filePath ?? '';
    }

    public function isValid(): bool
    {
        return $this->file instanceof UploadedFile;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function preUpload(): void
    {
        if ($this->file instanceof UploadedFile) {
            $this->filePath = md5(uniqid()).'.'.$this->file->guessExtension();
            if (file_exists($this->getFileRootDir().$this->filePath)) {
                $this->preUpload();

                return;
            }
        }
    }

    #[ORM\PostPersist]
    #[ORM\PostUpdate]
    public function upload(): void
    {
        if (is_file($this->getFileRootDir().$this->tmpPath)) {
            unlink($this->getFileRootDir().$this->tmpPath);
        }

        if ($this->file instanceof UploadedFile) {
            $this->file = $this->file->move(
                $this->getFileRootDir(),
                $this->filePath
            );
        }

        $this->file = null;
    }

    #[ORM\PreRemove]
    public function removeFile(): void
    {
        if (is_file($this->getFileRootDir().$this->filePath)) {
            unlink($this->getFileRootDir().$this->filePath);
        }
    }

    public function getDeleteFile(): bool
    {
        return $this->deleteFile;
    }

    public function setDeleteFile(bool $deleteFile): self
    {
        $this->deleteFile = $deleteFile;

        return $this;
    }
}
