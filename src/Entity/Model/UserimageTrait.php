<?php

namespace Tigris\BaseBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Entity\File;

trait UserimageTrait
{
    #[ORM\OneToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    protected File|null $userimage = null;

    public function getUserimage(): File|null
    {
        return $this->userimage;
    }

    public function setUserimage(File $userimage = null): self
    {
        $this->userimage = $userimage;

        return $this;
    }

    public function hasUserimage(): bool
    {
        return null !== $this->userimage;
    }
}
