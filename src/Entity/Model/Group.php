<?php

namespace Tigris\BaseBundle\Entity\Model;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Tigris\BaseBundle\Repository\GroupRepository;

#[ORM\MappedSuperclass(repositoryClass: GroupRepository::class)]
class Group implements \Stringable
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\Column]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    public function __construct(
        #[ORM\Column(length: 20)]
        protected string $name = '',
        #[ORM\Column(type: Types::JSON)]
        protected array $roles = []
    ) {}

    public function __toString(): string
    {
        return $this->name;
    }

    public function addRole($role): self
    {
        if (!$this->hasRole($role)) {
            $this->roles[] = strtoupper((string) $role);
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function hasRole($role): bool
    {
        return in_array(strtoupper((string) $role), $this->roles, true);
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function removeRole($role): self
    {
        if (false !== $key = array_search(strtoupper((string) $role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
