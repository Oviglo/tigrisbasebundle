<?php

namespace Tigris\BaseBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Repository\UserRepository;

#[ORM\MappedSuperclass(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'])]
#[UniqueEntity(fields: ['username'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface, \Stringable
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    final public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    
    final public const ROLE_DEFAULT = 'ROLE_DEFAULT';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: 60)]
    protected string $username;

    #[ORM\Column]
    #[Assert\Email]
    #[Assert\NotBlank]
    #[JMS\Groups(['Admin'])]
    protected string $email;

    #[ORM\Column]
    protected bool $enabled = true;

    #[JMS\Exclude]
    protected $deletedAt;

    #[JMS\Exclude]
    protected $updatedAt;

    #[ORM\Column]
    protected string $fullName = '';

    #[ORM\Column(type: Types::JSON)]
    #[JMS\AccessType(type: 'public_method')]
    protected $roles = [];

    #[ORM\ManyToMany(targetEntity: Group::class)]
    #[ORM\JoinTable(name: 'user_group')]
    protected Collection $groups;

    #[ORM\Column(nullable: true)]
    #[JMS\Exclude]
    protected ?bool $enableNotificationEmail = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $onlineAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $lastLogin = null;

    #[ORM\Column]
    #[JMS\Exclude]
    protected ?string $password = null;

    #[JMS\Exclude]
    protected ?string $plainPassword = null;

    #[ORM\Column(nullable: true)]
    protected ?bool $isVerified = true;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getUsername();
    }

    #[JMS\VirtualProperty]
    public function isOnline(): bool
    {
        if (!$this->onlineAt instanceof \DateTime) {
            return false;
        }

        $date = new \DateTime();
        $date->sub(new \DateInterval('PT5M'));

        return $date < $this->onlineAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function setUsername(string $username): self
    {
        $username = strip_tags(trim($username));
        if ('' === $this->fullName) {
            $this->fullName = $username;
        }

        $this->username = $username;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password ?? '';
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password ?? '';

        return $this;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setEnabled(bool $boolean): self
    {
        if (!$boolean && $this->hasRole(static::ROLE_SUPER_ADMIN)) {
            throw new \LogicException('Super admin can not be disabled');
        }

        $this->enabled = $boolean;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;

        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }

        // we need to make sure to have at least one role
        $roles[] = 'ROLE_USER';

        return array_values(array_unique($roles));
    }

    public function addRole(string $role): self
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function hasRole(string $role): bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function removeRole(string $role): self
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function isSuperAdmin(): bool
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN);
    }

    public function setSuperAdmin(bool $boolean): self
    {
        if ($boolean) {
            $this->addRole(static::ROLE_SUPER_ADMIN);
        } else {
            $this->removeRole(static::ROLE_SUPER_ADMIN);
        }

        return $this;
    }

    public function getEnableNotificationEmail(): bool
    {
        return $this->enableNotificationEmail;
    }

    public function isEnableNotificationEmail(): bool
    {
        return $this->enableNotificationEmail ?? false;
    }

    public function setEnableNotificationEmail(bool $enableNotificationEmail): self
    {
        $this->enableNotificationEmail = $enableNotificationEmail;

        return $this;
    }

    public function getOnlineAt(): ?\DateTime
    {
        return $this->onlineAt;
    }

    public function setOnlineAt(\DateTime $onlineAt): self
    {
        $this->onlineAt = $onlineAt;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username ?? '';
    }

    public function setLastLogin(?\DateTime $time = null): self
    {
        $this->lastLogin = $time;

        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = [];

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function getGroupNames(): array
    {
        $names = [];
        foreach ($this->getGroups() as $group) {
            $names[] = $group->getName();
        }

        return $names;
    }

    public function hasGroup(string $name): bool
    {
        return in_array($name, $this->getGroupNames());
    }

    public function addGroup(Group $group): self
    {
        if (!$this->getGroups()->contains($group)) {
            $this->getGroups()->add($group);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->getGroups()->contains($group)) {
            $this->getGroups()->removeElement($group);
        }

        return $this;
    }

    public function eraseCredentials(): void
    {
        // $this->password = null;
    }

    public function isEqualTo(UserInterface $user): bool
    {
        if (!$user instanceof self) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        return $this->username === $user->getUsername();
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPlainPassword(): string
    {
        return $this->plainPassword ?? '';
    }

    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function isVerified(): bool
    {
        return $this->isVerified ?? true;
    }

    public function setVerified(bool $isVerified): self
    {
        if (!$isVerified && $this->isSuperAdmin()) {
            throw new \LogicException('Super admin can not be unverified');
        }

        $this->isVerified = $isVerified;

        return $this;
    }
}
