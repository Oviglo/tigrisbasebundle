<?php

namespace Tigris\BaseBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Tigris\BaseBundle\Repository\DataExportRepository;

#[ORM\Entity(repositoryClass: DataExportRepository::class)]
#[ORM\HasLifecycleCallbacks]
class DataExport
{
    use TimestampableEntity;

    final public const STATUS_PENDING = 'pending';

    final public const STATUS_FINISH = 'finish';

    final public const STATUS_ERROR = 'error';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 80)]
    private string $type;

    private string $typeLabel = '';

    #[ORM\Column(length: 15)]
    private string $status;

    private string $statusLabel = '';

    #[ORM\Column(length: 180)]
    private string $filePath;

    #[ORM\Column(length: 50)]
    private string $format;

    private string $formatLabel = '';

    #[ORM\Column(type: Types::SIMPLE_ARRAY)]
    private array $criteria = [];

    #[ORM\Column(nullable: true)]
    private ?string $sendToMail = null;

    #[ORM\Column(nullable: true)]
    private ?string $errorMessage = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTypeLabel(): string
    {
        return $this->typeLabel;
    }

    public function setTypeLabel(string $typeLabel): self
    {
        $this->typeLabel = $typeLabel;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusLabel(): string
    {
        return $this->statusLabel;
    }

    public function setStatusLabel(string $statusLabel): self
    {
        $this->statusLabel = $statusLabel;

        return $this;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getAbsoluteFolder(): string
    {
        return __DIR__.'/../../../../public/data_export/';
    }

    public function getAbsolutePath(): string
    {
        return $this->getAbsoluteFolder().$this->filePath;
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    #[ORM\PreRemove]
    public function preRemove(): void
    {
        if (is_file($this->getAbsolutePath())) {
            unlink($this->getAbsolutePath());
        }
    }

    public function getCriteria(): array
    {
        return $this->criteria ?? [];
    }

    public function setCriteria(array $criteria): self
    {
        $this->criteria = $criteria;

        if (isset($criteria['exportType'])) {
            $this->type = $criteria['exportType'];
        }

        return $this;
    }

    public function getFormatLabel(): string
    {
        return $this->formatLabel;
    }

    public function setFormatLabel(string $formatLabel): self
    {
        $this->formatLabel = $formatLabel;

        return $this;
    }

    public function getSendToMail(): ?string
    {
        return $this->sendToMail;
    }

    public function setSendToMail(?string $sendToMail): self
    {
        $this->sendToMail = $sendToMail;

        return $this;
    }

    public function setErrorMessage(?string $errorMessage): self
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
}
