<?php

namespace Tigris\BaseBundle\Form\ConfiguredForm\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\ConfiguredForm\ArrayToForm;

class ConfiguredFormType extends AbstractType
{
    public function __construct(private readonly ArrayToForm $arrayToForm)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $fields = $options['fields'];

        $this->arrayToForm->buildForm($builder, $fields);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'fields' => [],
            'label' => false,
        ]);
    }
}