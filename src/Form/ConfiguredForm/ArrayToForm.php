<?php

namespace Tigris\BaseBundle\Form\ConfiguredForm;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Tigris\BaseBundle\Form\Type\WysiwygType;

class ArrayToForm
{
    public function buildForm(FormBuilderInterface $builder, array $fields, string $name = 'form'): FormBuilderInterface
    {
        foreach ($fields as $name => $field) {
            $options = array_merge([
                'label' => $field['label'] ?? false,
                'required' => $field['required'] ?? false,
                'attr' => $field['attr'] ?? false,
            ], $field['options']);

            // Constraints
            if ($options['required']) {
                $options['constraints'] = [
                    new NotBlank(),
                ];
            }

            switch ($field['type']) {
                case 'text':
                    $builder->add($name, TextType::class, $options);

                    break;

                case 'email':
                    $builder->add($name, EmailType::class, $options);

                    break;

                case 'color':
                    $builder->add($name, ColorType::class, $options);

                    break;

                case 'language':
                    $builder->add($name, LanguageType::class, $options);

                    break;

                case 'phone':
                    $builder->add($name, TelType::class, $options);

                    break;

                case 'number':
                    $builder->add($name, NumberType::class, $options);

                    break;

                case 'url':
                    $builder->add($name, UrlType::class, $options);

                    break;

                case 'textarea':
                    $builder->add($name, TextareaType::class, $options);

                    break;

                case 'wysiwyg':
                    $options['tools'] = 'maxi';
                    $builder->add($name, WysiwygType::class, $options);

                    break;

                case 'checkbox':
                    $builder->add($name, CheckboxType::class, $options);

                    break;

                case 'radio':
                    $builder->add($name, RadioType::class, $options);

                    break;

                case 'file':
                    $builder->add($name, FileType::class, $options);

                    break;

                case 'choices':
                    $choices = $field['choices'];
                    $options['choices'] = $choices;
                    $options['multiple'] = $field['multiple'] ?? false;
                    $options['expanded'] = $field['expanded'] ?? false;
                    $builder->add($name, ChoiceType::class, $options);

                    break;

                case 'date':
                    // $options['format'] = 'dd/MM/y';
                    $builder->add($name, DateType::class, $options);
                    $builder->get($name)->addModelTransformer($this->getDateModelTransformer());

                    break;

                case 'datetime':
                    // $options['format'] = 'dd/MM/y HH:mm';
                    $builder->add($name, DateTimeType::class, $options);
                    $builder->get($name)->addModelTransformer($this->getDateModelTransformer());

                    break;

                case 'entity':
                    $options['class'] = $field['class'];
                    $options['multiple'] = $field['multiple'] ?? false;
                    $options['expanded'] = $field['expanded'] ?? false;
                    $options['choice_value'] = 'id';
                    $builder->add($name, EntityType::class, $options);
                    $builder->get($name)->addModelTransformer($this->getEntityModelTransformer());

                    break;

                default:
                    if (isset($field['multiple'])) {
                        $options['multiple'] = $field['multiple'] ?? false;
                    }

                    $builder->add($name, $field['type'], $options);
                    $builder->get($name)->addModelTransformer($this->getEntityModelTransformer());

                    break;
            }
        }

        return $builder;
    }

    private function getEntityModelTransformer(): CallbackTransformer
    {
        return new CallbackTransformer(
            fn ($data) => $data,
            function ($data) {
                $result = [];
                $data = ($data instanceof ArrayCollection) ? $data->toArray() : $data;

                if (is_array($data)) { // Multiple
                    foreach ($data as $value) {
                        $result[] = is_object($value) ? (int) $value->getId() : $value;
                    }

                    return $result;
                }

                return is_object($data) ? (int) $data->getId() : $data;
            }
        );
    }

    private function getDateModelTransformer(): CallbackTransformer
    {
        return new CallbackTransformer(
            function (array $data) : ?\DateTime {
                $date = new \DateTime($data['date']);
                $date->setTimezone(new \DateTimeZone($data['timezone']));
                return $date;
            },
            fn ($data) => $data
        );
    }
}
