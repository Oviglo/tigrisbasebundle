<?php

namespace Tigris\BaseBundle\Form\ConfiguredForm;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\NodeParentInterface;
use Symfony\Component\Config\Definition\Builder\VariableNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;


class Definition
{
    public static function set(NodeParentInterface|NodeBuilder|ArrayNodeDefinition|VariableNodeDefinition|NodeDefinition|null $node): NodeParentInterface|NodeBuilder|ArrayNodeDefinition|VariableNodeDefinition|NodeDefinition|null
    {
        return $node->arrayNode('fields')
            ->useAttributeAsKey('id')
            ->arrayPrototype()
                ->children()
                    ->scalarNode('type')->end()
                    ->scalarNode('label')->end()
                    ->booleanNode('required')->defaultFalse()->end()
                    ->booleanNode('translatable')->defaultFalse()->end()
                    ->booleanNode('expanded')->end()
                    ->scalarNode('class')->end()
                    ->booleanNode('multiple')->end()
                    ->arrayNode('choices')
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('value')
                        ->prototype('scalar')->end()
                    ->end()
                    ->arrayNode('options')
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('value')
                        ->prototype('scalar')->end()
                    ->end()
                    ->arrayNode('attr')
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('value')
                        ->prototype('scalar')->end()
                    ->end()

                ->end()
            ->end()
        ->end();
    }
}