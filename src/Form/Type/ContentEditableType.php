<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContentEditableType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'content_editable';
    }

    public function getParent(): ?string
    {
        return TextareaType::class;
    }
}
