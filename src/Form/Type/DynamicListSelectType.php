<?php

namespace Tigris\BaseBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\Transformer\EntityHiddenTransformer;

abstract class DynamicListSelectType extends AbstractType
{
    public function __construct(protected readonly EntityManagerInterface $em, protected readonly string $className)
    {
    }

    public function getParent(): string
    {
        return EntityType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => $this->className,
            // 'choices' => [], @TODO changer la requête en fonction de la selection
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->resetModelTransformers();
        $builder->resetViewTransformers();
        $builder->addModelTransformer(new EntityHiddenTransformer($this->em, $this->className));
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $data = $form->getData();

        $view->vars['entityData'] = $data;
    }
}
