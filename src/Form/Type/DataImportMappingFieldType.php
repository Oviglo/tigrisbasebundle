<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Tigris\BaseBundle\Manager\DataImportManager;

class DataImportMappingFieldType extends AbstractType
{
    public function __construct(private readonly DataImportManager $dataImportManager)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('to', StaticType::class, [
                'label' => 'data.importer.to',
            ])

            ->add('format', ChoiceType::class, [
                'label' => 'data.importer.format',
                'choices' => [
                    'data.importer.format_string' => 'string',
                    'data.importer.format_bool' => 'bool',
                    'data.importer.format_date' => 'date',
                    'data.importer.format_datetime' => 'datetime',
                ]
            ]);

        $dataImportManager = $this->dataImportManager;

        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) use ($dataImportManager): void {
                $dataImport = $event->getForm()->getParent()->getParent()->getData();

                $headerItems = array_merge([
                    'data.importer.dont_import' => null,
                ], array_filter($dataImportManager->getHeader($dataImport), fn ($value): bool => !is_null($value) && $value !== ''));

                $event->getForm()->add('from', ChoiceType::class, [
                    'label' => 'data.importer.from',
                    'choices' => $headerItems,
                    'required' => true,
                ]);
            }
        );
    }
}
