<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ConfigType extends AbstractType
{
    public function __construct(
        #[Autowire('tigris.base.active_user_registration')]
        private readonly bool $registrationEnabled)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'config.title',
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'config.description',
                'attr' => [
                    'rows' => 4,
                ],
            ])

            ->add('noreply_mail', EmailType::class, [
                'label' => 'config.noreply_mail',
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])

            ->add('enable_info_message', CheckboxType::class, [
                'label' => 'config.enable_info_message',
                'required' => false,
            ])

            ->add('info_message', WysiwygType::class, [
                'label' => 'config.info_message',
                'required' => false,
            ])
        ;

        if ($this->registrationEnabled) {
            $builder
                ->add('enable_registration_recaptcha', CheckboxType::class, [
                    'label' => 'config.enable_registration_recaptcha',
                    'required' => false,
                ])
            ;
        }

        $builder
            ->add('rgpd', RGPDConfigType::class, [
                'label' => 'config.rgpd',
                'required' => false,
            ]);
    }
}
