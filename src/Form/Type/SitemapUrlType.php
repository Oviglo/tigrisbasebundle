<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class SitemapUrlType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('loc', UrlType::class, [
                'label' => 'seo.sitemap.url',
            ])
            ->add('lastMod', DateType::class, [
                'label' => 'seo.sitemap.last_mod',
            ])
            ->add('priority', NumberType::class, [
                'label' => 'seo.sitemap.priority',
                'html5' => true,
                'attr' => [
                    'step' => 0.10,
                ],
            ])
        ;
    }
}
