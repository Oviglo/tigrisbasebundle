<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PostSetDataEvent;
use Symfony\Component\Form\Event\SubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\File;

class FileEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (PostSetDataEvent $event): void {
            $file = $event->getData();
            $form = $event->getForm();
            if (!$file instanceof File) {
                return;
            }

            if (File::TYPE_IMAGE === $file->getType()) {
                $form->add('alt', TextType::class, [
                    'label' => 'file.option.alt',
                    'help' => 'file.option.alt_help',
                    'mapped' => false,
                    'data' => $file->getOption('alt'),
                ]);
            }
        });

        $builder->addEventListener(FormEvents::SUBMIT, function (SubmitEvent $event): void {
            $file = $event->getData();
            $form = $event->getForm();
            if (!$file instanceof File) {
                return;
            }

            if (File::TYPE_IMAGE === $file->getType()) {
                $file->setOption('alt', $form->get('alt')->getData());
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => File::class,
        ]);
    }
}
