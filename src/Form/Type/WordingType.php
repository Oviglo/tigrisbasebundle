<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\Wording;

class WordingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $form = $event->getForm();
            $data = $event->getData();

            if (!$data instanceof Wording) {
                return;
            }

            $config = match ($data->getType()) {
                Wording::TYPE_TEXT => ['type' => TextType::class, 'options' => []],
                Wording::TYPE_LONG_TEXT => ['type' => TextareaType::class, 'options' => ['attr' => ['rows' => 5]]],
                Wording::TYPE_HTML => ['type' => WysiwygType::class, 'options' => ['tools' => 'maxi']],
                Wording::TYPE_NUMBER => ['type' => NumberType::class, 'options' => []],
                default => null,
            };

            if (null !== $config) {
                $form->add('content', $config['type'], array_merge([
                    'label' => false,
                ], $config['options']));
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Wording::class,
        ]);
    }
}
