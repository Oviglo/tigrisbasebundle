<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class RGPDConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('enable', CheckboxType::class, [
                'label' => 'rgpd.config.enable',
                'required' => false,
            ])
            ->add('privacyUrl', UrlType::class, [
                'label' => 'rgpd.config.privacy_url',
                'required' => false,
            ])
            ->add('services', ChoiceType::class, [
                'label' => 'rgpd.config.services',
                'multiple' => true,
                'choices' => [
                    'rgpd.google_analytics' => 'google_analytics',
                    'rgpd.facebook_pixel' => 'facebook_pixel',
                ],
            ])
        ;
    }
}
