<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\DataExport;
use Tigris\BaseBundle\Manager\DataExportManager;

class DataExportType extends AbstractType
{
    public function __construct(private readonly DataExportManager $dataExportManager)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $exporters = $this->dataExportManager->getExporters();
        $exporterChoices = array_combine(
            array_map(fn ($value): string => 'data.exporter.'.$value, array_keys($exporters)),
            array_keys($exporters)
        );

        $builder
            ->add('format', ChoiceType::class, [
                'label' => 'data.exporter.format',
                'choices' => [
                    'data.exporter.xls' => 'xls',
                    'data.exporter.facebook_market' => 'facebook_market',
                ],
            ])
            ->add('sendToMail', EmailType::class, [
                'label' => 'data.exporter.send_to_mail',
                'required' => false,
            ])
            ->add('criteria', HiddenType::class)
        ;

        $builder->get('criteria')->addModelTransformer(new CallbackTransformer(
            fn ($criteriaAsArray) => json_encode($criteriaAsArray, JSON_THROW_ON_ERROR),
            fn ($criteriaAsString): mixed => json_decode((string) $criteriaAsString, true, 512, JSON_THROW_ON_ERROR)
        ));

        // Adding type choice if type is empty
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($exporterChoices): void {
            $dataExport = $event->getData();
            $criteria = $dataExport->getCriteria();
            $form = $event->getForm();
            $exportType = $criteria['exportType'] ?? null;

            if (empty($exportType)) {
                $form->add('type', ChoiceType::class, [
                    'label' => 'data.exporter.type.label',
                    'choices' => $exporterChoices,
                ]);
            } else {
                $form->add('type', HiddenType::class);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DataExport::class,
        ]);
    }
}
