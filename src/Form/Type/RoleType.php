<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\RoleEvent;

class RoleType extends AbstractType
{
    public function __construct(private readonly EventDispatcherInterface $eventDispatcher)
    {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $event = new RoleEvent();
        $this->eventDispatcher->dispatch($event, Events::LOAD_ROLES);

        $choices = [];
        foreach ($event->getRoles() as $role) {
            $choices['role.'.strtolower((string) $role)] = $role;
        }

        $resolver->setDefaults([
            'choices' => $choices,
            'expanded' => true,
            'multiple' => true,
        ]);
    }

    public function getParent(): ?string
    {
        return ChoiceType::class;
    }
}
