<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecaptchaType extends AbstractType
{
    public function __construct(
        private readonly string $siteKey,
        private readonly string $host,
        private readonly bool $enabled
    ) {
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['site_key'] = $this->siteKey;
        $view->vars['host'] = $this->host;
        $view->vars['enabled'] = $this->enabled;
        $view->vars['action_name'] = $options['action_name'];
        $view->vars['script_nonce_csp'] = $options['script_nonce_csp'] ?? '';
        $view->vars['locale'] = $options['locale'] ?? 'en';
    }

    public function getParent(): string
    {
        return HiddenType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'recaptcha';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'mapped' => false,
            'site_key' => null,
            'host' => null,
            'action_name' => 'homepage',
            'locale' => 'en',
            'script_nonce_csp' => '',
        ]);
    }
}
