<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\FileFolder;

class FileFolderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'file_folder.name',
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event): void {
                $folder = $event->getData();

                $event->getForm()->add('parent', EntityType::class, [
                    'class' => FileFolder::class,
                    'multiple' => false,
                    'required' => false,
                    'label' => 'file_folder.parent',
                    'empty_data' => null,
                    'query_builder' => function ($er) use ($folder) {
                        $er = $er->createQueryBuilder('f');
                        if (is_object($folder) && $folder->getId()) {
                            $er->andWhere('f.id != '.$folder->getId());
                            $er->andWhere('f.level <= '.$folder->getLevel());
                        }

                        return $er->orderBy('f.left', 'ASC');
                    },
                    'help' => 'file_folder.parent_help',
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FileFolder::class,
        ]);
    }
}
