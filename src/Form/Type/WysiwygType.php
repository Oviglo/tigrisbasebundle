<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Utils\Utils;

class WysiwygType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => ['class' => '', 'data-name' => ''],
            'tools' => 'mini',
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer(new CallbackTransformer(
            fn ($value): string|array|null|false|int|float => $value ? Utils::htmlFilter(Utils::closeTags($value)) : $value,
            fn ($value) => $value
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $name = $form->getName();

        // $this->wysiwyg->addEditor($name, $editorOptions);
        $view->vars['attr']['class'] = ($view->vars['attr']['class'] ?? '').' wysiwyg';
        $view->vars['attr']['data-name'] = $name;
        $view->vars['attr']['data-tools'] = $options['tools'];
    }

    public function getBlockPrefix(): string
    {
        return 'wysiwyg';
    }

    public function getParent(): ?string
    {
        return TextareaType::class;
    }
}
