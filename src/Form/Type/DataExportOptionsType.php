<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;

class DataExportOptionsType extends FormType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
    }
}
