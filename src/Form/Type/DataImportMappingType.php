<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\DataImport;

class DataImportMappingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('mapping', TableCollectionType::class, [
                'entry_type' => DataImportMappingFieldType::class,
                'allow_add' => false,
                'allow_delete' => false,
                'by_reference' => false,
                'prototype' => true,
            ]);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event): void {
                $data = $event->getData();
                $form = $event->getForm();

                if ($data instanceof DataImport) {
                    $fields = $data->getMapping();

                    $choices = array_map(fn (array $field): string => $field['from'] ?? '', $fields);
                    $choices = array_combine($choices, $choices);
                    $choices['data.importer.nothing'] = null;

                    $form->add('primaryKey', ChoiceType::class, [
                        'label' => 'data.importer.edit',
                        'choices' => $choices,
                    ]);
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DataImport::class,
        ]);
    }
}
