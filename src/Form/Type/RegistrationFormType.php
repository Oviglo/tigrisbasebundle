<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Validator\Constraints\StopForumSpam;
use Tigris\BaseBundle\Validator\Recaptcha;

class RegistrationFormType extends AbstractType
{
    public function __construct(private readonly ConfigManager $configManager) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'registration.email',
                // 'constraints' => new StopForumSpam(),
            ])
            ->add('username', TextType::class, [
                'label' => 'registration.username',
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'mapped' => false,
                'first_options' => ['label' => 'registration.password'],
                'second_options' => ['label' => 'registration.repeat_password'],
            ])

            ->add('securityButton', SecurityButtonType::class, [
                'mapped' => false,
            ])
        ;

        if ($this->configManager->getValue('TigrisBaseBundle.enable_registration_recaptcha', false)) {
            $builder
                ->add('recaptcha', RecaptchaType::class, [
                    'constraints' => [
                        new Recaptcha(),
                    ],
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
