<?php

namespace Tigris\BaseBundle\Form\Type;

use App\Entity\Group;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\Model\User;

class UserType extends AbstractType
{
    public function __construct(private readonly Security $security)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', null, [
                'label' => 'user.username',
                'attr' => [
                    'autocomplete' => 'off',
                    'v-validate' => "'required'",
                ],
            ])
            ->add('email', null, [
                'label' => 'user.email',
                'attr' => [
                    'autocomplete' => 'off',
                    'v-validate' => "'required|email'",
                ],
            ])

            ->add('enableNotificationEmail', CheckboxType::class, [
                'label' => 'user.enable_email_notification',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                ],
            ])

            ->add('groups', EntityType::class, [
                'label' => 'user.group.groups',
                'class' => Group::class,
                'required' => false,
                'multiple' => true,
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'user.enabled',
                'required' => false,
            ])
        ;

        $factory = $builder->getFormFactory();
        $security = $this->security;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($factory, $security): void {
                $user = $event->getData();
                if (null === $user) {
                    return;
                }
                
                $required = false;
                $passwordLabel = 'user.new_password';
                if (!$user->getUsername()) {
                    $required = true;
                    $passwordLabel = 'user.password';
                }

                $event->getForm()->add(
                    $factory->createNamed(
                        'plainPassword',
                        TextType::class,
                        [],
                        ['auto_initialize' => false, 'label' => $passwordLabel, 'required' => $required, 'attr' => ['autocomplete' => 'off']]
                    )
                );
                // Cannot disable super admin
                if ($user->isSuperAdmin()) {
                    $event->getForm()->remove('enabled');
                }

                if ($security->isGranted('ROLE_SUPER_ADMIN') && !$user->isSuperAdmin()) {
                    $event->getForm()->add('roles', ChoiceType::class, [
                        'label' => 'user.roles',
                        'multiple' => true,
                        'expanded' => true,
                        'choices' => [
                            'user.role_admin' => 'ROLE_ADMIN',
                        ],
                    ]);
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
