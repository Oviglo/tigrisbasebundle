<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Form\Transformer\UploadFileModelTransformer;
use Tigris\BaseBundle\Form\Transformer\UploadFileViewTransformer;

class UploadFileType extends AbstractType
{
    public function __construct(private readonly UploadFileViewTransformer $viewTransformer, private readonly UploadFileModelTransformer $modelTransformer)
    {
        $order = [];
        $this->viewTransformer->setOrder($order);
        $this->modelTransformer->setOrder($order);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this->modelTransformer);
        $builder->addViewTransformer($this->viewTransformer);

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => File::class,
            'multiple' => false,
            'type' => 'advanced',
            'preview' => true,
            'choice_value' => 'id',
            'acceptFiles' => "/(\.|\/)(gif|jpe?g|png)$/i",
        ]);

        parent::configureOptions($resolver);
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['attr']['class'] = ($view->vars['attr']['class'] ?? '').' '.$options['type'];
        $view->vars['type'] = $options['type'];
        $view->vars['preview'] = $options['preview'];
        $view->vars['acceptFiles'] = $options['acceptFiles'];
    }

    public function getBlockPrefix(): string
    {
        return 'uploadfile';
    }

    public function getParent(): ?string
    {
        return EntityType::class;
    }
}
