<?php

namespace Tigris\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;
use Tigris\BaseBundle\Security\SecurityButton;

class SecurityButtonType extends AbstractType
{
    public function __construct(private readonly SecurityButton $securityButton) {}

    public function getParent(): string
    {
        return HiddenType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'mapped' => false,
            'constraints' => [
                new NotBlank(message: 'security_button.invalid'),
                new EqualTo($this->securityButton->getToken(), message: 'security_button.invalid'),
            ],
        ]);
    }
}
