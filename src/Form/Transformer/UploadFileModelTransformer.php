<?php

namespace Tigris\BaseBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;
use Tigris\BaseBundle\Repository\FileRepository;

class UploadFileModelTransformer implements DataTransformerInterface
{
    private $order;

    public function __construct(private readonly FileRepository $fileRepository)
    {
    }

    public function setOrder(&$order): void
    {
        $this->order = &$order;
    }

    public function transform($entities): string|array|object|null
    {
        if (is_string($entities)) {
            $entities = $this->fileRepository->findOneById((int) $entities);
        }

        return $entities;
    }

    public function reverseTransform($entities): array|null|object
    {
        if (is_array($entities)) {
            $order = $this->order;
            uasort($entities, function ($entityA, $entityB) use ($order): int {
                $idA = $entityA->getId();
                $idB = $entityB->getId();

                return (array_search($idA, $order, true) < array_search($idB, $order, true)) ? -1 : 1;
            });
        }

        return $entities;
    }
}
