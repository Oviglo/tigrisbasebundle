<?php

namespace Tigris\BaseBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringToDateTimeTransformer implements DataTransformerInterface
{
    public function transform($string): \DateTime
    {
        return new \DateTime($string);
    }

    public function reverseTransform($dateTime)
    {
        return $dateTime;
    }
}
