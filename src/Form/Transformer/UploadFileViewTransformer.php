<?php

namespace Tigris\BaseBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;
use Tigris\BaseBundle\Repository\FileRepository;

class UploadFileViewTransformer implements DataTransformerInterface
{
    private ?array $order = null;

    public function __construct(private readonly FileRepository $fileRepository)
    {
    }

    public function setOrder(&$order): void
    {
        $this->order = &$order;
    }

    /**
     * Transforms an entity or an array of entities to a string or an array of string.
     */
    public function transform($string): mixed
    {
        $entities = null;

        if (is_array($string)) { // Multiple
            $entities = $this->fileRepository->findByIds($string, false, true);
        } else {
            $entities = $this->fileRepository->findOneById((int) $string);
        }

        return $entities;
    }

    /**
     * Transforms a string or array of string to an entity or an array of entities.
     */
    public function reverseTransform($string): mixed
    {
        if (is_array($string)) {
            $this->order = $string;
        }

        return $string;
    }
}
