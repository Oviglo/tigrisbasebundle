<?php

namespace Tigris\BaseBundle\Form\Transformer;

use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class EntityHiddenTransformer.
 *
 * @author  Francesco Casula <fra.casula@gmail.com>
 */
class EntityHiddenTransformer implements DataTransformerInterface
{
    public function __construct(private readonly ObjectManager $objectManager, private readonly string $className, private readonly string $primaryKey = 'id')
    {
    }

    public function getObjectManager(): ObjectManager
    {
        return $this->objectManager;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function getPrimaryKey(): string
    {
        return $this->primaryKey;
    }

    /**
     * Transforms an object (entity) to a string (number).
     *
     * @param object|null $entity
     *
     * @return string
     */
    public function transform(mixed $entity)
    {
        if (null === $entity) {
            return '';
        }

        $method = 'get'.ucfirst($this->getPrimaryKey());

        // Probably worth throwing an exception if the method doesn't exist
        // Note: you can always use reflection to get the PK even though there's no public getter for it
        if (is_iterable($entity)) {
            $entities = [];
            foreach ($entity as $singleEntity) {
                $entities[] = $singleEntity->$method();
            }

            return $entities;
        }

        return $entity->$method();
    }

    /**
     * Transforms a string (number) to an object (entity).
     *
     * @param string $identifier
     *
     * @return object|null
     *
     * @throws TransformationFailedException if object (entity) is not found
     */
    public function reverseTransform(mixed $identifier)
    {
        if ('' === $identifier || '0' === $identifier) {
            return null;
        }

        $entity = $this->getObjectManager()
            ->getRepository($this->getClassName())
            ->findBy([$this->getPrimaryKey() => $identifier])
        ;

        if (null === $entity) {
            throw new TransformationFailedException(sprintf('An entity with ID "%s" does not exist!', $identifier));
        }

        return $entity;
    }
}
