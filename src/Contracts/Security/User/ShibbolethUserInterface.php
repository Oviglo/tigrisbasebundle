<?php

namespace Tigris\BaseBundle\Contracts\Security\User;

interface ShibbolethUserInterface
{
    public function getShibbolethEppn(): ?string;

    public function setShibbolethEppn(string $shibbolethEppn): ShibbolethUserInterface;
}
