<?php

namespace Tigris\BaseBundle\Contracts\Geocoding;

use Tigris\BaseBundle\Service\Geocoding\GeocodingResponse;

interface GeocodingInterface
{
    /**
     * Return the API url.
     */
    public function getUrl(): string;

    /**
     * Call API with address.
     */
    public function searchAddress(string $address): GeocodingResponse;
}
