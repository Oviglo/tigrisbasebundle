<?php

namespace Tigris\BaseBundle\Contracts;

interface ArrayTransformable
{
    public function toArray(): array;
}
