<?php

namespace Tigris\BaseBundle\Contracts\Reader;

use Symfony\Component\HttpFoundation\File\File;

interface ReaderInterface
{
    public function read(File $file): array;

    public function getHeader(): array;
}
