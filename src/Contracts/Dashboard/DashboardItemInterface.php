<?php

namespace Tigris\BaseBundle\Contracts\Dashboard;

interface DashboardItemInterface
{
    public function configure(): array;

    public function data(): array;
}
