<?php

namespace Tigris\BaseBundle\Contracts\Model;

use Tigris\BaseBundle\Entity\File;

interface UserimageInterface
{
    public function setUserimage(File $userimage = null): self;

    public function getUserimage(): File|null;

    public function hasUserimage(): bool;
}
