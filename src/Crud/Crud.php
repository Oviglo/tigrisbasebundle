<?php

namespace Tigris\BaseBundle\Crud;

use Doctrine\ORM\EntityRepository;
use Tigris\BaseBundle\Utils\Utils;

class Crud
{
    final public const FORM_CLASS = 'form_class';
    
    final public const ENTITY_CLASS = 'entity_class';
    
    final public const ROUTE_PREFIX = 'route_prefix';
    
    final public const BREADCRUMBS = 'breadcrumbs';
    
    final public const REPOSITORY = 'repository';
    
    final public const TRANSLATION_PREFIX = 'translation_prefix';
    
    final public const SORTABLE = 'sortable';
    
    final public const VIEW_PATH = 'viewPath';

    final public const INDEX = 'index';
    
    final public const CREATE = 'create';
    
    final public const UPDATE = 'update';
    
    final public const DELETE = 'delete';
    
    final public const SORT = 'sort';

    private string|null $formClass;

    private string|null $entityClass = null;

    private string $routePrefix = '';

    private bool $breadcrumbs = true;

    private string $entityName = '';

    private array $routes = [];

    private string $translationPrefix = '';

    private bool $sortable = false;

    private string $viewPath = '@TigrisBase\admin\crud';

    private EntityRepository|null $repository;

    public function __construct(array $options = [])
    {
        $this->setFormClass($options[static::FORM_CLASS] ?? null);
        $this->setTranslationPrefix($options[static::TRANSLATION_PREFIX] ?? '');
        $this->setRoutePrefix($options[static::ROUTE_PREFIX] ?? '');
        $this->setBreadcrumbs($options[static::BREADCRUMBS] ?? true);
        $this->setRepository($options[static::REPOSITORY] ?? null);
        $this->setSortable($options[static::SORTABLE] ?? false);
        $this->setViewPath($options[static::VIEW_PATH] ?? '@TigrisBase\admin\crud');

        if (isset($options[static::ENTITY_CLASS])) {
            $this->setEntityClass($options[static::ENTITY_CLASS]);
        }
    }

    public static function create(): self
    {
        return new static();
    }

    public function getFormClass(): ?string
    {
        return $this->formClass;
    }

    public function setFormClass(string $formClass = null): self
    {
        $this->formClass = $formClass;

        return $this;
    }

    public function getEntityClass(): ?string
    {
        return $this->entityClass;
    }

    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;
        $parts = $this->cutNamespaceForNames($entityClass);
        $this->setEntityName(\strtolower((string) $parts['entity']));
        if ('' === $this->translationPrefix) {
            $this->setTranslationPrefix(Utils::underscore($parts['bundle']).'.'.Utils::underscore($parts['entity']));
        }

        return $this;
    }

    public function getRoutePrefix(): string
    {
        return $this->routePrefix;
    }

    public function setRoutePrefix(string $routePrefix): self
    {
        $this->routePrefix = $routePrefix;
        $this->generateRoutes();

        return $this;
    }

    public function hasBreadcrumbs(): bool
    {
        return $this->breadcrumbs ?? true;
    }

    public function setBreadcrumbs(bool $breadcrumbs): self
    {
        $this->breadcrumbs = $breadcrumbs;

        return $this;
    }

    public function getRepository(): ?EntityRepository
    {
        return $this->repository;
    }

    public function setRepository(EntityRepository $repository = null): self
    {
        $this->repository = $repository;

        return $this;
    }

    public function getEntityName(): string
    {
        return $this->entityName ?? '';
    }

    public function setEntityName(string $entityName): self
    {
        $this->entityName = $entityName;

        return $this;
    }

    private function generateRoutes(): void
    {
        $this->routes[static::INDEX] = $this->getRoutePrefix().'_index';
        $this->routes[static::CREATE] = $this->getRoutePrefix().'_new';
        $this->routes[static::UPDATE] = $this->getRoutePrefix().'_edit';
        $this->routes[static::DELETE] = $this->getRoutePrefix().'_delete';
        $this->routes[static::SORT] = $this->getRoutePrefix().'_sort';
    }

    public function getRoute(string $name): string
    {
        return $this->routes[$name] ?? $this->getRoutePrefix();
    }

    public function setRoute(string $name, string $route): self
    {
        $this->routes[$name] = $route;

        return $this;
    }

    public function getTranslationPrefix(): string
    {
        return $this->translationPrefix;
    }

    public function setTranslationPrefix(string $translationPrefix): self
    {
        $this->translationPrefix = $translationPrefix;

        return $this;
    }

    private function cutNamespaceForNames(string $namespace): array
    {
        $results = [
            'entity' => '',
            'bundle' => '',
        ];
        $parts = \explode('\\', $namespace);
        if (count($parts) > 2) {
            foreach ($parts as $part) {
                if (\strpos($part, 'Bundle')) {
                    $results['bundle'] = \substr($part, 0, -6);
                    break;
                }
            }

            $results['entity'] = $parts[count($parts) - 1];
        }

        return $results;
    }

    public function getSortable(): bool
    {
        return $this->sortable;
    }

    public function setSortable(bool $sortable): self
    {
        $this->sortable = $sortable;

        return $this;
    }

    public function getViewPath(): string
    {
        return $this->viewPath;
    }

    public function setViewPath(string $viewPath): self
    {
        $this->viewPath = $viewPath;

        return $this;
    }
}
