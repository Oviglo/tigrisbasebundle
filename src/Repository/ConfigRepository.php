<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Config;

class ConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Config::class);
    }

    public function findData(int $begin, int $count, string $order = 'id', bool $rev = true): array
    {
        $query = $this->createQueryBuilder('c')
            ->orderBy('c.'.$order, ($rev) ? 'DESC' : 'ASC')
            ->setFirstResult($begin)
            ->setMaxResults($count)
            ->getQuery()
        ;

        $query->enableResultCache(5);

        return $query->getResult();
    }

    public function getByName(string $name): null|Config
    {
        $query = $this->createQueryBuilder('c')
        ->andWhere("c.name ='".$name."'")
        ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function findAllAsArray(): array
    {
        $query = $this->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
        ;

        $entities = $query->getResult();
        $result = [];

        foreach ($entities as $entity) {
            
            // Explode name and make embed arrays
            $nameArray = explode('.', (string) $entity->getName());
            $temp = &$result;
            
            foreach ($nameArray as $key) {
                if ($temp instanceof Config) {
                    continue;
                }
                
                if (!isset($temp[$key])) {
                    $temp[$key] = [];
                }
                
                $temp = &$temp[$key];
            }
            
            $temp = $entity;
        }

        return $result;
    }
}
