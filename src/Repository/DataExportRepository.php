<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\DataExport;
use Tigris\BaseBundle\Traits\RepositoryTrait;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class DataExportRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataExport::class);
    }
}
