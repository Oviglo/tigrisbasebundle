<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\OnlineUser;

class OnlineUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OnlineUser::class);
    }

    public function onlineCount(int $timeout = 600): int
    {
        $timeoutDate = new \DateTime($timeout . ' seconds ago');
        $queryBuilder = $this->createQueryBuilder('o')
            ->select('COUNT(o)')
            ->where('o.onlineAt > :timeoutDate')
            ->setParameter(':timeoutDate', $timeoutDate)
            ->setMaxResults(1)
        ;

        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }
}
