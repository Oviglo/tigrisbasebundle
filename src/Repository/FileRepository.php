<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Traits\RepositoryTrait;

class FileRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    public function findData(array $criteria = []): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('f')
            ->addSelect('fo, u')
            ->leftJoin('f.folder', 'fo')
            ->leftJoin('f.user', 'u')
        ;

        if (0 !== $criteria['folder']) {
            $queryBuilder->where('fo.id = :folder')
                ->setParameter(':folder', $criteria['folder'])
            ;
        } else {
            $queryBuilder->where('fo IS NULL');
        }

        if (isset($criteria['search']) && !empty($criteria['search'])) {
            $queryBuilder->andWhere('f.name LIKE :search')
                ->setParameter(':search', '%'.$criteria['search'].'%')
            ;
        }

        if (isset($criteria['type']) && ($criteria['type'] && 'all' !== $criteria['type'])) {
            $queryBuilder->andWhere('f.type = :type')
                ->setParameter(':type', $criteria['type'])
            ;
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }
}
