<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\RepositoryTrait;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class UserRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry, string $userClass)
    {
        parent::__construct($registry, $userClass);
    }

    public function findData(array $criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');
        if (isset($criteria['search']) && $criteria['search']) {
            $queryBuilder->andWhere("e.username LIKE '".$criteria['search']."%'");
            unset($criteria['search']);
        }
        
        if (isset($criteria['type']) && 'all' != $criteria['type']) {
            if ('disabled' == $criteria['type']) {
                $queryBuilder->andWhere('e.enabled = 0');
            } else {
                $queryBuilder->andWhere('e.enabled = 1');
            }

            unset($criteria['type']);
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function findSuperAdmins(): array
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->andWhere('u.roles LIKE :role')
            ->setParameter(':role', '%ROLE_SUPER_ADMIN%');

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }

    public function findAdmins(): array
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->where('u.enabled = true')
            ->andWhere('u.roles LIKE :admin')
            ->orWhere('u.roles LIKE :super_admin')
            ->setParameter(':admin', '%ROLE_ADMIN%')
            ->setParameter(':super_admin', '%ROLE_SUPER_ADMIN%');

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }

    /**
     * @return User[]
     */
    public function findOnline(int $minutes = 5): array
    {
        $date = new \DateTime();
        $date->sub(new \DateInterval(sprintf('PT%dM', $minutes)));

        $queryBuilder = $this->createQueryBuilder('u')
            ->andWhere('u.onlineAt > :date')
            ->setParameter(':date', $date);

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }

    /**
     * @return User[]
     */
    public function findValidUser(): array
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->andWhere('u.enabled = true')
            ->orderBy('u.username', 'ASC');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return User[]
     */
    public function search(string $search): array
    {
        $search = trim($search);
        if ($search === '' || $search === '0') {
            return [];
        }

        $qb = $this->createQueryBuilder('u')
            ->select('u.username as username, u.id as id');

        $qb->where('u.username LIKE :search')
            ->setParameter(':search', sprintf('%%%s%%', $search));

        return $qb->getQuery()->getResult();
    }

    public function dashboardData(): array
    {
        $qb = $this->createQueryBuilder('u');
        $monthStart = new \DateTime('first day of this month');
        $monthEnd = new \DateTime('first day of next month');

        $qb->select('COUNT(DISTINCT u) AS enabled, COUNT(DISTINCT du) AS disabled, COUNT(DISTINCT um) as thisMonth')
            ->where('u.enabled = true')
            ->leftJoin(
                $this->getClassName(),
                'um',
                Join::WITH,
                $qb->expr()->andX('um.createdAt >= :monthStart', 'um.createdAt < :monthEnd', 'um.enabled = true')
            )
            ->leftJoin(
                $this->getClassName(),
                'du',
                Join::WITH,
                $qb->expr()->andX('du.enabled = false')
            )
            ->setParameters(new ArrayCollection([
                new Parameter('monthStart', $monthStart),
                new Parameter('monthEnd', $monthEnd),
            ]));

        return $qb->getQuery()->getSingleResult();
    }

    public function findUnverified(?\DateTime $from): array
    {
        if (!$from instanceof \DateTime) {
            $from = new \DateTime('1 month ago');
        }

        $queryBuilder = $this->createQueryBuilder('u')
            ->where('u.isVerified = false')
            ->andWhere('u.enabled = false')
            ->andWhere('u.createdAt < :from')
            ->setParameter(':from', $from)
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
