<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Entity\Notification;
use Tigris\BaseBundle\Traits\RepositoryTrait;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class NotificationRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    public function findData(array $criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->setParameter(':user', $criteria['user'])
        ;

        unset($criteria['user']);

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function findNew(User $user, \DateTime $date = null): array
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->where('n.user = :user')
            ->setParameter(':user', $user)
            ->andWhere('n.readed = :readed')
            ->setParameter(':readed', false)
        ;

        if ($date instanceof \DateTime) {
            $queryBuilder->andWhere('n.createdAt > :date')
                ->setParameter(':date', $date)
            ;
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function findUnread(User $user): array
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->where('n.user = :user')
            ->setParameter(':user', $user)
            ->andWhere('n.readed = :readed')
            ->setParameter(':readed', false)
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function unreadCount(User $user): int
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->select('COUNT(DISTINCT n)')
            ->where('n.user = :user')
            ->setParameter(':user', $user)
            ->andWhere('n.readed = :readed')
            ->setParameter(':readed', false)
        ;

        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function findLast($user, ?int $count = 6): array
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->where('n.user = :user')
            ->setParameter(':user', $user)
            ->orderBy('n.id', 'DESC')
            ->setMaxResults($count)
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function findByEntity(string $entityClass, int $entityId): array
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->where('n.entityClass = :entityClass')
            ->setParameter(':entityClass', $entityClass)
            ->andWhere('n.entityId = :entityId')
            ->setParameter(':entityId', $entityId)
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function read(User $user): void
    {
        $this->getEntityManager()
            ->createQueryBuilder()
            ->update(Notification::class, 'n')
            ->set('n.readed', 1)
            ->where('n.user = :user')
            ->andWhere('n.readed = 0')
            ->setParameter(':user', $user)
            ->getQuery()
            ->execute()
        ;
    }
}
