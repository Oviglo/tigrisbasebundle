<?php

namespace Tigris\BaseBundle\Repository;

use App\Entity\Group;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Traits\RepositoryTrait;

class GroupRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Group::class);
    }

    public function findData(array $criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('g');

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder);
    }
}
