<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Tigris\BaseBundle\Entity\FileFolder;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\RepositoryTrait;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class FileFolderRepository extends NestedTreeRepository implements ServiceEntityRepositoryInterface
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry, private readonly Security $security)
    {
        $manager = $registry->getManagerForClass(FileFolder::class);

        parent::__construct($manager, $manager->getClassMetadata(FileFolder::class));
    }

    public function getPath($node): string
    {
        $user = $this->security->getUser();
        $queryBuilder = $this->getPathQueryBuilder($node);
        if (!$this->security->isGranted(['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_ADMIN_FILE'])) {
            $queryBuilder->where('node.user = :user')
            ->setParameter(':user', $user);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function getUserRootNodes(User $user = null): array
    {
        $queryBuilder = $this->getRootNodesQueryBuilder();

        if ($user instanceof User) {
            $queryBuilder->andWhere('node.user = :user')
                ->setParameter(':user', $user)
            ;
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function getUserChildren(FileFolder $parent, null|User $user = null, bool $direct = true): array
    {
        $queryBuilder = $this->getChildrenQueryBuilder($parent, $direct);

        if ($user instanceof User) {
            $queryBuilder->andWhere('node.user = :user')
                ->setParameter(':user', $user);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function findUserFolder(User $user): null|FileFolder
    {
        $queryBuilder = $this->createQueryBuilder('f')
            ->addSelect('u')
            ->join('f.user', 'u')
            ->where('f.name = :username')
            ->andWhere('f.user = :user')
            ->setParameter('username', $user->getUsername())
            ->setParameter('user', $user)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
