<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\DataImport;
use Tigris\BaseBundle\Traits\RepositoryTrait;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class DataImportRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataImport::class);
    }

    /**
     * @return array<DataImport>
     */
    public function findPending(): array
    {
        $queryBuilder = $this->createQueryBuilder('d')
            ->andWhere('d.status = :status')
            ->setParameter(':status', DataImport::STATUS_PENDING);

        return $queryBuilder->getQuery()->getResult();
    }
}
