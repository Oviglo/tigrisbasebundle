<?php

namespace Tigris\BaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Wording;
use Tigris\BaseBundle\Traits\RepositoryTrait;

class WordingRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wording::class);
    }

    /**
     * @return Wording[]
     */
    public function findByPage(string $pageName, bool $nameAsKey = false): array
    {
        $qb = $this->createQueryBuilder('w');
        $qb
            ->where('w.page = :page')
            ->setParameter('page', $pageName)
        ;

        $result = $qb->getquery()->getResult();

        if ($nameAsKey) {
            $keys = array_map(fn (Wording $entity): string => $entity->getName(), $result);

            return \array_combine($keys, $result);
        }

        return $result;
    }

    public function findOne(string $page, string $name): ?Wording
    {
        $qb = $this->createQueryBuilder('w');
        $qb
            ->where('w.page = :page')
            ->andWhere('w.name = :name')
            ->setParameter('page', $page)
            ->setParameter('name', $name)
        ;

        return $qb->getquery()->getOneOrNullResult();
    }
}
