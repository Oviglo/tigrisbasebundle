<?php

namespace Tigris\BaseBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tigris\BaseBundle\Entity\Config;
use Tigris\BaseBundle\Repository\ConfigRepository;

#[AsCommand(
    name: 'tigris:format-config',
    description: 'Format new config data table',
)]
class FormatConfigCommand extends Command
{
    public function __construct(private readonly ConfigRepository $configRepository, private readonly EntityManagerInterface $em)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configs = $this->configRepository->findAll();
        foreach ($configs as $config) {
            $bundle = $config->getBundle();
            $name = $config->getName();
            $value = $config->getUnformattedValue();

            $validJson = \json_decode((string) $value);

            if (!str_contains((string) $name, (string) $bundle) && !empty($bundle)) {
                $config->setName($bundle.'.'.$name);

                if (is_array($validJson)) {
                    foreach ($validJson as $key => $val) {
                        if (!str_contains($key, (string) $bundle)) {
                            $newConfig = (new Config())
                                ->setBundle($bundle)
                                ->setName($bundle.'.'.$name.'.'.$key)
                                ->setValue($val)
                            ;

                            $this->em->persist($newConfig);
                        }
                    }
                }
            }
        }

        $this->em->flush();

        return Command::SUCCESS;
    }
}
