<?php

namespace Tigris\BaseBundle\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tigris\BaseBundle\Entity\FileFolder;
use Tigris\BaseBundle\Repository\FileFolderRepository;
use Tigris\BaseBundle\Utils\Utils;

#[AsCommand(name: 'tigris:base:update-folder-name')]
class UpdateFileFolderNameCommand extends Command
{
    public static int $count = 0;

    public function __construct(private readonly FileFolderRepository $fileFolderRepository)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Slugify file folder name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $folders = $this->fileFolderRepository->findBy(['parent' => null]);
        static::$count = 0;

        foreach ($folders as $folder) {
            $this->renameFolder($folder, $output);
        }

        $output->writeln(static::$count.' dossier(s) renommé(s)');

        return self::SUCCESS;
    }

    private function renameFolder(FileFolder $folder, OutputInterface $output): void
    {
        $absPath = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR;
        if (is_dir($absPath.$this->getOldPath($folder))) {
            rename($absPath.$this->getOldPath($folder), $absPath.$folder->getPath());
            ++static::$count;

            $output->writeln($absPath.$this->getOldPath($folder).' -> '.$absPath.$folder->getPath());
        }

        foreach ($folder->getChildren()->toArray() as $child) {
            $this->renameFolder($child, $output);
        }
    }

    private function getOldPath(FileFolder $folder): string
    {
        $parent = $folder->getParent();
        $path = '';

        if ($parent instanceof FileFolder) {
            $path = $parent->getPath().DIRECTORY_SEPARATOR;
        }

        return $path.Utils::stripAccent($folder->getName());
    }
}
