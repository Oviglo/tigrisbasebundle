<?php

namespace Tigris\BaseBundle\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Tigris\BaseBundle\Repository\UserRepository;

#[AsCommand(
    name: 'tigris:create-user',
    description: 'Create a new user',
)]
class CreateUserCommand extends Command
{
    private SymfonyStyle $io;

    public function __construct(private readonly UserPasswordHasherInterface $passwordHasher, private readonly UserRepository $userRepository, private readonly EntityManagerInterface $em)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the user')
            ->addArgument('password', InputArgument::REQUIRED, 'The plain password of the user')
            ->addOption('admin', null, InputOption::VALUE_NONE, 'If set, the user is created as an administrator')
            ->addOption('superadmin', null, InputOption::VALUE_NONE, 'If set, the user is created as a super administrator')
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $isAdmin = $input->getOption('admin');
        $isSuperAdmin = $input->getOption('superadmin');

        $this->validateUserData($email);

        $user = new User();
        $user->setEmail($email);
        $user->setUsername($email);
        $user->setRoles($isSuperAdmin ? ['ROLE_SUPER_ADMIN'] : ($isAdmin ? ['ROLE_ADMIN'] : ['ROLE_USER']));
        $user->setPassword($this->passwordHasher->hashPassword($user, $password));
        $user->setEnabled(true);

        $this->em->persist($user);
        $this->em->flush();

        $this->io->success(sprintf('User was successfully created: %s', $email));

        return Command::SUCCESS;
    }

    private function validateUserData(string $email): void
    {
        $existingUser = $this->userRepository->findOneBy(['email' => $email]);
        if (null !== $existingUser) {
            throw new \RuntimeException(sprintf('There is already a user registered with the "%s" email.', $email));
        }
    }
}
