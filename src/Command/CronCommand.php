<?php

namespace Tigris\BaseBundle\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Event\CronEvent;
use Tigris\BaseBundle\Event\Events;

#[AsCommand(name: 'app:cron')]
class CronCommand extends Command
{
    public function __construct(private readonly EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Run cron');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $event = new CronEvent();
        $this->eventDispatcher->dispatch($event, Events::CRON);

        return self::SUCCESS;
    }
}
