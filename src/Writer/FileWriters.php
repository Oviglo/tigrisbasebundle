<?php

namespace Tigris\BaseBundle\Writer;

class FileWriters
{
    private ?array $writers = null;

    public function addWriter($writer, $format): void
    {
        if (!isset($this->writers[$format])) {
            $this->writers[$format] = $writer;
        }
    }

    public function getWriter($format)
    {
        return $this->writers[$format] ?? null;
    }
}
