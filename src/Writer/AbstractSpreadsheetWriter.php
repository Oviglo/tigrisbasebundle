<?php

namespace Tigris\BaseBundle\Writer;

use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Factory\PhpSpreadsheetFactory;
use Tigris\BaseBundle\Utils\Utils;

abstract class AbstractSpreadsheetWriter extends AbstractWriter
{
    /**
     * @var string
     */
    protected $format = 'Xls';

    public function __construct(protected PhpSpreadsheetFactory $phpSpreadsheetFactory, TranslatorInterface $translator)
    {
        parent::__construct($translator);
    }

    public function write(string $path, string $filename, array $data): string
    {
        $phpSpreadsheetObject = $this->phpSpreadsheetFactory->createSpreadsheet();
        $offsetY = 0;
        $col = 'A';
        foreach ($data as $i => $singleData) {
            $offsetX = 0;
            $itemsCount = is_countable($data[$i]) ? count($data[$i]) : 0;
            for ($j = 0; $j < $itemsCount; ++$j) {
                $col = Utils::getSheetAlphaFromNumber($j + $offsetX);
                $row = $i + $offsetY + 1;
                $phpSpreadsheetObject->setActiveSheetIndex(0)->setCellValue($col.$row, $singleData[$j]['value']);
                $phpSpreadsheetObject->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);

                // Cell background color
                if (isset($singleData[$j]['backgroundColor'])) {
                    $phpSpreadsheetObject->getActiveSheet()->getStyle($col.$row)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB(substr((string) $singleData[$j]['backgroundColor'], 1));
                    $phpSpreadsheetObject->getActiveSheet()->getStyle($col.$row)->getFill()->setFillType(Fill::FILL_SOLID)->getEndColor()->setRGB(substr((string) $singleData[$j]['backgroundColor'], 1));
                }

                if (isset($singleData[$j]['color'])) {
                    $phpSpreadsheetObject->getActiveSheet()->getStyle($col.$row)->getFont()->getColor()->setRGB(substr((string) $singleData[$j]['color'], 1));
                }

                if (isset($singleData[$j]['colspan'])) {
                    $colSpan = Utils::getSheetAlphaFromNumber(($j + $offsetX) + ((int) $singleData[$j]['colspan'] - 1));
                    $phpSpreadsheetObject->getActiveSheet()->mergeCells($col.$row.':'.$colSpan.$row);
                    $offsetX += ($singleData[$j]['colspan'] - 1);
                }
            }
        }

        // Style header
        $phpSpreadsheetObject->getActiveSheet()->getStyle(sprintf('A1:%s1', $col))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('333333');
        $phpSpreadsheetObject->getActiveSheet()->getStyle(sprintf('A1:%s1', $col))->getFont()->getColor()->setARGB(Color::COLOR_WHITE);

        $phpSpreadsheetObject->getActiveSheet()->freezePane('A1');

        $writer = $this->phpSpreadsheetFactory->createWriter($phpSpreadsheetObject, $this->format);
        $fileName = $filename.'.'.strtolower($this->format);
        $writer->save($path.$fileName);

        return $fileName;
    }
}
