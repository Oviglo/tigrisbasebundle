<?php

namespace Tigris\BaseBundle\Writer;

use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractWriter
{
    abstract public function write(string $path, string $filename, array $data): string;

    abstract public static function getName(): string;

    public function __construct(protected TranslatorInterface $translator)
    {
    }
}
