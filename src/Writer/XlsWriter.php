<?php

namespace Tigris\BaseBundle\Writer;

class XlsWriter extends AbstractSpreadsheetWriter
{
    public static function getName(): string
    {
        return 'XLS';
    }
}
