<?php

namespace Tigris\BaseBundle\Writer;

class CsvWriter extends AbstractWriter
{
    public static function getName(): string
    {
        return 'CSV';
    }

    public function write(string $path, string $filename, array $data): string
    {
        return '';
    }
}
