<?php

namespace Tigris\BaseBundle\Doctrine;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\AssociationMapping;
use Doctrine\ORM\Mapping\ClassMetadata;

#[AsDoctrineListener(event: Events::loadClassMetadata)]
class TablePrefix
{
    public function __construct(protected string $prefix)
    {
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $classMetadata = $eventArgs->getClassMetadata();

        if (!$classMetadata instanceof ClassMetadata) {
            return;
        }

        if (!$classMetadata->isInheritanceTypeSingleTable() || $classMetadata->getName() === $classMetadata->rootEntityName) {
            $classMetadata->setPrimaryTable([
                'name' => $this->prefix.$classMetadata->getTableName(),
            ]);
        }

        /* @var AssociationMapping */
        foreach ($classMetadata->getAssociationMappings() as $fieldName => $mapping) {
            if (ClassMetadata::MANY_TO_MANY == $mapping['type'] && $mapping->isOwningSide()) {
                $mappedTableName = $mapping->joinTable->toArray()['name'];
                $classMetadata->associationMappings[$fieldName]->joinTable['name'] = $this->prefix.$mappedTableName;
            }
        }
    }
}
