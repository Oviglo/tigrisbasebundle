<?php

namespace Tigris\BaseBundle\Factory;

use PhpOffice\PhpSpreadsheet\Helper\Html;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Symfony\Component\HttpFoundation\StreamedResponse;

class PhpSpreadsheetFactory
{
    public function __construct(public string $ioFactory = IOFactory::class)
    {
    }

    public function createSpreadsheet(string $filename = null): Spreadsheet
    {
        return (null === $filename) ? new Spreadsheet() : call_user_func([$this->ioFactory, 'load'], $filename);
    }

    /**
     * Create a worksheet drawing.
     */
    public function createSpreadsheetDrawing(): Drawing
    {
        return new Drawing();
    }

    /**
     * Create a reader.
     */
    public function createReader(string $type = 'Xls'): mixed
    {
        return call_user_func([$this->ioFactory, 'createReader'], $type);
    }

    public function createWriter(Spreadsheet $spreadsheetObject, string $type = 'Xls'): mixed
    {
        return call_user_func([$this->ioFactory, 'createWriter'], $spreadsheetObject, $type);
    }

    public function createStreamedResponse($writer, int $status = 200, array $headers = []): StreamedResponse
    {
        return new StreamedResponse(
            function () use ($writer): void {
                $writer->save('php://output');
            },
            $status,
            $headers
        );
    }

    /**
     * Create a PHPExcel Helper HTML Object.
     */
    public function createHelperHTML(): Html
    {
        return new Html();
    }
}
