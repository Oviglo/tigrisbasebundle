<?php

namespace Tigris\BaseBundle\Reader;

use Symfony\Component\HttpFoundation\File\File;
use Tigris\BaseBundle\Contracts\Reader\ReaderInterface;
use Tigris\BaseBundle\Factory\PhpSpreadsheetFactory;
use Tigris\BaseBundle\Utils\Utils;

abstract class AbstractSpreadsheetReader implements ReaderInterface
{
    private array $header = [];

    public function __construct(private readonly PhpSpreadsheetFactory $spreadsheetFactory)
    {
    }

    public function read(File $file): array
    {
        $ext = ucfirst((string) $file->guessExtension());
        if ('Txt' === $ext) {
            $ext = 'Csv';
        }
        
        $sheetReader = $this->spreadsheetFactory->createReader($ext);
        $phpSpreadSheet = $sheetReader->load($file->getPathname());
        $worksheet = $phpSpreadSheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();
        $highestCol = Utils::lettersToNum($worksheet->getHighestColumn());

        // Set header
        for ($colId = 1; $colId <= $highestCol; ++$colId) {
            $this->header[] = $worksheet->getCellByColumnAndRow($colId, 1)->getValue();
        }

        $this->header = array_combine($this->header, $this->header);

        $result = [];

        for ($rowId = 2; $rowId <= $highestRow; ++$rowId) {
            $result[$rowId] = [];
            for ($colId = 1; $colId <= $highestCol; ++$colId) {
                $result[$rowId][$colId] = $worksheet->getCellByColumnAndRow($colId, $rowId)->getValue();
            }
        }

        return $result;
    }

    public function getHeader(): array
    {
        return $this->header;
    }
}
