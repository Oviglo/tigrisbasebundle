<?php

namespace Tigris\BaseBundle\Reader;

use Tigris\BaseBundle\Contracts\Reader\ReaderInterface;

class FileReaders
{
    private ?array $readers = null;

    public function addReader(ReaderInterface $reader, string $name): self
    {
        if (!isset($this->readers[$name])) {
            $this->readers[$name] = $reader;
        }

        return $this;
    }

    public function getReader(string $name): ?ReaderInterface
    {
        return $this->readers[$name] ?? null;
    }
}
