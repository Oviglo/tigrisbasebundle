<?php

namespace Tigris\BaseBundle\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class Recaptcha extends Constraint
{
    public string $message = 'Invalid captcha';
}
