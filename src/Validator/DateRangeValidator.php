<?php

namespace Tigris\BaseBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Tigris\BaseBundle\Validator\Constraints\DateRange;

class DateRangeValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof DateRange) {
            throw new UnexpectedTypeException($constraint, DateRange::class);
        }

        // Get all form data
        $values = $this->context->getObject()->getParent()->getData();
        // Data type
        $start = null;
        $end = null;
        if (is_array($values)) {
            $start = $values[$constraint->startField];
            $end = $values[$constraint->endField];
        } elseif (is_object($values)) {
            $sm = 'get'.ucfirst((string) $constraint->startField);
            $em = 'get'.ucfirst((string) $constraint->endField);
            $start = $values->$sm();
            $end = $values->$em();
        }

        if (!$start instanceof \DateTime) {
            throw new UnexpectedValueException($start, 'DateTime');
        }

        if (!$end instanceof \DateTime) {
            throw new UnexpectedValueException($end, 'DateTime');
        }

        if ($start > $end) {
            $this->context->buildViolation($constraint->message)
                ->addViolation()
            ;
        }
    }
}
