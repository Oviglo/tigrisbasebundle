<?php

namespace Tigris\BaseBundle\Validator;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use ReCaptcha\ReCaptcha as GoogleRecaptcha;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class RecaptchaValidator extends ConstraintValidator implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __construct(private readonly GoogleRecaptcha $recaptcha, private readonly RequestStack $requestStack)
    {
    }

    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof Recaptcha) {
            throw new UnexpectedTypeException($constraint, Recaptcha::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $request = $this->requestStack->getCurrentRequest();

        $response = $this->recaptcha
            ->verify($value, $request->getClientIp())
        ;

        if (!$response->isSuccess()) {
            $errorCodes = implode('; ', array_map([$this, 'getErrorMessage'], $response->getErrorCodes()));
            $this->buildViolation($constraint->message, $value, $errorCodes);
        }
    }

    private function getErrorMessage(string $errorCode): string
    {
        $messages = [
            'missing-input-secret' => 'The secret parameter is missing',
            'invalid-input-secret' => 'The secret parameter is invalid or malformed',
            'missing-input-response' => 'The response parameter is missing',
            'invalid-input-response' => 'The response parameter is invalid or malformed',
            'bad-request' => 'The request is invalid or malformed',
            'timeout-or-duplicate' => 'The response is no longer valid: either is too old or has been used previously',
            'challenge-timeout' => 'Challenge timeout',
            'score-threshold-not-met' => 'Score threshold not met',
            'bad-response' => 'Did not receive a 200 from the service',
            'connection-failed' => 'Could not connect to service',
            'invalid-json' => 'Invalid JSON received',
            'unknown-error' => 'Not a success, but no error codes received',
            'hostname-mismatch' => 'Expected hostname did not match',
            'apk_package_name-mismatch' => 'Expected APK package name did not match',
            'action-mismatch' => 'Expected action did not match',
        ];

        return $messages[$errorCode] ?? $errorCode;
    }

    public function buildViolation(string $message, mixed $value, string $errorCodes): void
    {
        $this->context->buildViolation($message)
            ->setParameter('{{ value }}', $value)
            ->setParameter('{{ errorCodes }}', $errorCodes)
            ->addViolation()
        ;

        if ($this->logger instanceof LoggerInterface) {
            $this->logger->info($message, ['value' => $value, 'errorCodes' => $errorCodes]);
        }
    }
}
