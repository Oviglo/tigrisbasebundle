<?php

namespace Tigris\BaseBundle\Validator;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Tigris\BaseBundle\Validator\Constraints\StopForumSpam;

class StopForumSpamValidator extends ConstraintValidator
{
    final public const API_URL = 'http://api.stopforumspam.org';

    public function __construct(private HttpClientInterface $client, private readonly LoggerInterface $logger)
    {
        $this->client = $client->withOptions([
            'base_uri' => self::API_URL,
        ]);
    }

    public function testEmail(string $email): bool
    {
        $response = $this->client->request('GET', '/api', [
            'query' => [
                'email' => $email,
                'json' => 1,
            ],
        ]);

        $arrayResponse = $response->toArray();
        if (!$arrayResponse['success']) {
            return true;
        }

        $frequency = $arrayResponse['email']['frequency'];

        return 0 == (int) $frequency;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof StopForumSpam) {
            throw new UnexpectedTypeException($constraint, StopForumSpam::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new \UnexpectedValueException($value, 'string');
        }

        try {
            if (!$this->testEmail($value)) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ email }}', $value)
                    ->addViolation();
            }
        } catch (\Exception $exception) {
            $this->logger->info($exception);
        }
    }
}
