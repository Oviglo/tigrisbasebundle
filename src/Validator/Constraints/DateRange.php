<?php

namespace Tigris\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Tigris\BaseBundle\Validator\DateRangeValidator;

#[\Attribute]
class DateRange extends Constraint
{
    public $message = 'date_range';

    public $startField;
    
    public $endField;

    public function getRequiredOptions(): array
    {
        return [
            'startField',
            'endField',
        ];
    }

    public function validatedBy(): string
    {
        return DateRangeValidator::class;
    }
}
