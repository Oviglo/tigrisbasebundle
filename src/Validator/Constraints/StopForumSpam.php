<?php

namespace Tigris\BaseBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Tigris\BaseBundle\Validator\StopForumSpamValidator;

#[\Attribute]
class StopForumSpam extends Constraint
{
    public $message = 'stop_forum_spam';

    public function validatedBy(): string
    {
        return StopForumSpamValidator::class;
    }
}
