<?php

namespace Tigris\BaseBundle\FileUploader;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileUploaderInterface
{
    public function upload(UploadedFile $file, ?string $path = null, array $imageStyles = []): string|array;

    public function remove(string $path): void;
}
