<?php

namespace Tigris\BaseBundle\FileUploader;

enum ImageStyle: string
{
    case THUMBNAIL = 'thumbnail';
    case SMALL = 'small';
    case MEDIUM = 'medium';
    case LARGE = 'large';
    case ORIGINAL = 'original';

    public function getSizes(): array
    {
        return match ($this) {
            self::THUMBNAIL => [100, 100],
            self::SMALL => [200, 200],
            self::MEDIUM => [400, 400],
            self::LARGE => [800, 800],
            self::ORIGINAL => [0, 0],
        };
    }
}
