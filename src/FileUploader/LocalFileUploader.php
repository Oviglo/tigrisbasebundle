<?php

namespace Tigris\BaseBundle\FileUploader;

use Gumlet\ImageResize;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LocalFileUploader implements FileUploaderInterface
{
    public function __construct(
        #[Autowire('%project_dir%/public/files')]
        private string $uploadDir
    ) {
    }

    public function upload(UploadedFile $file, ?string $path = null, array $imageStyles = []): string|array
    {
        $filePath = $this->uploadDir.'/'.$path;
        $fileName = \uniqid().'_'.$file->getClientOriginalName();

        $file->move($filePath, $fileName);

        if (empty($imageStyles)) {
            return $path.'/'.$fileName;
        }

        return $this->generateStyles($path.'/'.$fileName, $imageStyles);
    }

    public function remove(string $path): void
    {
        $filePath = $this->uploadDir.'/'.$path;

        if (!file_exists($filePath)) {
            return;
        }

        unlink($path);
    }

    private function generateStyles(string $path, array $imageStyles): array
    {
        $imagePaths = [];
        foreach ($imageStyles as $style) {
            try {
                $image = new ImageResize($this->uploadDir.'/'.$path);
                $width = $style->getSizes()[0];
                $height = $style->getSizes()[1] ?? null;
                if (0 === $width && 0 === $height) {
                    continue;
                }

                if (null === $height) {
                    $image->resizeToWidth($width);
                } else {
                    $image->resize($width, $height);
                }

                $image->save($this->uploadDir.'/'.$style->getPath());
                $imagePaths[] = $style->getPath();
            } catch (\Exception $e) {
                continue;
            }
        }

        return $imagePaths;
    }
}
