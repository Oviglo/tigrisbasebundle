<?php

namespace Tigris\BaseBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Contracts\Security\User\ShibbolethUserInterface;

trait ShibbolethUserTrait
{
    #[ORM\Column(length: 200, nullable: true)]
    private ?string $shibbolethEppn;

    public function getShibbolethEppn(): string
    {
        return $this->shibbolethEppn ?? '';
    }

    public function setShibbolethEppn(?string $shibbolethEppn): ShibbolethUserInterface
    {
        $this->shibbolethEppn = $shibbolethEppn;

        return $this;
    }
}
