<?php

namespace Tigris\BaseBundle\Traits;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

trait RepositoryTrait
{
    abstract public function createQueryBuilder(string $alias, ?string $indexBy = null): QueryBuilder;

    public function findData(array $criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');

        if (isset($criteria['search']) && !empty($criteria['search'])) {
            $queryBuilder->andWhere('LOWER(e.name) LIKE :search')
                ->setParameter(':search', '%'.$criteria['search'].'%')
            ;
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function findLast(): ?object
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(1)
        ;

        $query = $queryBuilder->getQuery();

        return $query->getOneOrNullResult();
    }

    public function findByIds(array $ids, bool $idForIndex = false, bool $reorder = false): array
    {
        $ids = array_map('intval', array_values($ids));

        $queryBuilder = $this->createQueryBuilder('e')
            ->where('e.id IN (:ids)')
            ->setParameter(':ids', $ids)
        ;

        $result = $queryBuilder->getQuery()->getResult();

        if ($idForIndex) {
            $entities = [];
            foreach ($result as $entity) {
                $entities[$entity->getId()] = $entity;
            }

            return $entities;
        }

        if ($reorder) {
            uasort($result, function (object $entityA, object $entityB) use ($ids): int {
                $idA = $entityA->getId();
                $idB = $entityB->getId();

                return (array_search($idA, $ids, true) < array_search($idB, $ids, true)) ? -1 : 1;
            });
        }

        return $result;
    }

    public function findChartByDates(\DateTime $dateStart, \DateTime $dateEnd, ?string $groupBy = null, ?string $select = null): array
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('count(e) as nb, SUBSTRING(e.createdAt, 1, 4) as year, SUBSTRING(e.createdAt, 6,2) as month')
            ->where('e.createdAt >= :start')
            ->andWhere('e.createdAt < :end')
            ->setParameter(':start', $dateStart)
            ->setParameter(':end', $dateEnd)
        ;

        if (null !== $select) {
            $queryBuilder->addSelect($select);
        }

        if (null !== $groupBy) {
            $queryBuilder->groupBy($groupBy);
        }

        $queryBuilder->addGroupBy('year, month')
            ->orderBy('year', 'ASC')
            ->addOrderBy('month', 'ASC')
        ;

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }

    public function addBasicCriteria(QueryBuilder $queryBuilder, array $criteria): void
    {
        if (isset($criteria['begin'])) {
            $queryBuilder->setFirstResult($criteria['begin']);
        }

        if (isset($criteria['count'])) {
            $queryBuilder->setMaxResults($criteria['count']);
        }

        $rootAliases = $queryBuilder->getRootAliases();

        if (isset($criteria['order'])) {
            $desc = $criteria['rev'] ?? true;
            $queryBuilder->orderBy($rootAliases[0].'.'.$criteria['order'], ($desc) ? 'DESC' : 'ASC');
        }

        if (isset($criteria['orders'])) {
            foreach ($criteria['orders'] as $sort => $order) {
                $queryBuilder->addOrderBy($rootAliases[0].'.'.$sort, $order);
            }
        }
    }

    public function findIds(): array
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e.id')
        ;

        return $qb->getQuery()->getResult();
    }
}
