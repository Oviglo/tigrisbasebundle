<?php

namespace Tigris\BaseBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

trait UploadEntityTrait
{
    /**
     * File path by public dir.
     */
    #[ORM\Column(name: 'file_path', type: 'string', length: 255, nullable: true)]
    protected $filePath;

    /**
     * @var bool
     */
    protected $deleteFile;

    #[Assert\File(maxSize: '32768k')]
    protected $file;
    
    protected $tmpFilePath;

    /**
     * @JMS\AccessType("public_method")
     */
    protected $webPath;

    abstract protected function getUploadDir();

    public function setWebPath()
    {
    }

    public function getWebPath(): string
    {
        return $this->getUploadDir().$this->getFilePath();
    }

    public function __toString()
    {
        return ''; // $this->getWebPath();
    }

    public function setFile(?UploadedFile $file)
    {
        $this->tmpFilePath = $this->filePath;
        $this->filePath = '';
        $this->file = $file;

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    protected function getPublicRootDir(): string
    {
        return __DIR__.'/../../../../../public/';
    }

    protected function getFileRootDir(): string
    {
        return $this->getPublicRootDir().$this->getUploadDir();
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function preUpload(): void
    {
        if ($this->file instanceof UploadedFile) {
            $this->filePath = md5(uniqid()).'.'.$this->file->guessExtension();
            if (file_exists($this->getFileRootDir().$this->filePath)) {
                $this->preUpload();

                return;
            }
        }
    }

    #[ORM\PostPersist]
    #[ORM\PostUpdate]
    public function upload(): void
    {
        if ($this->file instanceof UploadedFile) {
            $this->file = $this->file->move(
                $this->getFileRootDir(),
                $this->filePath
            );
        }
    }

    #[ORM\PreRemove]
    public function removeFile(): void
    {
        if (is_file($this->getFileRootDir().$this->filePath)) {
            unlink($this->getFileRootDir().$this->filePath);
        }
    }

    /**
     * Get the value of deleteFile.
     *
     * @return bool
     */
    public function getDeleteFile()
    {
        return $this->deleteFile;
    }

    /**
     * Set the value of deleteFile.
     *
     * @return self
     */
    public function setDeleteFile(bool $deleteFile)
    {
        $this->deleteFile = $deleteFile;

        return $this;
    }
}
