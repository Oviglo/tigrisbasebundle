<?php

namespace Tigris\BaseBundle\Traits;

use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Tigris\BaseBundle\Form\Type\FormActionsType;

trait FormTrait
{
    public function getDeleteForm($entity, string $routePrefix, array $routeParams = [], array $options = []): FormInterface
    {
        $routeParams = array_merge(['id' => $entity->getId()], $routeParams);

        $buttons = [];

        if (!isset($options['save_button'])) {
            $options['save_button'] = [];
        }

        if (is_array($options['save_button'])) {
            $btnOptions = array_merge([
                'label' => 'button.delete',
                'attr' => ['class' => 'btn-danger'],
            ], $options['save_button']);

            $buttons['save'] = [
                'type' => SubmitType::class,
                'options' => $btnOptions,
            ];
        }

        if (!isset($options['cancel_button'])) {
            $options['cancel_button'] = [];
        }

        if (is_array($options['cancel_button'])) {
            $btnOptions = $this->generateCancelButton($options, $routePrefix);

            $buttons['cancel'] = [
                'type' => ButtonType::class,
                'options' => $btnOptions,
            ];
        }

        $routeName = $routePrefix.'delete';
        if ($this->routeExists($routeName)) {
            @trigger_error('The route name  "delete" are  deprecated.', E_USER_DEPRECATED);
        } else {
            $routeName = $routePrefix.'remove';
        }

        $formOptions = [
            'action' => $this->generateUrl($routeName, $routeParams),
            'method' => 'DELETE',
        ];

        if (isset($options['form']) && is_array($options['form'])) {
            $formOptions = array_merge($formOptions, $options['form']);
        }

        $form = $this->createFormBuilder($entity, $formOptions)->getForm();
        $form->add('actions', FormActionsType::class, [
            'label' => false,
            'buttons' => $buttons,
        ]);

        return $form;
    }

    public function getConfirmationForm($action = '', $cancelUrl = '', $method = 'DELETE', $saveLabel = 'button.confirm', $cancelButtonLabel = 'button.cancel')
    {
        $form = $this->createFormBuilder(null)
            ->setAction($action)
            ->setMethod($method)
            ->getForm()
        ;

        $this->addFormActions($form, $cancelUrl, $saveLabel, $cancelButtonLabel);

        return $form;
    }

    public function addFormActions(FormInterface $form, bool|string $cancelUrl = false, string $saveLabel = 'button.save', string $cancelButtonLabel = 'button.cancel'): void
    {
        $saveButtonStyle = 'btn-primary';
        if (str_contains($saveLabel, 'delete')) {
            $saveButtonStyle = 'btn-danger';
        }

        $buttons = [
            'save' => [
                'type' => SubmitType::class,
                'options' => ['label' => $saveLabel, 'attr' => ['class' => $saveButtonStyle]],
            ],
        ];

        if (false !== $cancelUrl) {
            $buttons['cancel'] = [
                'type' => ButtonType::class,
                'options' => [
                    'as_link' => true,
                    'label' => $cancelButtonLabel,
                    'attr' => ['data-cancel' => true, 'href' => $cancelUrl],
                ],
            ];
        }

        $form->add('actions', FormActionsType::class, [
            'label' => false,
            'buttons' => $buttons,
        ]);
    }

    /**
     * Return true if route exists.
     *
     * @param string $name Route name
     */
    public function routeExists(string $name): bool
    {
        $router = $this->container->get('router');

        return null !== $router->getRouteCollection()->get($name);
    }

    abstract protected function createFormBuilder(mixed $data = null, array $options = []): FormBuilderInterface;

    /**
     * @return mixed[]
     */
    private function generateCancelButton(array $options, string $routePrefix, string $routeName = 'index'): array
    {
        $btnOptions = array_merge([
            'as_link' => true,
            'label' => 'button.cancel',
            'attr' => ['data-cancel' => true, 'href' => $this->generateUrl($routePrefix.$routeName)],
        ], $options['cancel_button']);

        // Create url
        if (isset($btnOptions['attr'], $btnOptions['attr']['href']) && is_null($btnOptions['attr']['href'])) {
            $btnOptions['attr']['href'] = $this->generateUrl($routePrefix.$routeName);
        }

        return $btnOptions;
    }
}
