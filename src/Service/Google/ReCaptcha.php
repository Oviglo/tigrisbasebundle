<?php

namespace Tigris\BaseBundle\Service\Google;

use ReCaptcha\ReCaptcha as BaseReCaptcha;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;

class ReCaptcha
{
    private readonly BaseReCaptcha $reCaptcha;

    public function __construct(
        #[Autowire(env: 'GOOGLE_RECAPTCHA_SECRET')]
        string $secret
    ) {
        $this->reCaptcha = new BaseReCaptcha($secret);
    }

    public function verify(Request $request): bool
    {
        $response = $request->request->get('g-recaptcha-response');
        $remoteIp = $request->getClientIp();

        $resp = $this->reCaptcha->verify($response, $remoteIp);

        return $resp->isSuccess();
    }
}
