<?php

namespace Tigris\BaseBundle\Service\Google;

use Google\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class MyBusinessReview implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private const ONLY_RATING = 'Only rating provided.';
    
    private const API_URL = 'https://maps.googleapis.com/maps/api/place/details/json?';

    public function __construct(private readonly Client $client)
    {
    }

    public function getReviews(string $placeId): array
    {
        try {
            $httpClient = $this->client->authorize();
            $response = $httpClient->request('GET', self::API_URL.\http_build_query([
                'fields' => 'reviews',
                'reviews_no_translations' => true,
                'place_id' => $placeId,
            ]));
            $apiResult = json_decode((string) $response->getBody(), false, 512, JSON_THROW_ON_ERROR);
            if (!isset($apiResult->result)) {
                return [];
            }
            
            $apiResult = $apiResult->result;

            $reviews = [];
            foreach ($apiResult->reviews as $review) {
                if (self::ONLY_RATING !== $review->text && $review->rating > 4) {
                    $date = new \DateTime();
                    $date->setTimestamp($review->time);
                    $reviews[] = new Review($review->text, $review->rating, $review->author_name, $review->author_url, $review->profile_photo_url, $date);
                }
            }

            return $reviews;
        } catch (\JsonException|ClientException|ConnectException $e) {
            $this->logger->warning('MyBusinessReview', ['error' => $e::class, 'message' => $e->getMessage()]);

            return [];
        }
    }

    public function getRating(string $placeId): array
    {
        try {
            $httpClient = $this->client->authorize();
            $response = $httpClient->request('GET', self::API_URL.\http_build_query([
                'fields' => 'rating,user_ratings_total',
                'place_id' => $placeId,
            ]));

            if (200 !== $response->getStatusCode()) {
                return [];
            }

            $apiResult = json_decode((string) $response->getBody(), false, 512, JSON_THROW_ON_ERROR);
            if (!isset($apiResult->result)) {
                return [];
            }

            return [
                'rating' => $apiResult->result->rating,
                'count' => $apiResult->result->user_ratings_total,
            ];
        } catch (\JsonException|ClientException|ConnectException $e) {
            $this->logger->warning('MyBusinessReview', ['error' => $e::class, 'message' => $e->getMessage()]);

            return [];
        }
    }
}
