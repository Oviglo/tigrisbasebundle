<?php

namespace Tigris\BaseBundle\Service\Google;

class Review
{
    public function __construct(
        public string $text,
        public int $rating,
        public string $author,
        public string $url,
        public string $photo,
        public \DateTime $date,
    ) {
    }
}
