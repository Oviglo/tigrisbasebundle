<?php

namespace Tigris\BaseBundle\Service\Geocoding;

use Tigris\BaseBundle\Contracts\ArrayTransformable;

class GeocodingResponse implements ArrayTransformable
{
    final public const SUCCESS = 'success';
    
    final public const ERROR = 'error';

    public string $status = self::ERROR;

    private ?string $error = null;

    public array $addresses = [];

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(?string $error): self
    {
        $this->error = $error;

        return $this;
    }

    public function toArray(): array
    {
        $addresses = [];
        foreach ($this->addresses as $addr) {
            $addresses[] = $addr->toArray();
        }

        return [
            'error' => $this->getError(),
            'addresses' => $addresses,
            'status' => $this->status,
        ];
    }
}
