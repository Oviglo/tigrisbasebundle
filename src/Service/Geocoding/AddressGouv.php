<?php

namespace Tigris\BaseBundle\Service\Geocoding;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Tigris\BaseBundle\Contracts\Geocoding\GeocodingInterface;
use Tigris\BaseBundle\Utils\Address;

class AddressGouv implements GeocodingInterface
{
    public function __construct(private readonly HttpClientInterface $client)
    {
    }

    public function getUrl(): string
    {
        return 'https://api-adresse.data.gouv.fr/search/';
    }

    public function searchAddress(string $address): GeocodingResponse
    {
        $geocodingResponse = new GeocodingResponse();
        $data = [
            'q' => $address,
            'type' => 'municipality',
        ];

        $response = $this->client->request('GET', sprintf('%s?%s', $this->getUrl(), http_build_query($data)));
        $apiResponse = json_decode($response->getContent(), true);

        foreach ($apiResponse['features'] as $f) {
            $newAddr = new Address();
            $newAddr->lat = $f['geometry']['coordinates'][0];
            $newAddr->lng = $f['geometry']['coordinates'][1];
            $newAddr->countryCode = 'FR';
            $newAddr->line1 = $f['properties']['name'];
            $newAddr->zipCode = $f['properties']['postcode'];
            $newAddr->city = $f['properties']['city'];
            $geocodingResponse->addresses[] = $newAddr;
        }

        $geocodingResponse->status = GeocodingResponse::SUCCESS;

        return $geocodingResponse;
    }
}
