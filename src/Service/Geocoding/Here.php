<?php

namespace Tigris\BaseBundle\Service\Geocoding;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Tigris\BaseBundle\Contracts\Geocoding\GeocodingInterface;
use Tigris\BaseBundle\Utils\Address;

class Here implements GeocodingInterface
{
    public function __construct(
        #[Autowire(env: 'default::HERE_API_KEY')]
        private readonly ?string $apiKey,
        private readonly HttpClientInterface $client
    ) {
    }

    public function getUrl(): string
    {
        return 'https://geocode.search.hereapi.com/v1/geocode';
    }

    public function searchAddress(string $address): GeocodingResponse
    {
        if (null === $this->apiKey) {
            throw new \RuntimeException('Here API key is required.');
        }

        $geocodingResponse = new GeocodingResponse();
        $data = [
            'q' => $address,
            'apiKey' => $this->apiKey,
        ];

        try {
            $response = $this->client->request('GET', sprintf('%s?%s', $this->getUrl(), http_build_query($data)));
        } catch (TransportExceptionInterface $transportException) {
            $geocodingResponse->status = GeocodingResponse::ERROR;
            $geocodingResponse->setError($transportException->getMessage());

            return $geocodingResponse;
        }

        $apiResponse = json_decode($response->getContent(), true);

        foreach ($apiResponse['items'] as $item) {
            $newAddr = new Address();
            $newAddr->lat = $item['position']['lat'];
            $newAddr->lng = $item['position']['lng'];
            $newAddr->countryCode = $item['address']['countryCode'];
            $newAddr->line1 = $item['address']['label'];
            $newAddr->zipCode = $item['address']['postalCode'];
            $newAddr->city = $item['address']['city'];
            $geocodingResponse->addresses[] = $newAddr;
        }

        $geocodingResponse->status = GeocodingResponse::SUCCESS;

        return $geocodingResponse;
    }
}
