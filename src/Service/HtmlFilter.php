<?php

namespace Tigris\BaseBundle\Service;

use Symfony\Component\HtmlSanitizer\HtmlSanitizer;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerConfig;

class HtmlFilter
{
    private readonly HtmlSanitizerConfig $config;
    
    private readonly HtmlSanitizer $sanitizer;

    public function __construct()
    {
        $this->config = (new HtmlSanitizerConfig())
            ->allowSafeElements()
            ->dropAttribute('style', '*')
            ->allowRelativeLinks()
            ->allowRelativeMedias()
        ;

        $this->sanitizer = new HtmlSanitizer($this->config);
    }

    public function filter(string $html): string
    {
        return $this->sanitizer->sanitize($html);
    }
}
