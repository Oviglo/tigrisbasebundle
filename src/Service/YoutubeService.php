<?php

namespace Tigris\BaseBundle\Service;

use Google\Client;
use Google\Service\YouTube;
use Google\Service\YouTube\ActivityListResponse;

class YoutubeService
{
    private readonly YouTube $youtube;

    public function __construct(private readonly Client $client)
    {
        $this->youtube = new YouTube($this->client);
    }

    public function getLastActivitiesByChannel(string $channeId, int $maxResults = 5): ActivityListResponse
    {
        $queryParams = [
            'channelId' => $channeId,
            'maxResults' => $maxResults,
        ];

        return $this->youtube->activities->listActivities('snippet,contentDetails', $queryParams);
    }
}
