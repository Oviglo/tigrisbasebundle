<?php

namespace Tigris\BaseBundle\Service\Meta\GraphAPI;

enum FacebookAPIFieldType: string
{
    case EVENTS = 'events';
    case POSTS = 'posts';
    case POST = 'post';
}
