<?php

namespace Tigris\BaseBundle\Service\Meta\GraphAPI;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class InstagramGraphAPI extends AbstractGraphAPI
{
    public function __construct(HttpClientInterface $client, protected readonly ?string $accountId = null, ?string $appId = null, ?string $appSecret = null, ?string $accessToken = null)
    {
        parent::__construct($client, $appId, $appSecret, $accessToken);
    }

    public function medias(null|int $limit = null): array
    {
        $fields = "media.filtering([{field:'media_type',operator:'EQUAL',value:'CAROUSEL_ALBUM'}])";
        if (null !== $limit) {
            $fields .= sprintf('.limit(%d)', $limit);
        }
        
        $fields .= '{caption,media_url,permalink,thumbnail_url,timestamp,media_type,children{media_url,thumbnail_url},comments_count,like_count}';

        return $this->get($this->accountId, params: ['fields' => $fields]);
    }
}
