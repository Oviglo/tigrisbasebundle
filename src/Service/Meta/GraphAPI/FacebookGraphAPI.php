<?php

namespace Tigris\BaseBundle\Service\Meta\GraphAPI;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class FacebookGraphAPI extends AbstractGraphAPI
{
    public function __construct(HttpClientInterface $client, protected readonly ?string $pageId = null, ?string $appId = null, ?string $appSecret = null, ?string $accessToken = null)
    {
        parent::__construct($client, $appId, $appSecret, $accessToken);
    }

    public function accessToken(): array
    {
        return $this->get('oauth/access_token', [
            'grant_type' => 'client_credentials',
        ]);
    }

    public function events(int $limit = 0, ?string $timeFilter = null, bool $sort = true): array
    {
        $field = new FacebookAPIField(FacebookAPIFieldType::EVENTS, $limit);

        return $field->postRequest($this->get($this->pageId, params: ['fields' => (string) $field]));
    }

    public function posts(int $limit = 0): array
    {
        $field = new FacebookAPIField(FacebookAPIFieldType::POSTS, $limit);

        return $field->postRequest($this->get($this->pageId, params: ['fields' => (string) $field]));
    }

    public function post(string $id): array
    {
        $field = new FacebookAPIField(FacebookAPIFieldType::POST);

        return $field->postRequest($this->get($id, params: ['fields' => (string) $field]));
    }

    public function fields(array $fields): array
    {
        $fieldsStr = '';
        foreach ($fields as $field) {
            if ($field instanceof FacebookAPIField) {
                $fieldsStr .= $field.',';
            }
        }

        $fieldsStr = rtrim($fieldsStr, ',');

        $response = $this->get($this->pageId, params: ['fields' => $fieldsStr]);

        foreach ($fields as $field) {
            if ($field instanceof FacebookAPIField) {
                $response = $field->postRequest($response);
            }
        }

        return $response;
    }

    public function permanentPageAccessToken(): array
    {
        return $this->get($this->pageId, params: ['fields' => 'access_token']);
    }
}
