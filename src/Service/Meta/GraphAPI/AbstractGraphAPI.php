<?php

namespace Tigris\BaseBundle\Service\Meta\GraphAPI;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractGraphAPI implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    public const API_URL = 'https://graph.facebook.com/v20.0/';

    protected array $queryParams = [];
    
    protected HttpClientInterface $client;

    public function __construct(HttpClientInterface $client, ?string $appId = null, ?string $appSecret = null, ?string $accessToken = null)
    {
        $this->queryParams = [
            'client_id' => $appId,
            'client_secret' => $appSecret,
            'access_token' => $accessToken,
        ];

        $this->client = $client->withOptions([
            'base_uri' => self::API_URL,
        ]);
    }

    public function get(string $endpoint = 'me', array $params = []): array
    {
        $params = array_merge($this->queryParams, $params);

        try {
            $response = $this->client->request('GET', $endpoint, [
                'query' => $params,
            ]);

            return json_decode($response->getContent(), true, flags: JSON_THROW_ON_ERROR);
        } catch (TransportExceptionInterface|\JsonException|ClientException $e) {
            $this->logger->warning('AbstractGraphAPI', ['error' => $e::class, 'message' => $e->getMessage()]);

            return [];
        }
    }
}
