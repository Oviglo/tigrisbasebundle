<?php

namespace Tigris\BaseBundle\Service\Meta\GraphAPI;

class FacebookAPIField implements \Stringable
{
    private static array $subFields = [
        'events' => '{cover,name,start_time,end_time,place,description,id,event_times,attending_count}',
        'posts' => '{created_time,message,full_picture,permalink_url,attachments{media_type,media,subattachments{description},url,title,description},likes.summary(true),comments.summary(true),child_attachments}',
        'post' => 'id,story,created_time,message,via,full_picture,permalink_url,sharedposts,insights,attachments{media_type,media{image,source},subattachments{url,title,description,media},url,title,description},likes.summary(true),comments.summary(true),child_attachments',
    ];

    public function __construct(private readonly FacebookAPIFieldType $type, private readonly int $limit = 0)
    {
    }

    public function __toString(): string
    {
        return $this->buildFields();
    }

    public function postRequest(array $response): array
    {
        if (FacebookAPIFieldType::EVENTS === $this->type && isset($response['events'])) {
            $response['events']['data'] = self::sortEvents($response['events']['data'], true, $this->limit);
        }

        return $response;
    }

    private function buildFields(): string
    {
        if (FacebookAPIFieldType::POST === $this->type) {
            return self::$subFields[$this->type->value];
        }

        if (FacebookAPIFieldType::EVENTS === $this->type) {
            return $this->type->value.'.time_filter(upcoming)'.self::$subFields[$this->type->value];
        }

        $fields = $this->type->value;

        return $fields.self::$subFields[$this->type->value];
    }

    public static function sortEvents(array $events, bool $onlyUpcoming = true, int $limit = 20): array
    {
        $allEvents = [];
        foreach ($events as $event) {
            $startTime = new \DateTime($event['start_time']);
            if ($onlyUpcoming && $startTime > new \DateTime()) {
                if (isset($allEvents[$startTime->format('Ymdhis')])) {
                    $startTime->modify('+1 second');
                }
                
                $allEvents[$startTime->format('Ymdhis')] = $event;
            }

            if (isset($event['event_times'])) {
                foreach ($event['event_times'] as $eventTime) {
                    $startTime = new \DateTime($eventTime['start_time']);
                    if ($onlyUpcoming && $startTime < new \DateTime()) {
                        continue;
                    }
                    
                    $event['start_time'] = $eventTime['start_time'];
                    if (isset($allEvents[$startTime->format('Ymdhis')])) {
                        $startTime->modify('+1 second');
                    }
                    
                    $allEvents[$startTime->format('Ymdhis')] = $event;
                }
            }
        }

        \ksort($allEvents);

        if ($limit > 0) {
            $allEvents = array_slice($allEvents, 0, $limit);
        }

        return $allEvents;
    }
}
