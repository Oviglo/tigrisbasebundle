<?php

namespace Tigris\BaseBundle\Service;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class CanonicalUrlGenerator
{
    public function __construct(private readonly RouterInterface $router, private readonly string $siteUrl)
    {
    }

    /**
     * Returns the canonical URL for a route.
     *
     * @param string       $route  route to generate a URL for
     * @param string|array $params string in 'key1=val1&key2=val2' format or array of query parameters
     */
    public function generate(string|null $route, string|array $params = []): string
    {
        if ('' === $route || null === $route) {
            return '';
        }

        $params = $this->getParameters($params);

        $uri = $this->router->generate($route, $params, UrlGeneratorInterface::ABSOLUTE_PATH);

        return \rtrim($this->siteUrl, '/').'/'.\ltrim($uri, '/');
    }

    private function getParameters(string|array $parameters = []): array
    {
        if (\is_string($parameters)) {
            \parse_str($parameters, $parameters);
        }

        if ([] === $parameters) {
            $parameters = [];
        }

        return $parameters;
    }
}
