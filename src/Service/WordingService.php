<?php

namespace Tigris\BaseBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\Wording;
use Tigris\BaseBundle\Repository\WordingRepository;
use Tigris\BaseBundle\Utils\Utils;

class WordingService
{
    private static array $wordings = [];

    public function __construct(
        private readonly WordingRepository $wordingRepository,
        private readonly array $wordingConfig,
        private readonly EntityManagerInterface $em,
        private readonly TranslatorInterface $translator
    ) {
    }

    public function getByPage(string $pageName): array
    {
        $entities = $this->wordingRepository->findByPage($pageName, true);

        try {
            $results = [];
            $wordings = $this->wordingConfig[$pageName]['wordings'];
            foreach ($wordings as $wording) {
                $result = [
                    'page' => $pageName,
                    'name' => $wording['name'],
                    'label' => $this->translator->trans('wording.'.Utils::underscore($wording['name'])),
                    'description' => $wording['description'],
                    'default' => $wording['default'],
                    'type' => $wording['type'],
                ];

                if (isset($entities[$result['name']])) {
                    $result['entity'] = $entities[$result['name']];
                } else {
                    $entity = (new Wording())
                        ->setName($wording['name'])
                        ->setContent($wording['default'])
                        ->setType($wording['type'])
                        ->setPage($pageName)
                    ;

                    $result['entity'] = $entity;
                }

                $results[$wording['name']] = $result;
            }

            return $results;
        } catch (\Throwable) {
            return [];
        }
    }

    public function getFirstPage(): ?string
    {
        try {
            return \array_keys($this->wordingConfig)[0];
        } catch (\OutOfBoundsException) {
            return null;
        }
    }

    public function getPages(): array
    {
        return \array_keys($this->wordingConfig);
    }

    public function content(string $pageName, string $name): string
    {
        if (!array_key_exists($pageName, self::$wordings)) {
            self::$wordings[$pageName] = $this->getByPage($pageName);
        }

        if (array_key_exists($name, self::$wordings[$pageName])) {
            return (string) self::$wordings[$pageName][$name]['entity'] ?? '';
        }

        return '';
    }

    public function get(string $page, string $name): ?array
    {
        if (!isset(self::$wordings[$page]) || !isset(self::$wordings[$page][$name])) {
            self::$wordings[$page] = $this->getByPage($page);
        }

        $wording = self::$wordings[$page][$name];
        $entity = $this->wordingRepository->findOne($page, $name);

        if (!$entity instanceof Wording) {
            $entity = (new Wording())
                ->setName($name)
                ->setContent($wording['default'])
                ->setType($wording['type'])
                ->setPage($page)
            ;

            $this->em->persist($entity);
        }

        return [
            'name' => $wording['name'],
            'description' => $wording['description'],
            'default' => $wording['default'],
            'type' => $wording['type'],
            'label' => $this->translator->trans('wording.'.Utils::underscore($wording['name'])),
            'entity' => $entity,
        ];
    }
}
