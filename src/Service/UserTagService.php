<?php

namespace Tigris\BaseBundle\Service;

use Symfony\Component\Routing\RouterInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Manager\UserManager;

class UserTagService
{
    public function __construct(private readonly UserManager $userManager, private readonly RouterInterface $router)
    {
    }

    public function searchForUser(string $content): array
    {
        $result = [];

        $userManager = $this->userManager;
        preg_replace_callback('/@([[:alnum:]\-_]+)/', function ($matches) use ($userManager, &$result): void {
            $username = str_replace('-', ' ', $matches[1]);
            $user = $userManager->findByUsername($username);

            if ($user instanceof User) {
                $result[] = $user;
            }
        }, $content);

        return $result;
    }

    public function formatText(string $content, array $taggedUsers, string $routeName = null): string
    {
        foreach ($taggedUsers as $taggedUser) {
            $username = $taggedUser->getUsername();
            $tag = '@'.str_ireplace(' ', '-', (string) $username);
            if (null !== $routeName) {
                $url = $this->router->generate($routeName, ['identifier' => $username]);
                $htmlTag = sprintf('<a href="%s" class="user-tag">@%s</a>', $url, $username);
            } else {
                $htmlTag = $username;
            }

            $content = str_ireplace($tag, (string) $htmlTag, $content);
        }

        return $content;
    }
}
