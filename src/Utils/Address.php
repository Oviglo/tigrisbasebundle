<?php

namespace Tigris\BaseBundle\Utils;

use Symfony\Component\Intl\Countries;

class Address
{
    public string $countryCode = '';

    public string $city = '';

    public string $name = '';

    public string $firstName = '';

    public string $civility = '';

    public string $zipCode = '';

    public string $line1 = '';

    public string $line2 = '';

    public float|null $lat = null;

    public float|null $lng = null;

    public function getCountry(): string
    {
        return Countries::getName($this->countryCode);
    }

    public function getFullName(): string
    {
        return strtoupper($this->name ?? '').' '.$this->firstName ?? '';
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public function fromArray(array $address): self
    {
        $vars = get_object_vars($this);
        foreach (array_keys($vars) as $key) {
            if (isset($address[$key])) {
                $this->$key = $address[$key];
            }
        }

        return $this;
    }
}
