<?php

namespace Tigris\BaseBundle\Utils\Sitemap;

class Url
{
    private string $loc;

    private ?\DateTime $lastmod = null;

    private float $priority = 1.00;

    public function getLoc(): string
    {
        return $this->loc ?? '';
    }

    public function setLoc(string $loc): self
    {
        $this->loc = $loc;

        return $this;
    }

    public function getLastmod(): ?\DateTime
    {
        return $this->lastmod;
    }

    public function setLastmod(\DateTime $lastmod = null): self
    {
        $this->lastmod = $lastmod;

        return $this;
    }

    public function getPriority(): float
    {
        return $this->priority;
    }

    public function setPriority(float $priority): self
    {
        $this->priority = $priority;

        return $this;
    }
}
