<?php

namespace Tigris\BaseBundle\Utils;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration
{
    public static function customFormConfiguration(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('fields');

        return $treeBuilder->getRootNode()
            ->useAttributeAsKey('id')
            ->arrayPrototype()
                ->children()
                    ->scalarNode('type')->end()
                    ->scalarNode('label')->end()
                    ->booleanNode('required')->defaultFalse()->end()
                    ->booleanNode('translatable')->defaultFalse()->end()
                    ->booleanNode('expanded')->defaultFalse()->end()
                    ->scalarNode('class')->end()
                    ->booleanNode('multiple')->defaultFalse()->end()
                    ->arrayNode('choices')
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('value')
                        ->scalarPrototype()->end()
                    ->end()
                    ->arrayNode('options')
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('value')
                        ->scalarPrototype()->end()
                    ->end()
                    ->arrayNode('attr')
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('value')
                        ->scalarPrototype()->end()
                    ->end()

                ->end()
            ->end();
    }
}
