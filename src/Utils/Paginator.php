<?php

namespace Tigris\BaseBundle\Utils;

/**
 * Custom Paginator class save iterators and don't execute query for each getIterator call.
 */
class Paginator implements \Countable, \IteratorAggregate
{
    private readonly \Traversable $items;
    
    private readonly int $count;

    public function __construct(\Doctrine\ORM\Tools\Pagination\Paginator $paginator)
    {
        $this->items = $paginator->getIterator();
        $this->count = $paginator->count();
    }

    public function count(): int
    {
        return $this->count;
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->items);
    }
}
