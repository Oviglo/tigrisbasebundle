<?php

namespace Tigris\BaseBundle\Utils;

class Utils
{
    /**
     * Return datasheet alpha value from a number.
     *
     * @param int $num value to convert
     */
    public static function getSheetAlphaFromNumber($num): string
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = (int) ($num / 26);
        if ($num2 > 0) {
            return Utils::getSheetAlphaFromNumber($num2 - 1).$letter;
        } else {
            return $letter;
        }
    }

    public static function lettersToNum($letters): float|int
    {
        $num = 0;
        $arr = array_reverse(str_split((string) $letters));

        foreach ($arr as $i => $singleArr) {
            $num += (ord(strtolower($singleArr)) - 96) * 26 ** $i;
        }

        return $num;
    }

    /**
     * Slugify string.
     */
    public static function slugify(string $text): string
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', (string) $text);
        // remove unwanted characters
        $text = preg_replace('~[^\-\w]+~', '', $text);
        // trim
        $text = trim((string) $text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        // lowercase
        $text = strtolower((string) $text);

        if ('' === $text) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * Remove accents of a string.
     *
     * @param string $str
     *
     * @return string without accents
     */
    public static function stripAccent($str): string
    {
        return strtr(mb_convert_encoding($str, 'ISO-8859-1'), mb_convert_encoding('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', 'ISO-8859-1'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    /**
     * Convert a string to camel case.
     *
     * @param string $string
     * @param bool   $capitalizeFirstCharacter
     *
     * @return string camel case string
     */
    public static function toCamelCase($string, $capitalizeFirstCharacter = false): string
    {
        $str = strtr(ucwords(strtr($string, ['_' => ' ', '.' => '_ ', '\\' => '_ '])), [' ' => '']);

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }

    /**
     * A string to underscore.
     *
     * @param string $id The string to underscore
     *
     * @return string The underscored string
     */
    public static function underscore($id): string
    {
        return strtolower((string) preg_replace(['/([A-Z]+)([A-Z][a-z])/', '/([a-z\d])([A-Z])/'], ['\\1_\\2', '\\1_\\2'], str_replace(' ', '_', str_replace('_', '.', $id))));
    }

    public static function convertPHPToMomentFormat($format): string
    {
        $replacements = [
            'dd' => 'DD',
            'y' => 'YYYY',
        ];

        return strtr($format, $replacements);
    }

    public static function truncateHtml(string $text, int $length = 100, string $ending = '...'): string
    {
        if (\mb_strlen($text) <= $length) {
            return $text;
        }

        $exploder = ' ';
        $allWords = explode($exploder, $text);
        $cutSentence = [];
        $count = 0;
        foreach ($allWords as $one) {
            $cutSentence[] = $one;
            // Word length + space
            $count += mb_strlen($one) + 1;

            if ($count > $length) {
                break;
            }
        }

        return implode($exploder, $cutSentence).$ending;
    }

    public static function getIp()
    {
        // IP si internet partagé
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        // IP derrière un proxy
        elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        // Sinon : IP normale
        else {
            return $_SERVER['REMOTE_ADDR'] ?? '';
        }
    }

    public static function arrayFlatten(array $array): array
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, self::arrayFlatten($value));
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    public static function htmlFilter(string $html): string
    {
        if ($html === '' || $html === '0') {
            return $html;
        }

        return strip_tags($html, '<p><div><table><tbody><thead><tr><th><td><img><h2><h3><h4><h5><h6><small><span><strong><b><u><s><iframe><canvas><ul><ol><li><br><hr><figure><figcaption><a><blockquote><i><code><pre><em>');
    }

    public static function formatUrl(string $string): string
    {
        $string = str_replace('&nbsp;', ' ', $string);

        return preg_replace('@(http(s)?)(://)?(([a-zA-Z])([-\w]*\.)+([^\s\.]+[^\s]*)+[^,.\s])@', '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $string);
    }

    public static function closeTags(string $html = null): ?string
    {
        if (null === $html || '' === $html) {
            return $html;
        }
        
        preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];
        $len_opened = is_countable($openedtags) ? count($openedtags) : 0;
        if ((is_countable($closedtags) ? count($closedtags) : 0) === $len_opened) {
            return $html;
        }
        
        $openedtags = array_reverse($openedtags);
        for ($i = 0; $i < $len_opened; ++$i) {
            if (!in_array($openedtags[$i], $closedtags)) {
                $html .= '</'.$openedtags[$i].'>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags, true)]);
            }
        }

        return $html;
    }

    /*public static function convertToUTF8($var, $deep = true)
    {
        if (is_array($var)) {
            foreach ($var as $key => $value) {
                if ($deep) {
                    $var[$key] = self::convertToUTF8($value, $deep);
                } elseif (!is_array($value) && !is_object($value) && !mb_detect_encoding((string) $value, 'utf-8', true)) {
                    $var[$key] = mb_convert_encoding($var, 'UTF-8', 'ISO-8859-1');
                }
            }

            return $var;
        } elseif (is_object($var)) {
            foreach ($var as $key => $value) {
                if ($deep) {
                    $var->$key = self::convertToUTF8($value, $deep);
                } elseif (!is_array($value) && !is_object($value) && !mb_detect_encoding((string) $value, 'utf-8', true)) {
                    $var->$key = mb_convert_encoding($var, 'UTF-8', 'ISO-8859-1');
                }
            }

            return $var;
        } else {
            return (mb_detect_encoding((string) $var, 'utf-8', true)) ? $var : mb_convert_encoding((string) $var, 'UTF-8', 'ISO-8859-1');
        }
    }*/

    public static function loadEmojis(string $text): string
    {
        json_decode(file_get_contents('https://api.github.com/emojis'), true, 512, JSON_THROW_ON_ERROR);

        return '';
    }

    public static function HTMLToRGB(string $htmlCode): int
    {
        if ('#' === $htmlCode[0]) {
            $htmlCode = substr($htmlCode, 1);
        }

        if (3 == strlen($htmlCode)) {
            $htmlCode = $htmlCode[0].$htmlCode[0].$htmlCode[1].$htmlCode[1].$htmlCode[2].$htmlCode[2];
        }

        $r = hexdec($htmlCode[0].$htmlCode[1]);
        $g = hexdec($htmlCode[2].$htmlCode[3]);
        $b = hexdec($htmlCode[4].$htmlCode[5]);

        return $b + ($g << 0x8) + ($r << 0x10);
    }

    public static function RGBToHSL(int $RGB)
    {
        $h = null;
        $r = 0xFF & ($RGB >> 0x10);
        $g = 0xFF & ($RGB >> 0x8);
        $b = 0xFF & $RGB;

        $r = ((float) $r) / 255.0;
        $g = ((float) $g) / 255.0;
        $b = ((float) $b) / 255.0;

        $maxC = max($r, $g, $b);
        $minC = min($r, $g, $b);

        $l = ($maxC + $minC) / 2.0;

        if ($maxC === $minC) {
            $s = 0;
            $h = 0;
        } else {
            $s = $l < .5 ? ($maxC - $minC) / ($maxC + $minC) : ($maxC - $minC) / (2.0 - $maxC - $minC);
            if ($r === $maxC) {
                $h = ($g - $b) / ($maxC - $minC);
            }
            
            if ($g === $maxC) {
                $h = 2.0 + ($b - $r) / ($maxC - $minC);
            }
            
            if ($b === $maxC) {
                $h = 4.0 + ($r - $g) / ($maxC - $minC);
            }

            $h /= 6.0;
        }

        $h = (int) round(255.0 * $h);
        $s = (int) round(255.0 * $s);
        $l = (int) round(255.0 * $l);

        return (object) ['hue' => $h, 'saturation' => $s, 'lightness' => $l];
    }

    /**
     * Format value type by the value.
     */
    public static function formatValue(mixed $value): mixed
    {
        if ('true' == $value) {
            return true;
        }

        if ('false' == $value) {
            return false;
        }

        return $value;
    }

    /**
     * A simple PHP BBCode Parser function.
     *
     * @author Afsal Rahim
     *
     * @see http://digitcodes.com/create-simple-php-bbcode-parser-function/
     **/

    // BBCode Parser function

    public static function showBBcodes($text): ?string
    {
        // BBcode array
        $find = [
            '~\[b\](.*?)\[/b\]~s',
            '~\[i\](.*?)\[/i\]~s',
            '~\[u\](.*?)\[/u\]~s',
            '~\[quote\](.*?)\[/quote\]~s',
            '~\[size=(.*?)\](.*?)\[/size\]~s',
            '~\[color=(.*?)\](.*?)\[/color\]~s',
            '~\[url\]((?:ftp|https?)://.*?)\[/url\]~s',
            '~\[img\](https?://.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]~s',
        ];

        // HTML tags to replace BBcode
        $replace = [
            '<b>$1</b>',
            '<i>$1</i>',
            '<span style="text-decoration:underline;">$1</span>',
            '<pre>$1</pre>',
            '<span style="font-size:$1px;">$2</span>',
            '<span style="color:$1;">$2</span>',
            '<a href="$1">$1</a>',
            '<img src="$1" alt="" />',
        ];

        // Replacing the BBcodes with corresponding HTML tags
        return preg_replace($find, $replace, (string) $text);
    }

    /**
     * Remove style attribute from html tag.
     *
     * @param $html Html code
     *
     * @return Html code without style attr
     */
    public static function removeStyleAttr(string $html): string
    {
        return preg_replace('/(<[^>]+) style=".*?"/i', '$1', $html);
    }

    public static function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K'): float
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper((string) $unit);

        if ('K' === $unit) {
            return round($miles * 1.609344, 2);
        } elseif ('N' === $unit) {
            return round($miles * 0.8684, 2);
        } else {
            return round($miles, 2);
        }
    }

    public static function extractUrls($string)
    {
        $matches = [];
        preg_match_all('#https?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', (string) $string, $matches);

        return $matches[0];
    }

    public static function convertJsDateTime($jsDateTime, $format = 'Y-m-d h:i:s'): string
    {
        if (str_contains((string) $jsDateTime, 'GMT')) {
            $jsDateTime = substr((string) $jsDateTime, 0, strpos((string) $jsDateTime, 'GMT'));
        }

        return date($format, strtotime((string) $jsDateTime));
    }

    public static function toPointCase(string $text): string
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '.', $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', (string) $text);
        // trim
        $text = trim($text, '.');
        // lowercase
        $text = strtolower($text);

        if ('' === $text) {
            return 'n-a';
        }

        return $text;
    }
}
