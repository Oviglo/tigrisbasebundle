<?php

namespace Tigris\BaseBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;

class UserVoter extends Voter
{
    final public const VIEW = 'view';
    
    final public const EDIT = 'edit';

    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        return $subject instanceof User && in_array($attribute, [self::VIEW, self::EDIT]);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView($subject),
            self::EDIT => $this->canEdit($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    private function canView(User $subject): bool
    {
        return $this->authorizationChecker->isGranted('ROLE_ADMIN') || $subject->isEnabled();
    }

    private function canEdit(User $subject, User|null $user): bool
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return $subject->getUsername() === $user->getUsername();
    }
}
