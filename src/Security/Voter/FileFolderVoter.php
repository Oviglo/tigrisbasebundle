<?php

namespace Tigris\BaseBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\FileFolder;
use Tigris\BaseBundle\Entity\Model\User;

class FileFolderVoter extends Voter
{
    final public const UPLOAD = 'upload';
    
    final public const VIEW = 'view';
    
    final public const EDIT = 'edit';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::UPLOAD])) {
            return false;
        }

        if (!$subject instanceof FileFolder || self::VIEW !== $attribute) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        return match ($attribute) {
            self::UPLOAD => $this->canUpload($subject, $user),
            self::VIEW => $this->canView($subject, $user),
            self::EDIT => $this->canEdit($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    private function canUpload(FileFolder $subject, User $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_FILE_ADMIN')) {
            return true;
        }

        return $user === $subject->getUser();
    }

    private function canView(FileFolder $subject, User $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_FILE_ADMIN')) {
            return true;
        }

        return $user === $subject->getUser();
    }

    private function canEdit(FileFolder $subject, User $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_FILE_ADMIN')) {
            return true;
        }

        return $user === $subject->getUser();
    }
}
