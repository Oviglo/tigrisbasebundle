<?php

namespace Tigris\BaseBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Entity\Model\User;

class FileVoter extends Voter
{
    final public const UPLOAD = 'upload';
    
    final public const VIEW = 'view';
    
    final public const EDIT = 'edit';

    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::UPLOAD])) {
            return false;
        }

        if (!$subject instanceof File && self::UPLOAD !== $attribute) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            $user = null;
        }

        return match ($attribute) {
            self::UPLOAD => $this->canUpload($subject, $user),
            self::VIEW => $this->canView(),
            self::EDIT => $this->canEdit($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    private function canUpload(File $subject = null, User $user = null): bool
    {
        if (!$subject instanceof File) {
            return false;
        }

        if (
            null != $subject->getFile()
            && in_array($subject->getFile()->getMimeType(), ['text/html', 'text/x-php'])
        ) {
            return false;
        }

        if (!$user instanceof User) {
            // Only this types for an anonymous user
            return in_array($subject->getFile()->getMimeType(), ['application/pdf', 'image/jpeg', 'image/png']);
        }

        return true;
    }

    private function canView(): bool
    {
        return true;
    }

    private function canEdit(File $subject, User $user = null): bool
    {
        if (null == $user) {
            return false;
        }

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN') || $this->authorizationChecker->isGranted('ROLE_FILE_ADMIN')) {
            return true;
        }

        return $user === $subject->getUser();
    }
}
