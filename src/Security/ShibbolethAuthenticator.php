<?php

namespace Tigris\BaseBundle\Security;

use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Contracts\Security\User\ShibbolethUserInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Event\SecurityEvent;
use Tigris\BaseBundle\Event\SecurityEvents;
use Tigris\BaseBundle\Repository\UserRepository;

class ShibbolethAuthenticator extends AbstractAuthenticator implements AuthenticationEntryPointInterface
{
    /**
     * @var string
     */
    private $login_path;

    /**
     * @var string
     */
    private $login_target;

    /**
     * @var string
     */
    private $username;

    /**
     * @var array
     */
    private $attributes;

    final public const REGISTRATION_ROUTE = 'tigris_base_registration_register';

    /**
     * ShibbolethGuardAuthenticator constructor.
     */
    public function __construct(
        array $config,
        private readonly RouterInterface $router,
        private readonly UserRepository $userRepository,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly LoggerInterface $logger
    ) {
        $this->login_path = $config['login_path'];
        $this->login_target = $config['login_target'];
        $this->username = $config['username'];
        $this->attributes = $config['attributes'];
        if (!in_array($this->username, $this->attributes)) {
            throw new InvalidConfigurationException('Shibboleth configuration error : the value of username parameter must be in attributes list parameter');
        }
    }

    public function supports(Request $request): ?bool
    {
        return self::REGISTRATION_ROUTE !== $request->attributes->get('_route')
            && '' !== $this->getAttribute($request, 'eppn');
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new RedirectResponse($request->getSchemeAndHttpHost() . '/'.trim($this->login_path, '/').'?target='.(empty($this->login_target) ? $request->getUri() : $request->getSchemeAndHttpHost() . $this->router->generate($this->login_target)));
    }

    public function authenticate(Request $request): Passport
    {
        $this->getAttribute($request, 'eppn');
        $email = $this->getAttribute($request, 'mail');

        return new Passport(
            new UserBadge($email, fn ($userIdentifier): ?object => $this->userRepository->findOneBy(['email' => $userIdentifier])),
            new CustomCredentials(function (string $credentials, UserInterface $user) {
                $this->logger->info('Try Shibboleth connexion with: '.$credentials);
                if ($user instanceof User && $user instanceof ShibbolethUserInterface) {
                    return $user->getShibbolethEppn() === $credentials || $user->getEmail() === $credentials;
                }
            }, $email)
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];
        $response = new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
        $event = new SecurityEvent($response);

        $this->eventDispatcher->dispatch($event, SecurityEvents::AUTHENTICATION_FAILURE);

        return $event->getResponse();
    }

    private function getAttribute(Request $request, string $name): string
    {
        $attributes = [$name, strtoupper($name), 'HTTP_'.strtoupper($name), 'REDIRECT_' . $name];
        foreach ($attributes as $attribute) {
            if (!empty($request->server->has($attribute))) {
                return $request->server->get($attribute);
            }
        }

        return '';
    }
}
