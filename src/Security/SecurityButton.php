<?php

namespace Tigris\BaseBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class SecurityButton
{
    public const SESSION_NAME = 'securityButtonToken';

    public function __construct(private readonly RequestStack $requestStack) {}

    public function generateToken(string $key = ''): string
    {
        $token = bin2hex(random_bytes(32).$key);

        $session = $this->requestStack->getSession();
        $session->set(static::SESSION_NAME, $token);

        return $token;
    }

    public function getToken(): string
    {
        $session = $this->requestStack->getSession();

        return $session->get(static::SESSION_NAME, '');
    }

    public function isTokenValid(Request|string $token): bool
    {
        $session = $this->requestStack->getSession();
        $sessionToken = $session->get(static::SESSION_NAME);

        if ($token instanceof Request) {
            $token = $token->get(static::SESSION_NAME);
        }

        return $sessionToken === $token;
    }
}
