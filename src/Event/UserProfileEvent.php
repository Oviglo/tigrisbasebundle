<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\UserProfile\UserProfileManager;
use Tigris\BaseBundle\UserProfile\UserProfileTab;

class UserProfileEvent extends Event
{
    protected UserProfileManager $profile;

    public function __construct(protected User $user, protected User|null $viewer = null)
    {
        $this->profile = new UserProfileManager();
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getViewer(): User|null
    {
        return $this->viewer;
    }

    public function setViewer(User|null $viewer): self
    {
        $this->viewer = $viewer;

        return $this;
    }

    public function getProfile(): UserProfileManager
    {
        return $this->profile;
    }

    public function addTab(UserProfileTab $tab): self
    {
        $this->profile->addTab($tab);

        return $this;
    }

    public function getViewParams(): array
    {
        return $this->profile->getViewParams();
    }
}
