<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tigris\BaseBundle\Dashboard\DashboardCard;

class DashboardEvent extends Event
{
    private array $cards = [];

    public function addCard(DashboardCard $card): static
    {
        $group = $card->getGroup();

        if (!isset($this->cards[$group])) {
            $this->cards[$group] = [];
        }
        
        $this->cards[$group][] = $card;

        return $this;
    }

    /**
     * @return array<DashboardCard>
     */
    public function getCards(string $group = null): array
    {
        if (null === $group) {
            return $this->cards;
        }

        if (isset($this->cards[$group])) {
            $returnCards = $this->cards[$group];
            unset($this->cards[$group]);

            return $returnCards;
        }

        return [];
    }
}
