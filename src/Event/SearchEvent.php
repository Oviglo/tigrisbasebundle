<?php

namespace Tigris\BaseBundle\Event;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\EventDispatcher\Event;
use Tigris\BaseBundle\Search\SearchResult;
use Tigris\BaseBundle\Utils\Utils;

class SearchEvent extends Event
{
    private array $results = [];

    public function __construct(
        private readonly string $search,
        private readonly array $options,
        private readonly EntityManagerInterface $entityManager,
        private readonly RouterInterface $router
    ) {
    }

    public function getSearch(): string
    {
        return $this->search;
    }

    public function addResult(array|SearchResult $result): self
    {
        if ($result instanceof SearchResult) {
            $result = $result->toArray();
            $result['url'] = $this->router->generate($result['route'], $result['routeParams']);
        }
        
        // $result['content'] = Utils::convertToUTF8($result['content']);
        $this->results[] = $result;

        return $this;
    }

    public function getResults(): array
    {
        return $this->results;
    }

    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    public function getRouter(): Router
    {
        return $this->router;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
