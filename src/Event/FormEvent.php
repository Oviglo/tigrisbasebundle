<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Component\Form\Form;
use Symfony\Contracts\EventDispatcher\Event;

class FormEvent extends Event
{
    public function __construct(private Form $form)
    {
        $this->form = $form;
    }

    public function getForm(): Form
    {
        return $this->form;
    }

    public function getFormData(): mixed
    {
        return $this->form->getData();
    }

    public function addField(string $name, string $type = null, array $options = []): Form
    {
        return $this->form->add($name, $type, $options);
    }

    public function setForm(Form $form): self
    {
        $this->form = $form;

        return $this;
    }
}
