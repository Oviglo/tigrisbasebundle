<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tigris\BaseBundle\Entity\Model\User;

class UserUpdateEvent extends Event
{
    public function __construct(private readonly User $oldUser, private readonly User $newUser)
    {
    }

    public function getOldUser(): User
    {
        return $this->oldUser;
    }

    public function getNewUser(): User
    {
        return $this->newUser;
    }
}
