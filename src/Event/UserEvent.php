<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Component\EventDispatcher\GenericEvent;
use Tigris\BaseBundle\Entity\Model\User;

class UserEvent extends GenericEvent
{
    public function __construct(private User $user)
    {
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
