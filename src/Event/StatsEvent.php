<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tigris\BaseBundle\Utils\Stats;

class StatsEvent extends Event
{
    private array $stats = [];

    public function __construct()
    {
    }

    public function addStats(Stats $stats): self
    {
        $this->stats[] = $stats;

        return $this;
    }

    public function getStats(): array
    {
        return $this->stats;
    }

    public function setStats(array $stats): self
    {
        $this->stats = $stats;

        return $this;
    }
}
