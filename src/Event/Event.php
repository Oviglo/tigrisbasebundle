<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

class Event extends GenericEvent
{
    public function __construct(private readonly array $params = [], private array $data = [])
    {
        $entity = null;
        if (isset($params['entity'])) {
            $entity = $params['entity'];
        }

        parent::__construct($entity, array_merge($params, $data));
    }

    /**
     * @return array Return event params
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $data Event data
     *
     * @return object Return event object
     */
    public function setData(array $data): static
    {
        $this->data = $data;
        foreach ($data as $key => $value) {
            $this->setArgument($key, $value);
        }

        return $this;
    }

    /**
     * @return array Return event data
     */
    public function getData(): array
    {
        return $this->getArguments();
    }
}
