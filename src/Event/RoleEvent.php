<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class RoleEvent extends Event
{
    private array $roles = [];

    public function addRole(string $role): RoleEvent
    {
        $role = strtoupper($role);
        if (!isset($this->roles[$role])) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function addRoles(array $roles): RoleEvent
    {
        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }
}
