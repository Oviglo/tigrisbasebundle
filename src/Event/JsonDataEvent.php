<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class JsonDataEvent extends Event
{
    public function __construct(private array $jsonData = [])
    {
    }

    public function getJsonData(): array
    {
        return $this->jsonData;
    }

    public function setJsonData(array $jsonData): self
    {
        $this->jsonData = $jsonData;

        return $this;
    }
}
