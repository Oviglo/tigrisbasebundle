<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tigris\BaseBundle\Utils\Sitemap\Url;

class SitemapEvent extends Event
{
    /**
     * @var array<Url>
     */
    private array $urls = [];

    public function addUrl(Url $url): self
    {
        $this->urls[] = $url;

        return $this;
    }

    public function getUrls(): array
    {
        return $this->urls;
    }
}
