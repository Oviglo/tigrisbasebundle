<?php

namespace Tigris\BaseBundle\Event;

final class SecurityEvents
{
    public const PRE_LOAD = 'tigris_base.security.pre_load';
    
    public const AUTHENTICATION_FAILURE = 'tigris_base.security.authentication_failure';
    
    public const AUTHENTICATION_SUCCESS = 'tigris_base.security.authentication_success';
}
