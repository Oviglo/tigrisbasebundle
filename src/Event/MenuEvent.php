<?php

namespace Tigris\BaseBundle\Event;

use Knp\Menu\ItemInterface;
use Symfony\Contracts\EventDispatcher\Event;

class MenuEvent extends Event
{
    public function __construct(private ItemInterface $menu, private readonly array $options = [])
    {
    }

    public function getMenu(): ItemInterface
    {
        return $this->menu;
    }

    public function setMenu(ItemInterface $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    public function addChild($child, array $options = []): ItemInterface
    {
        return $this->menu->addChild($child, $options);
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
