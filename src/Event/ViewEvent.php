<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class ViewEvent extends Event
{
    private array $viewParams = [];

    public function setViewParams(array $params): self
    {
        $this->viewParams = $params;

        return $this;
    }

    public function getViewParams(): array
    {
        return $this->viewParams;
    }

    public function addViewParam(string $key, mixed $value): self
    {
        $this->viewParams[$key] = $value;

        return $this;
    }
}
