<?php

namespace Tigris\BaseBundle\Event;

abstract class Events
{
    final public const AFTER_CREATE_USER = 'tigris_base.after_create_user';
    
    final public const AFTER_UPDATE_USER = 'tigris_base.after_update_user';
    
    final public const AFTER_REMOVE_USER = 'tigris_base.after_remove_user';

    final public const LOAD_ADMIN_MENU = 'tigris_base.load_admin_menu';
    
    final public const LOAD_MAIN_MENU = 'tigris_base.load_main_menu';
    
    final public const LOAD_USER_MENU = 'tigris_base.load_user_menu';
    
    final public const LOAD_CONFIGS = 'tigris_base.load_configs';

    final public const LOAD_STATS = 'tigris_base.load_stats';

    final public const LOAD_ROLES = 'tigris_base.load_roles';

    final public const WYSIWYG_GENERATE_OPTIONS = 'tigris_base.wysiwyg_generate_options';
    
    final public const WYSIWYG_GENERATE_VIEWS = 'tigris_base.wysiwyg_generate_views';

    final public const USER_SHOW = 'tigris_base.user_show';
    
    final public const USER_PROFILE_SHOW = 'tigris_base.user_profile_show';
    
    final public const LOAD_MY_ACCOUNT_MENU = 'tigris_base.my_account_menu_load';
    
    final public const LOAD_USER_PROFILE_MENU = 'tigris_base.user_profile_menu_load';

    final public const NOTIFY = 'tigris_base.notify';
    
    final public const DELETE_NOTIFICATIONS = 'tigris_base.delete_notifications';

    final public const AJAX_CRON = 'tigris_base.ajax_cron';
    
    final public const CRON = 'tigris_base.cron';

    final public const SEARCH = 'tigris_base.search';
    
    final public const LOAD_SEARCH_OPTIONS = 'tigris_base.load_search_options';

    final public const LOAD_DATA_IMPORTER_TYPE = 'tigris_base.load_dataimporter_type';

    final public const LOAD_DATA_IMPORTER_FORM = 'tigris_base.dataimporter_form';

    final public const LOAD_ADMIN_DASHBOARD = 'tigris_base.load_admin_dashboard';

    final public const CREATE_USER = 'tigris_base.create_user';

    final public const GENERATE_PAGINATOR_TO_JSON = 'tigris_base.generate_paginator_to_json';

    final public const GENERATE_SITEMAP = 'tigris_base.generate_sitemap';
}
