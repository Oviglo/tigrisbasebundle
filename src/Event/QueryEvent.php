<?php

namespace Tigris\BaseBundle\Event;

use Doctrine\ORM\QueryBuilder;
use Symfony\Contracts\EventDispatcher\Event;

class QueryEvent extends Event
{
    public function __construct(private QueryBuilder $queryBuilder, private array $criteria = [])
    {
    }

    public function getBuilder(): QueryBuilder
    {
        return $this->queryBuilder;
    }

    public function setBuilder(QueryBuilder $queryBuilder): self
    {
        $this->queryBuilder = $queryBuilder;

        return $this;
    }

    public function getCriteria(): array
    {
        return $this->criteria;
    }

    public function setCriteria(array $criteria): self
    {
        $this->criteria = $criteria;

        return $this;
    }
}
