<?php

namespace Tigris\BaseBundle\Event;

final class RegistrationEvents
{
    public const SUCCESS = 'tigris_base.registration.success';
    
    public const PRE_LOAD = 'tigris_base.registration.pre_load';
}
