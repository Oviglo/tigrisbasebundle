<?php

namespace Tigris\BaseBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Allow to collect data from SourceEvent js
 */
class SourceEvent extends Event
{
    private bool $updated = false;

    public function __construct(private ?\DateTime $lastCalledAt = null, private array $data = [])
    {
        
    }

    public function getData(): array 
    {
        return $this->data;
    }

    public function addData(string $name, array|string|int $value): void
    {
        $this->updated = true;
        $this->data[$name] = $value;
    }

    public function isUpdated(): bool
    {
        return $this->updated;
    }

    public function getLastCalledAt(): \DateTime
    {
        if (null === $this->lastCalledAt) {
            $this->lastCalledAt = new \DateTime();
        }

        return $this->lastCalledAt;
    }
}