<?php

namespace Tigris\BaseBundle\MessageHandler;

use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Tigris\BaseBundle\Message\AppNotification;

#[AsMessageHandler]
class AppNotificationHandler
{
    public function __invoke(AppNotification $message)
    {
        // do something with your message
    }
}
