<?php

namespace Tigris\BaseBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\RoleEvent;

class RoleListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_ROLES => 'onLoadRoles',
        ];
    }

    public function onLoadRoles(RoleEvent $event): void
    {
        $event->addRoles([
            'ROLE_ADMIN',
        ]);
    }
}
