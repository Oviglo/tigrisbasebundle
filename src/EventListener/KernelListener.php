<?php

namespace Tigris\BaseBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Tigris\BaseBundle\Entity\OnlineUser;
use Tigris\BaseBundle\Repository\OnlineUserRepository;
use Tigris\BaseBundle\Service\Meta\FacebookPixel;
use Twig\Environment;

class KernelListener implements EventSubscriberInterface
{
    public function __construct(private readonly OnlineUserRepository $userOnlineRepository, private readonly EntityManagerInterface $entityManager, private readonly Environment $template, private readonly array $configurations, private readonly FacebookPixel $facebookPixel) {}

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onRequest',
            KernelEvents::RESPONSE => 'onResponse',
        ];
    }

    public function onRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if (!$event->isMainRequest() || 'html' != $request->getRequestFormat()) {
            return;
        }

        if ($this->configurations['active_online_counter']) {
            $ip = $request->getClientIp();
            $url = $request->getRequestUri();

            $onlineUser = $this->userOnlineRepository->findOneByIp($ip);

            if (null == $onlineUser) {
                $onlineUser = (new OnlineUser())
                    ->setIp($ip)
                    ->setUrl($url)
                ;

                $this->entityManager->persist($onlineUser);
            }

            $onlineUser
                ->setOnlineAt(new \DateTime())
                ->setUrl($url)
            ;
            $this->entityManager->flush();

            $onlineUser = [
                'onlineCount' => $this->userOnlineRepository->onlineCount(),
                'totalCount' => $this->userOnlineRepository->count([]),
            ];
            $this->template->addGlobal('onlineUser', $onlineUser);
        }
    }

    public function onResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        if (!$event->isMainRequest() || 'html' != $request->getRequestFormat() || $response instanceof BinaryFileResponse || $response instanceof StreamedResponse) {
            return;
        }

        $content = $response->getContent();

        if (!empty($this->configurations['facebook']['pixel_id'])) {
            $content = str_replace('</head>', $this->facebookPixel->getPixelHtmlCode().'</head>', $content);
        }

        $response->setContent($content);
        $event->setResponse($response);
    }
}
