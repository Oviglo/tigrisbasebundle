<?php

namespace Tigris\BaseBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\CronEvent;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Manager\DataImportManager;

class DataImporterListener implements EventSubscriberInterface
{
    public function __construct(private readonly DataImportManager $dataImporterManager)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::CRON => 'onCronRun',
        ];
    }

    public function onCronRun(CronEvent $event): void
    {
        $this->dataImporterManager->execute();
    }
}
