<?php

namespace Tigris\BaseBundle\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Tigris\BaseBundle\Entity\File;

#[AsDoctrineListener(Events::postLoad)]
class DoctrineListener
{
    public function __construct(private readonly array $imageStyles)
    {
    }

    public function postLoad(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof File) {
            $entity->setImageStyles($this->imageStyles);
        }
    }
}
