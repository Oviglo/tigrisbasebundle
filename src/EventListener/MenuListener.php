<?php

namespace Tigris\BaseBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;

class MenuListener implements EventSubscriberInterface
{
    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_MAIN_MENU => 'onLoadMainMenu',
        ];
    }

    public function onLoadMainMenu(MenuEvent $event): void
    {
        $menu = $event->getMenu();

        if ($this->authorizationChecker->isGranted(['ROLE_SUPER_ADMIN', 'ROLE_ADMIN'])) {
            $menu->addChild('menu.admin', ['route' => 'tigris_base_admin_user_index']);
        }
    }
}
