<?php

namespace Tigris\BaseBundle\Mailing;

use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Notification\NotificationBuilderInterface;

class NotificationEmail extends AbstractEmail
{
    public function __invoke(NotificationBuilderInterface $builder, User $user): void
    {
        $email = $this->getTemplatedEmail()
            ->subject($this->translator->trans($builder->getTitle()))
            ->htmlTemplate('@TigrisBase/email/notification.html.twig')
            ->context([
                'entity' => $user,
                'data' => $builder->build(),
            ])
        ;

        $this->send($email);
    }
}
