<?php

namespace Tigris\BaseBundle\Mailing;

use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\File;
use Tigris\BaseBundle\Entity\DataExport;

class ExportFileEmail extends AbstractEmail
{
    public function __invoke(DataExport $dataExport): void
    {
        $emailAddress = $dataExport->getSendToMail();
        if (null === $emailAddress) {
            return;
        }

        $email = $this->getTemplatedEmail()
            ->to($emailAddress)
            ->subject($this->translator->trans('data.exporter.email.subject'))
            ->htmlTemplate('@TigrisBase/email/data_export_file.html.twig')
            ->context([
                'entity' => $dataExport,
            ])
            ->addPart(new DataPart(new File($dataExport->getAbsolutePath())))
        ;

        $this->send($email);
    }
}
