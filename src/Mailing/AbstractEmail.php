<?php

namespace Tigris\BaseBundle\Mailing;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Service\WordingService;

class AbstractEmail implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __construct(
        protected readonly MailerInterface $mailer,
        protected readonly TranslatorInterface $translator,
        protected readonly ConfigManager $configManager,
        protected readonly WordingService $wordingService
    ) {
    }

    protected function getTemplatedEmail(): TemplatedEmail
    {
        $siteTitle = $this->configManager->getValue('TigrisBaseBundle.title', '');
        $notificationMail = $this->configManager->getValue('TigrisBaseBundle.noreply_mail', '');

        return (new TemplatedEmail())
            ->from(new Address($notificationMail, $siteTitle))
        ;
    }

    protected function send(TemplatedEmail $email): bool
    {
        try {
            $this->mailer->send($email);

            return true;
        } catch (TransportExceptionInterface $transportException) {
            $this->logger->error('An error occurred while sending an email', [
                'exception' => $transportException,
            ]);

            return false;
        }
    }
}
