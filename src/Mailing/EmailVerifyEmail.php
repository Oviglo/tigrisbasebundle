<?php

namespace Tigris\BaseBundle\Mailing;

use Symfony\Component\Mime\Address;
use Tigris\BaseBundle\Entity\Model\User;

class EmailVerifyEmail extends AbstractEmail
{
    public function __invoke(User $user, string $verifyUrl): void
    {
        $email = $this->getTemplatedEmail()
            ->to(new Address($user->getEmail()))
            ->subject($this->translator->trans('email.verify.subject'))
            ->htmlTemplate('@TigrisBase/email/user_mail_verify.html.twig')
            ->context([
                'user' => $user,
                'verifyUrl' => $verifyUrl,
            ])
        ;

        $this->send($email);
    }
}
