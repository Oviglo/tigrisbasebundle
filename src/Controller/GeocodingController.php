<?php

namespace Tigris\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Tigris\BaseBundle\Contracts\Geocoding\GeocodingInterface;

#[Route('/geocoding')]
class GeocodingController extends BaseController
{
    #[Route('/search', options: ['expose' => true], methods: ['GET'])]
    public function search(Request $request, GeocodingInterface $geocoding): JsonResponse
    {
        $q = $request->get('q', '');
        $response = [];

        if (!empty($q)) {
            $response = $geocoding->searchAddress($q)->toArray();
        }

        return new JsonResponse($response);
    }
}
