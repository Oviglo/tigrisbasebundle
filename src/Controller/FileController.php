<?php

namespace Tigris\BaseBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Entity\FileFolder;
use Tigris\BaseBundle\Form\Type\FileEditType;
use Tigris\BaseBundle\Manager\FileManager;
use Tigris\BaseBundle\Repository\FileFolderRepository;
use Tigris\BaseBundle\Repository\FileRepository;
use Tigris\BaseBundle\Security\Voter\FileVoter;
use Tigris\BaseBundle\Traits\FormTrait;

/**
 * File controller.
 */
#[Route(path: '/basefile')]
class FileController extends BaseController
{
    use FormTrait;

    #[Route(path: '/index', options: ['expose' => true])]
    #[IsGranted('ROLE_USER')]
    public function index(): Response
    {
        return $this->render('@TigrisBase/file/index.html.twig');
    }

    #[Route(path: '/list', options: ['expose' => true])]
    #[IsGranted('ROLE_USER')]
    public function list(Request $request, FileRepository $fileRepository, FileManager $fileManager): JsonResponse
    {
        $criteria = $this->getCriteria($request, 'file_data');
        $criteria['search'] = $request->query->get('search', '');
        $criteria['public'] = $request->query->get('public', 1);
        $criteria['type'] = $request->query->get('type', 'all');
        $criteria['folder'] = (int) $request->query->get('folder');

        if (0 === $criteria['folder'] && !$this->isGranted('ROLE_ADMIN')) {
            $folderEntity = $fileManager->getUserFolder($this->getUser());
            $criteria['folder'] = $folderEntity->getId();
        }

        $data = $fileRepository->findData($criteria);

        $this->saveCriteria($criteria, 'file_data');

        return $this->paginatorToJsonResponse($data);
    }

    #[Route(path: '/upload/{folder}', requirements: ['folder' => '\d+'], defaults: ['folder' => null], options: ['expose' => true], methods: ['GET', 'POST'])]
    public function upload(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator, FileFolderRepository $fileFolderRepository, ?FileFolder $folder = null): JsonResponse
    {
        $uploadedFile = $request->files->get('file');
        $entity = (new File())
            ->setFile($uploadedFile)
            ->setUser($this->getUser())
        ;

        // If the user is not an admin, load the file in the user folder
        if (!$this->isGranted('ROLE_ADMIN')) {
            $folder = $fileFolderRepository->findUserFolder($this->getUser());
        }

        if (null != $folder) {
            $entity->setFolder($folder);
        }

        if ($entity->isValid() && $this->isGranted(FileVoter::UPLOAD, $entity)) {
            $entityManager->persist($entity);
            $entityManager->flush();

            return new JsonResponse(['status' => 'success', 'message' => $translator->trans('file.upload.success'), 'id' => $entity->getId(), 'path' => $entity->getWebPath()]);
        }

        return new JsonResponse(['status' => 'error', 'message' => '', 'errors' => []]);
    }

    #[Route(path: '/delete', options: ['expose' => true], methods: ['GET', 'DELETE'])]
    #[IsGranted('ROLE_USER')]
    public function delete(Request $request, FileRepository $fileRepository, EntityManagerInterface $entityManager): Response
    {
        $selected = $request->query->get('ids', null);
        if (null === $selected) {
            return $this->redirectToRoute('tigris_base_file_index');
        }

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('tigris_base_file_delete', ['ids' => $selected]))
            ->setMethod(Request::METHOD_DELETE)
            ->getForm()
        ;
        $this->addFormActions($form, $this->generateUrl('tigris_base_file_index'));

        $ids = explode(',', $selected);
        $entities = $fileRepository->findByIds($ids);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($entities as $entity) {
                if ($this->isGranted(FileVoter::EDIT, $entity)) {
                    $entityManager->remove($entity);
                }
            }

            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => '']);
            } else {
                return $this->redirectToRoute('tigris_base_file_index');
            }
        }

        return $this->render('@TigrisBase/file/delete.html.twig', ['form' => $form, 'entities' => $entities]);
    }

    #[Route(path: '/download/', name: 'tigris_base_file_download_zip', options: ['expose' => true])]
    #[Route(path: '/download/{slug}.{_format}')]
    public function download(Request $request, FileRepository $fileRepository, EntityManagerInterface $em, ?File $entity = null): Response
    {
        $response = new Response();
        if (!$entity instanceof File) {
            $ids = explode(',', $request->query->get('ids'));
            if (0 == count($ids)) {
                throw new \Exception('File id not found');
            }

            $files = $fileRepository->findByIds($ids);
            $zip = new \ZipArchive();
            $zipName = time().'.zip';
            if (true !== $zip->open($zipName, \ZipArchive::CREATE)) {
                throw new \Exception('Cannot create zip file');
            }

            foreach ($files as $file) {
                $zip->addFile($file->getWebPath(), $file->getPath());
            }

            $zip->close();

            if (file_exists($zipName)) {
                $response->headers->set('Cache-Control', 'public');
                $response->headers->set('Content-type', 'application/octet-stream');
                $response->headers->set('Content-Disposition', 'attachment; filename="'.$zipName.'"');
                $response->headers->set('Content-length', filesize($zipName));
                $response->sendHeaders();

                $response->setContent(readfile($zipName));
                unlink($zipName);

                return $response;
            }
        } else {
            if ('file' === $entity->getType()) {
                $entity->addDownload();
                $em->persist($entity);
                $em->flush();
            }

            $file = finfo_open();
            $mime = finfo_file($file, $entity->getAbsolutePath(), FILEINFO_MIME);
            finfo_close($file);

            $response = new Response();
            $response->headers->set('Cache-Control', 'private');
            $response->headers->set('Content-type', $mime);
            $response->headers->set('Content-length', filesize($entity->getAbsolutePath()));
            $response->sendHeaders();

            $response->setContent(readfile($entity->getAbsolutePath()));
        }

        return $response;
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/{id<\d+>}/edit', methods: ['GET', 'PUT'], options: ['expose' => true])]
    public function edit(File $entity, Request $request, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted(FileVoter::EDIT, $entity);

        $form = $this->createForm(FileEditType::class, $entity, [
            'method' => Request::METHOD_PUT,
            'action' => $this->generateUrl('tigris_base_file_edit', ['id' => $entity->getId()]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            return new JsonResponse(['status' => 'success', 'message' => '']);
        }

        return $this->render('@TigrisBase/file/edit.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }
}
