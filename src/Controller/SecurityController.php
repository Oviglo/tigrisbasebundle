<?php

namespace Tigris\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Event\SecurityEvent;
use Tigris\BaseBundle\Event\SecurityEvents;

class SecurityController extends BaseController
{
    #[Route('/login')]
    public function login(AuthenticationUtils $authenticationUtils, EventDispatcherInterface $eventDispatcher): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $response = $this->render('@TigrisBase/security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
        $preLoadEvent = new SecurityEvent($response);

        $eventDispatcher->dispatch($preLoadEvent, SecurityEvents::PRE_LOAD);

        return $preLoadEvent->getResponse();
    }

    #[Route('/logout')]
    public function logout(): never
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
