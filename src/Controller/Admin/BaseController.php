<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Tigris\BaseBundle\Controller\BaseController as ParentController;

class BaseController extends ParentController
{
    public function generateBreadcrumbs(array $items = []): void
    {
        $items = array_merge([
            'menu.admin' => ['route' => 'tigris_base_admin_admin_dashboard'],
        ], $items);

        parent::generateBreadcrumbs($items);
    }
}
