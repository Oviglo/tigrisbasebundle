<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Crud\Crud;
use Tigris\BaseBundle\Traits\FormTrait;

abstract class AbstractCrudController extends BaseController
{
    use FormTrait;

    protected ?Crud $crud = null;

    final public const VIEW_PATH_CREATE = 'create';

    final public const VIEW_PATH_UPDATE = 'update';

    final public const VIEW_PATH_DELETE = 'delete';

    final public const VIEW_PATH_SORT = 'sort';

    abstract protected function generateCrud(): Crud;

    public function __construct(protected EntityManagerInterface $em, protected TranslatorInterface $translator)
    {
    }

    protected function getCrud(): Crud
    {
        if (!$this->crud instanceof Crud) {
            $this->crud = $this->generateCrud();
        }

        return $this->crud;
    }

    protected function createCRUD(object $entity, Request $request, string $successMsg, ?callable $prePersist = null): Response
    {
        $formType = $this->getCrud()->getFormClass();
        $form = $this->createForm($formType, $entity, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl($this->getCrud()->getRoute(Crud::CREATE)),
        ]);

        $this->addFormActions($form, $this->generateUrl($this->getCrud()->getRoute(Crud::INDEX)));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($prePersist) {
                $prePersist($entity, $form);
            }

            $this->em->persist($entity);
            $this->em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $successMsg]);
            }

            $this->addFlash('success', $successMsg);

            return $this->redirectToRoute($this->getCrud()->getRoute(Crud::INDEX));
        }

        return $this->render($this->generateViewPath(self::VIEW_PATH_CREATE), ['form' => $form, 'entity' => $entity]);
    }

    protected function indexCRUD(array $values = ['name'], array $actions = ['create', 'update', 'delete'], array $renderParams = []): Response
    {
        if ($this->getCrud()->hasBreadcrumbs()) {
            $this->generateBreadcrumbs();
        }

        if ($this->getCrud()->getSortable()) {
            $actions[] = 'sort';
        }

        return $this->render($this->generateViewPath('index'), array_merge([
            'routePrefix' => $this->getCrud()->getRoutePrefix(),
            'entityName' => $this->getCrud()->getEntityName(),
            'values' => $values,
            'actions' => $actions,
        ], $renderParams));
    }

    #[Route('/data', methods: ['GET'], options: ['expose' => true])]
    public function data(Request $request): JsonResponse
    {
        return $this->dataCRUD($request, $this->getCrud()->getRepository());
    }

    protected function dataCRUD(Request $request, EntityRepository $repository, array $moreCriteria = []): JsonResponse
    {
        $criteria = $this->getCriteria($request);

        foreach ($moreCriteria as $c) {
            $criteria[$c] = $request->get($c);
        }

        $data = $repository->findData($criteria);

        $groups = ['Default'];
        if ($this->isGranted('ROLE_ADMIN')) {
            $groups[] = 'Admin';
        }

        return $this->paginatorToJsonResponse($data, $groups);
    }

    protected function updateCRUD(object $entity, Request $request, string $successMsg, ?string $action = null, array $renderParams = [], ?callable $preUpdate = null, string $method = Request::METHOD_PUT): Response
    {
        $formType = $this->getCrud()->getFormClass();
        $form = $this->createForm($formType, $entity, [
            'method' => $method,
            'action' => $action ?? $this->generateUrl($this->getCrud()->getRoute(Crud::UPDATE), ['id' => $entity->getId()]),
        ]);

        $this->addFormActions($form, $this->generateUrl($this->getCrud()->getRoute(Crud::INDEX)));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($preUpdate) {
                $preUpdate($entity, $form);
            }
            $this->em->persist($entity);
            $this->em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $successMsg]);
            }

            $this->addFlash('success', $successMsg);

            return $this->redirectToRoute($this->getCrud()->getRoute(Crud::INDEX));
        }

        return $this->render($this->generateViewPath(self::VIEW_PATH_UPDATE), array_merge(['form' => $form, 'entity' => $entity], $renderParams));
    }

    protected function deleteCRUD(object $entity, Request $request, string $successMsg, ?callable $preRemove = null): Response
    {
        $form = $this->createFormBuilder($entity)
            ->setAction($this->generateUrl($this->getCrud()->getRoute(Crud::DELETE), ['id' => $entity->getId()]))
            ->setMethod(Request::METHOD_DELETE)
            ->getForm()
        ;

        $this->addFormActions($form, $this->generateUrl($this->getCrud()->getRoute(Crud::INDEX)), 'button.delete');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($preRemove) {
                $preRemove($entity);
            }

            $this->em->remove($entity);
            $this->em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $successMsg]);
            }

            $this->addFlash('success', $successMsg);

            return $this->redirectToRoute($this->getCrud()->getRoute(Crud::INDEX));
        }

        return $this->render($this->generateViewPath(self::VIEW_PATH_DELETE), ['form' => $form, 'entity' => $entity]);
    }

    public function sortCRUD(Request $request): Response
    {
        $entities = $this->getCrud()->getRepository()->findAll();
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($this->getCrud()->getRoute(Crud::SORT)))
            ->setMethod(Request::METHOD_PUT)
            ->getForm()
        ;

        $this->addFormActions($form, $this->generateUrl($this->getCrud()->getRoute(Crud::INDEX)), 'button.sort');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $positions = $request->request->all('position');
            foreach ($positions as $id => $position) {
                foreach ($entities as $entity) {
                    if ($entity->getId() === $id && \method_exists($entity, 'setPosition')) {
                        $entity->setPosition($position);

                        $this->em->persist($entity);
                        break;
                    }
                }
            }

            $this->em->flush();

            return new JsonResponse(['status' => 'success', 'message' => $this->translator->trans($this->getCrud()->getTranslationPrefix().'.sort.success')]);
        }

        return $this->render($this->generateViewPath(self::VIEW_PATH_SORT), ['form' => $form, 'entities' => $entities]);
    }

    protected function render(string $view, array $parameters = [], ?Response $response = null): Response
    {
        $parameters['entityName'] = $this->getCrud()->getEntityName();
        $parameters['transPrefix'] = $this->getCrud()->getTranslationPrefix();

        return parent::render($view, $parameters, $response);
    }

    protected function generateViewPath(string $action): string
    {
        return $this->getCrud()->getViewPath().'\\'.$action.'.html.twig';
    }
}
