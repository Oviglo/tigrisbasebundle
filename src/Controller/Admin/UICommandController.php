<?php

namespace Tigris\BaseBundle\Controller\Admin;

use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use SensioLabs\AnsiConverter\Theme\SolarizedXTermTheme;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/ui-command')]
#[IsGranted('ROLE_SUPER_ADMIN')]
class UICommandController extends BaseController
{
    public function generateBreadcrumbs(array $items = []): void
    {
        $items = array_merge([
            'menu.ui_command' => ['route' => 'tigris_base_admin_uicommand_index'],
        ], $items);

        parent::generateBreadcrumbs($items);
    }

    #[Route('/')]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisBase/admin/ui_command/index.html.twig');
    }

    #[Route('/data', methods: ['GET'], options: ['expose' => 'admin'])]
    public function data(Request $request, KernelInterface $kernel): Response
    {
        $application = new Application($kernel);
        $search = $request->get('search', null);
        $commands = $application->all(empty($search) ? null : $search);
        /*if (!empty($search)) {
            $commands = $application->find($search);
        }*/

        // Format to array
        $commands = \array_map(fn(Command $command): array => [
            'name' => $command->getName(),
            'description' => $command->getDescription(),
            'hidden' => $command->isHidden(),
        ], $commands);

        return new JsonResponse(['entities' => $commands, 'count' => count($commands)]);
    }

    #[Route('/command')]
    public function command(Request $request, KernelInterface $kernel): Response
    {
        $this->generateBreadcrumbs();

        $commandName = $request->get('c', '');
        $application = new Application($kernel);
        $command = $application->get($commandName);

        return $this->render('@TigrisBase/admin/ui_command/command.html.twig', [
            'command' => $command,
        ]);
    }

    #[Route('/run', options: ['expose' => 'admin'])]
    public function run(Request $request, KernelInterface $kernel): JsonResponse
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $name = $request->get('name', '');
        $arguments = $request->get('args', '');
        $input = new StringInput($name.' '.$arguments);

        $output = new BufferedOutput(decorated: true);
        $application->run($input, $output);

        $converter = new AnsiToHtmlConverter(new SolarizedXTermTheme());

        return new JsonResponse([
            'output' => nl2br((string) $converter->convert($output->fetch())),
        ]);
    }
}
