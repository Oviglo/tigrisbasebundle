<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Crud\Crud;
use Tigris\BaseBundle\Entity\Wording;
use Tigris\BaseBundle\Form\Type\WordingType;
use Tigris\BaseBundle\Repository\WordingRepository;
use Tigris\BaseBundle\Service\WordingService;

#[IsGranted('ROLE_ADMIN')]
#[Route('/admin/wording')]
class WordingController extends AbstractCrudController
{
    public function __construct(
        private readonly WordingRepository $wordingRepository,
        protected EntityManagerInterface $em,
        protected TranslatorInterface $translator,
        private readonly WordingService $wordingService,
    ) {
    }

    protected function generateCrud(): Crud
    {
        return new Crud([
            Crud::ENTITY_CLASS => Wording::class,
            Crud::VIEW_PATH => '@TigrisBase/admin/wording',
            Crud::BREADCRUMBS => true,
            Crud::FORM_CLASS => WordingType::class,
            Crud::ROUTE_PREFIX => 'tigris_base_admin_wording',
            Crud::REPOSITORY => $this->wordingRepository,
            Crud::SORTABLE => false,
        ]);
    }

    public function generateBreadcrumbs(array $items = []): void
    {
        parent::generateBreadcrumbs([
            'wording.menu' => ['route' => 'tigris_base_admin_wording_index'],
        ]);
    }

    #[Route('/', methods: ['GET'])]
    public function index(): Response
    {
        return $this->indexCRUD(['name'], ['update'], [
            'firstPage' => $this->wordingService->getFirstPage(),
            'pages' => $this->wordingService->getPages(),
        ]);
    }

    #[Route('/data', methods: ['GET'], options: ['expose' => true])]
    public function data(Request $request): JsonResponse
    {
        $pageName = $request->get('page', $this->wordingService->getFirstPage());

        $data = $this->wordingService->getByPage($pageName);

        $jsonData = [
            'totalCount' => count($data),
            'entities' => $this->generateJsonEntities($data),
        ];

        return new JsonResponse($jsonData);
    }

    #[Route('/{page}/{name}', methods: ['GET', 'POST', 'PUT'], options: ['expose' => true])]
    public function edit(Request $request, string $page, string $name): Response
    {
        $wording = $this->wordingService->get($page, $name);
        $entity = $wording['entity'];

        return $this->updateCRUD($entity, $request, '', $this->generateUrl($this->getCrud()->getRoute(Crud::UPDATE), ['page' => $page, 'name' => $name]), ['wording' => $wording]);
    }
}
