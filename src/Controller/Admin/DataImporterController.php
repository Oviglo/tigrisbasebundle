<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\DataImporter\DataImporters;
use Tigris\BaseBundle\Entity\DataImport;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\BaseBundle\Form\Type\DataImportMappingType;
use Tigris\BaseBundle\Form\Type\DataImportType;
use Tigris\BaseBundle\Manager\DataImportManager;
use Tigris\BaseBundle\Traits\FormTrait;

#[Route('/admin/data/importer')]
#[IsGranted('ROLE_ADMIN')]
class DataImporterController extends BaseController
{
    use FormTrait;

    #[Route('/')]
    public function index(): Response
    {
        $this->generateBreadcrumbs([
            'menu.data_importer' => [
                'route' => 'tigris_base_admin_dataimporter_index',
            ],
        ]);

        return $this->render('@TigrisBase/admin/data/importer/index.html.twig');
    }

    #[Route('/data', options: ['expose' => 'admin'])]
    public function data(Request $request, DataImportManager $dataImportManager): JsonResponse
    {
        $criteria = $this->getCriteria($request);
        $criteria['status'] = $request->query->get('status', null);

        $data = $dataImportManager->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/choose', options: ['expose' => 'admin'])]
    public function choose(DataImporters $dataImporters): Response
    {
        $types = array_keys($dataImporters->getImporters());

        return $this->render('@TigrisBase/admin/data/importer/choose.html.twig', ['types' => $types]);
    }

    #[Route('import/{type}', requirements: ['type' => '[a-z_]+'], methods: ['GET', 'POST'])]
    public function import(
        Request $request,
        string $type,
        DataImportManager $dataImportManager,
        TranslatorInterface $translator,
        EventDispatcherInterface $eventDispatcher,
        FormFactoryInterface $formFactory
    ): Response {
        $this->generateBreadcrumbs([
            'menu.data_importer' => [
                'route' => 'tigris_base_admin_dataimporter_index',
            ],
        ]);

        $entity = (new DataImport())
            ->setType($type);
        $form = $this->createForm(DataImportType::class, $entity, ['method' => 'POST', 'action' => $this->generateUrl('tigris_base_admin_dataimporter_import', ['type' => $type])]);

        // Form event
        $optionsForm = $formFactory->createNamedBuilder('options', FormType::class, $entity->getOptions(), ['auto_initialize' => false, 'label' => false]);
        $form->add($optionsForm->getForm());
        $event = new FormEvent($form);

        $eventDispatcher->dispatch($event, Events::LOAD_DATA_IMPORTER_FORM);

        $this->addFormActions($form, $this->generateUrl('tigris_base_admin_dataimporter_index'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataImportManager->save($entity);

            $this->addFlash('success', $translator->trans('data.importer.add.success'));

            return $this->redirectToRoute('tigris_base_admin_dataimporter_mapping', ['id' => $entity->getId()]);
        }

        return $this->render('@TigrisBase/admin/data/importer/import.html.twig', ['type' => $type, 'form' => $form]);
    }

    #[Route('/{id<\d+>}/mapping', options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function mapping(Request $request, DataImport $entity, DataImportManager $dataImportManager, TranslatorInterface $translator): Response
    {
        $this->generateBreadcrumbs([
            'menu.data_importer' => [
                'route' => 'tigris_base_admin_dataimporter_index',
            ],
        ]);

        $form = $this->createForm(DataImportMappingType::class, $entity, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_base_admin_dataimporter_mapping', ['id' => $entity->getId()]),
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_base_admin_dataimporter_index'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataImportManager->save($entity);

            $this->addFlash('success', $translator->trans('data.importer.mapping.success'));

            return $this->redirectToRoute('tigris_base_admin_dataimporter_index');
        }

        return $this->render('@TigrisBase/admin/data/importer/mapping.html.twig', ['entity' => $entity, 'form' => $form]);
    }


    #[Route('/{id<\d+>}/remove', options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(Request $request, DataImport $entity, EntityManagerInterface $entityManager): Response
    {
        $form = $this->getConfirmationForm($this->generateUrl('tigris_base_admin_dataimporter_remove', ['id' => $entity->getId()]));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($entity);
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => '']);
            }

            return $this->redirectToRoute('tigris_base_dataimporter_index');
        }

        return $this->render('@TigrisBase/admin/data/importer/remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }
}
