<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\DataExport;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Form\Type\DataExportType;
use Tigris\BaseBundle\Mailing\ExportFileEmail;
use Tigris\BaseBundle\Manager\DataExportManager;
use Tigris\BaseBundle\Traits\FormTrait;

#[Route('/admin/data/exporter')]
#[IsGranted('ROLE_ADMIN')]
class DataExporterController extends BaseController
{
    use FormTrait;

    #[Route('/', options: ['expose' => 'admin'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs([
            'menu.data_exporter' => [
                'route' => 'tigris_base_admin_dataexporter_index',
            ],
        ]);

        return $this->render('@TigrisBase/admin/data/exporter/index.html.twig');
    }

    #[Route('/data', options: ['expose' => 'admin'])]
    public function data(Request $request, DataExportManager $dataExportManager): JsonResponse
    {
        $criteria = $this->getCriteria($request);
        $criteria['status'] = $request->query->get('status', null);

        $data = $dataExportManager->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(Request $request, DataExportManager $dataExporterManager, ExportFileEmail $exportFileEmail, TranslatorInterface $translator): Response
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw $this->createAccessDeniedException();
        }

        $entity = (new DataExport())
            ->setCriteria($request->query->all())
            ->setSendToMail($user->getEmail())
        ;
        $form = $this->createForm(DataExportType::class, $entity, [
            'action' => $this->generateUrl('tigris_base_admin_dataexporter_new'),
            'method' => 'POST',
            'attr' => ['class' => ''],
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_base_admin_dataexporter_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!file_exists($entity->getAbsoluteFolder())) {
                mkdir($entity->getAbsoluteFolder(), 0777, true);
            }

            $dataExporterManager->save($entity);

            if ($entity->getStatus() === DataExport::STATUS_ERROR) {
                return new JsonResponse(['status' => 'error', 'message' => $translator->trans('data.exporter.export_error', ['%error%' => $entity->getErrorMessage()])]);
            }

            if ($entity->getSendToMail()) {
                $exportFileEmail($entity);

                if ($request->isXmlHttpRequest()) {
                    return new JsonResponse(['status' => 'success', 'message' => $translator->trans('data.exporter.email_sent')]);
                }
            }

            $response = new BinaryFileResponse($entity->getAbsolutePath());
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $entity->getFilePath()
            );

            return $response;
        }

        return $this->render('@TigrisBase/admin/data/exporter/new.html.twig', ['form' => $form]);
    }

    #[Route('/{id<\d+>}/download', options: ['expose' => 'admin'], methods: ['GET'])]
    public function download(DataExport $entity): BinaryFileResponse
    {
        return $this->file($entity->getAbsolutePath(), $entity->getFilePath());
    }

    #[Route('/{id<\d+>}/remove', options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(Request $request, DataExport $entity, EntityManagerInterface $entityManager): Response
    {
        $form = $this->getConfirmationForm($this->generateUrl('tigris_base_admin_dataexporter_remove', ['id' => $entity->getId()]));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($entity);
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => '']);
            }

            return $this->redirectToRoute('tigris_base_dataexporter_index');
        }

        return $this->render('@TigrisBase/admin/data/exporter/remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }
}
