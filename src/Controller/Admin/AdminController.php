<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Dashboard\DashboardManager;

#[Route('/admin')]
#[IsGranted('ROLE_ADMIN')]
class AdminController extends BaseController
{
    #[Route('/')]
    public function dashboard(DashboardManager $dashboardManager): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisBase/admin/dashboard.html.twig', [
            'cards' => $dashboardManager->getItems(),
        ]);
    }

    #[Route('/save-menu-style', options: ['expose' => 'admin'])]
    public function saveMenuStyle(Request $request): JsonResponse
    {
        $session = $request->getSession();
        $extendedMenu = $request->get('menuExtended', 1);
        $session->set('menu-extended', $extendedMenu);

        return new JsonResponse(['status' => 'success', 'message' => '']);
    }
}
