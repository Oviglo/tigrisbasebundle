<?php

namespace Tigris\BaseBundle\Controller\Admin;

use App\Entity\Group;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Crud\Crud;
use Tigris\BaseBundle\Form\Type\GroupType;
use Tigris\BaseBundle\Repository\GroupRepository;
use Tigris\BaseBundle\Traits\FormTrait;

#[Route('/admin/group')]
#[IsGranted('ROLE_SUPER_ADMIN')]
class GroupController extends AbstractCrudController
{
    use FormTrait;

    protected function generateCrud(): Crud
    {
        return new Crud([
            Crud::ENTITY_CLASS => Group::class,
            Crud::REPOSITORY => $this->groupRepository,
            Crud::ROUTE_PREFIX => 'tigris_base_admin_group',
            Crud::FORM_CLASS => GroupType::class,
            Crud::BREADCRUMBS => true,
            Crud::TRANSLATION_PREFIX => 'user.group',
        ]);
    }

    public function __construct(private readonly GroupRepository $groupRepository, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        parent::__construct($em, $translator);
    }

    public function generateBreadcrumbs(array $items = []): void
    {
        $items = array_merge(['menu.groups' => ['route' => 'tigris_base_admin_group_index']], $items);
        parent::generateBreadcrumbs($items);
    }

    #[Route('/')]
    public function index(): Response
    {
        return $this->indexCRUD();
    }

    #[Route('/new', methods: ['GET', 'POST'], options: ['expose' => 'admin'])]
    public function new(Request $request, TranslatorInterface $translator): Response
    {
        return $this->createCRUD(new Group(), $request, $translator->trans('user.group.add.success'));
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function edit(Request $request, Group $entity, TranslatorInterface $translator): Response
    {
        return $this->updateCRUD($entity, $request, $translator->trans('user.group.edit.success'));
    }

    #[Route('{id}/remove', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET', 'DELETE'])]
    public function delete(Request $request, Group $entity, TranslatorInterface $translator): Response
    {
        return $this->deleteCRUD($entity, $request, $translator->trans('user.group.delete.success'));
    }
}
