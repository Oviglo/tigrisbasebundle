<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Repository\FileFolderRepository;

#[Route('/admin/basefile')]
#[IsGranted('ROLE_ADMIN')]
class FileController extends BaseController
{
    #[Route('/')]
    public function index(FileFolderRepository $fileFolderRepository): Response
    {
        $this->generateBreadcrumbs([
            'file.list' => ['route' => 'tigris_base_admin_file_index'],
        ]);

        $rootFolders = $fileFolderRepository->getRootNodes();

        return $this->render('@TigrisBase/admin/file/index.html.twig', ['rootFolders' => $rootFolders]);
    }
}
