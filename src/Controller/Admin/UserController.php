<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\ChartControllerTrait;
use Tigris\BaseBundle\Dashboard\Item\DisabledUserCounterItem;
use Tigris\BaseBundle\Dashboard\Item\UserCounterItem;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Event\Event;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\UserEvent;
use Tigris\BaseBundle\Event\UserUpdateEvent;
use Tigris\BaseBundle\Form\Type\UserType;
use Tigris\BaseBundle\Manager\UserManager;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\BaseBundle\Utils\Utils;

#[Route('/admin/user')]
#[IsGranted('ROLE_ADMIN')]
class UserController extends BaseController
{
    use FormTrait;
    use ChartControllerTrait;

    #[Route('/')]
    public function index(UserCounterItem $userCounterItem, DisabledUserCounterItem $disabledUserCounterItem): Response
    {
        $this->generateBreadcrumbs([
            'menu.users' => [
                'route' => 'tigris_base_admin_user_index',
            ],
        ]);

        return $this->render("@TigrisBase\admin\user\index.html.twig", [
            'userCounterItem' => $userCounterItem,
            'disabledUserCounterItem' => $disabledUserCounterItem,
        ]);
    }

    #[Route('/data', options: ['expose' => 'admin'])]
    public function data(Request $request, UserRepository $userRepository): JsonResponse
    {
        $criteria = $this->getCriteria($request);

        $criteria['search'] = mb_strtolower($request->query->get('search', ''));
        $criteria['type'] = $request->query->get('type', 'all');
        $data = $userRepository->findData($criteria);

        $groups = ['Default'];

        if ($this->isGranted('ROLE_ADMIN')) {
            $groups[] = 'Admin';
        }

        return $this->paginatorToJsonResponse($data, $groups);
    }

    #[Route('/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(Request $request, UserManager $userManager, EventDispatcherInterface $eventDispatcher): Response
    {
        $this->generateBreadcrumbs([
            'menu.users' => ['route' => 'tigris_base_admin_user_index'],
            'user.add.add' => ['route' => 'tigris_base_admin_user_new'],
        ]);

        $entity = $userManager->createUser();

        $form = $this->createForm(UserType::class, $entity, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl('tigris_base_admin_user_new'),
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_base_admin_user_index'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->updateUser($entity);
            $event = new UserEvent($entity);
            $eventDispatcher->dispatch($event, Events::AFTER_CREATE_USER);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => '']);
            }

            return $this->redirectToRoute('tigris_base_admin_user_index');
        }

        return $this->render('@TigrisBase\admin\user\new.html.twig', ['form' => $form, 'entity' => $entity]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function edit(Request $request, int $id, UserManager $userManager, EventDispatcherInterface $eventDispatcher, TranslatorInterface $translator): Response
    {
        $entity = $userManager->findUserBy(['id' => $id]);
        $oldEntity = clone $entity;

        $this->generateBreadcrumbs([
            'menu.users' => ['route' => 'tigris_base_admin_user_index'],
            'user.edit.edit' => ['route' => 'tigris_base_admin_user_edit', 'params' => ['id' => $entity->getId()]],
        ]);

        $form = $this->createForm(UserType::class, $entity, [
            'method' => Request::METHOD_PUT,
            'action' => $this->generateUrl('tigris_base_admin_user_edit', ['id' => $entity->getId()]),
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_base_admin_user_index'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->updateUser($entity);
            $event = new UserUpdateEvent($oldEntity, $entity);
            $eventDispatcher->dispatch($event, Events::AFTER_UPDATE_USER);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('user.add.success')]);
            }

            return $this->redirectToRoute('tigris_base_admin_user_index');
        }

        return $this->render('@TigrisBase\admin\user\edit.html.twig', ['form' => $form, 'entity' => $entity]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(Request $request, int $id, UserRepository $userRepository, EventDispatcherInterface $eventDispatcher, EntityManagerInterface $em): Response
    {
        $entity = $userRepository->findOneById($id);

        $this->generateBreadcrumbs([
            'menu.users' => ['route' => 'tigris_base_admin_user_index'],
            'user.delete.delete' => ['route' => 'tigris_base_admin_user_remove', 'params' => ['id' => $entity->getId()]],
        ]);

        $form = $this->getDeleteForm($entity, 'tigris_base_admin_user_');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();
            $event = new Event([
                'user' => $entity,
            ]);
            $eventDispatcher->dispatch($event, Events::AFTER_REMOVE_USER);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => '']);
            }

            return $this->redirectToRoute('tigris_base_admin_user_index');
        }

        return $this->render('@TigrisBase\admin\user\remove.html.twig', ['form' => $form, 'entity' => $entity]);
    }

    #[Route('/{id}/show', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    public function show(int $id, EventDispatcherInterface $eventDispatcher, UserRepository $userRepository): Response
    {
        $entity = $userRepository->findOneById($id);
        $this->generateBreadcrumbs([
            'menu.users' => ['route' => 'tigris_base_admin_user_index'],
            $entity->getUsername() => ['route' => 'tigris_base_admin_user_show', 'params' => ['id' => $entity->getId()]],
        ]);

        $event = new GenericEvent($entity, []);
        $eventDispatcher->dispatch($event, Events::USER_SHOW);
        $data = $event->getArguments();

        return $this->render('@TigrisBase\admin\user\show.html.twig', ['entity' => $entity, 'moreData' => $data]);
    }

    #[Route('/chart', options: ['expose' => 'admin'])]
    public function chart(Request $request, TranslatorInterface $translator, UserRepository $userRepository): JsonResponse
    {
        $criteria = $this->getDateCriteria($request);

        $board = $this->getStatsBoard(
            $criteria['startDate'],
            $criteria['endDate'],
            false
        );
        $labels = $board['labels'];
        $stats = $board['stats'];
        $dateStart = $board['dates']['start'];
        $dateEnd = $board['dates']['end'];

        $entities = $userRepository->findChartByDates($dateStart, $dateEnd);

        foreach ($entities as $entity) {
            $stats[(int) $entity['year'].'-'.(int) $entity['month']] = (int) $entity['nb'];
        }

        return new JsonResponse([
            'labels' => $labels,
            'stats' => [
                [
                'label' => $translator->trans('user.users'),
                'data' => array_values($stats),
                ],
            ],
        ]);
    }

    #[Route('/{id<\d+>}/change-part', options: ['expose' => 'admin'], methods: ['PUT'])]
    public function changePart(User $entity, Request $request, EntityManagerInterface $em, TranslatorInterface $translator, EventDispatcherInterface $eventDispatcher): Response
    {
        $this->denyAccessUnlessGranted('edit', $entity);

        $oldEntity = clone $entity;

        $data = $request->request->get('data', null);
        $value = $request->request->get('value', null);

        if (null !== $data && null !== $value) {
            $method = 'set'.ucfirst($data);

            if (method_exists($entity, $method)) {
                $entity->$method(Utils::formatValue($value));

                $em->persist($entity);
                $em->flush();

                $event = new UserUpdateEvent($oldEntity, $entity);
                $eventDispatcher->dispatch($event, Events::AFTER_UPDATE_USER);

                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('user.change_part.success')]);
            }
        }

        return new JsonResponse(['status' => 'error', 'message' => '']);
    }

    #[Route('/remove-group', options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function removeGroup(Request $request, UserRepository $userRepository, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        $ids = $request->get('ids');
        $users = $userRepository->findByIds($ids);

        $form = $this->createFormBuilder(null, [
            'action' => $this->generateUrl('tigris_base_admin_user_removegroup', ['ids' => $ids]),
            'method' => Request::METHOD_DELETE,
        ])->getForm();

        $this->addFormActions($form, $this->generateUrl('tigris_base_admin_user_index'), 'button.delete');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($users as $user) {
                $em->remove($user);
            }

            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('user.delete_group.success')]);
            }

            $this->addFlash('success', 'user.delete_group.success');

            return $this->redirectToRoute('tigris_base_admin_user_index');
        }

        return $this->render('@TigrisBase/admin/user/remove_group.html.twig', [
            'users' => $users,
            'form' => $form,
        ]);
    }

    #[Route('/remove-unverified', options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function removeUnverified(Request $request, UserRepository $userRepository, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        $formBuilder = $this->createFormBuilder(null, [
            'action' => $this->generateUrl('tigris_base_admin_user_removeunverified'),
            'method' => Request::METHOD_DELETE,
        ]);

        // Add from input in number of week
        $formBuilder->add('weeksAgo', NumberType::class, [
            'label' => 'user.remove_unverified.nb_weeks_ago',
            'required' => true,
            'attr' => [
                'min' => 0,
                'max' => 52,
            ],
            'data' => 4,
        ]);

        $form = $formBuilder->getForm();

        $this->addFormActions($form, $this->generateUrl('tigris_base_admin_user_index'), 'button.delete');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $weeksAgo = $form->get('weeksAgo')->getData();
            $users = $userRepository->findUnverified(new \DateTime($weeksAgo . ' weeks ago'));
            foreach ($users as $user) {
                $em->remove($user);
            }

            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('user.remove_unverified.success')]);
            }
        }

        return $this->render('@TigrisBase/admin/user/remove_unverified.html.twig', [
            'form' => $form,
        ]);
    }
}
