<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Crud\Crud;
use Tigris\BaseBundle\Entity\SitemapUrl;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\SitemapEvent;
use Tigris\BaseBundle\Form\Type\SitemapUrlType;
use Tigris\BaseBundle\Repository\SitemapUrlRepository;

#[Route('/admin/seo/sitemap')]
#[IsGranted('ROLE_ADMIN')]
class SitemapController extends AbstractCrudController
{
    protected function generateCrud(): Crud
    {
        return new Crud([
            Crud::ENTITY_CLASS => SitemapUrl::class,
            Crud::ROUTE_PREFIX => 'tigris_base_admin_sitemap',
            Crud::FORM_CLASS => SitemapUrlType::class,
            Crud::REPOSITORY => $this->sitemapUrlRepository,
        ]);
    }

    public function __construct(private readonly SitemapUrlRepository $sitemapUrlRepository, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        parent::__construct($em, $translator);
    }

    public function generateBreadcrumbs(array $items = []): void
    {
        $items = array_merge([
            'menu.seo' => null,
            'menu.sitemap' => ['route' => 'tigris_base_admin_sitemap_index'],
        ], $items);
        parent::generateBreadcrumbs($items);
    }

    #[Route('/')]
    public function index(EventDispatcherInterface $eventDispatcher): Response
    {
        $this->generateBreadcrumbs();

        $event = new SitemapEvent();
        $eventDispatcher->dispatch($event, Events::GENERATE_SITEMAP);

        return $this->render('@TigrisBase/admin/sitemap/index.html.twig', [
            'urls' => $event->getUrls(),
        ]);
    }

    #[Route('/new', methods: ['GET', 'POST'], options: ['expose' => true])]
    public function new(Request $request): Response
    {
        return $this->createCRUD(new SitemapUrl(), $request, '');
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function edit(Request $request, SitemapUrl $entity): Response
    {
        return $this->updateCRUD($entity, $request, '');
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], methods: ['GET', 'DELETE'], options: ['expose' => 'admin'])]
    public function delete(Request $request, SitemapUrl $entity): Response
    {
        return $this->deleteCRUD($entity, $request, '');
    }
}
