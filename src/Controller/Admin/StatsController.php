<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\StatsEvent;

#[Route('/admin/stats')]
#[IsGranted('ROLE_ADMIN')]
class StatsController extends BaseController
{
    #[Route('/index')]
    public function index(EventDispatcherInterface $eventDispatcher): Response
    {
        $this->generateBreadcrumbs([
            'menu.stats' => [
                'route' => 'tigris_base_admin_stats_index',
            ],
        ]);

        $event = new StatsEvent();

        $eventDispatcher->dispatch($event, Events::LOAD_STATS);

        return $this->render("@TigrisBase\admin\stats\index.html.twig", ['stats' => $event->getStats()]);
    }
}
