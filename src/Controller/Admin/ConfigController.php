<?php

namespace Tigris\BaseBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\Config;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\BaseBundle\Form\Type\FormActionsType;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Repository\ConfigRepository;

#[Route(path: '/admin/config')]
#[IsGranted('ROLE_ADMIN')]
class ConfigController extends BaseController
{
    #[Route(path: '/index', methods: ['GET', 'POST'])]
    public function index(
        Request $request,
        EventDispatcherInterface $eventDispatcher,
        TranslatorInterface $translator,
        ConfigRepository $configRepository,
        ConfigManager $configManager,
        EntityManagerInterface $entityManager
    ): Response {
        $this->generateBreadcrumbs([
            'menu.configs' => ['route' => 'tigris_base_admin_config_index'],
        ]);

        $form = $this
            ->createFormBuilder(null, [
                'attr' => [
                    'novalidate' => 'novalidate',
                ],
            ])
            ->setAction($this->generateUrl('tigris_base_admin_config_index'))
            ->setMethod(Request::METHOD_POST)
            ->getForm()
        ;

        $form->add('actions', FormActionsType::class, [
            'label' => false,
            'buttons' => [
                'save' => [
                    'type' => SubmitType::class,
                    'options' => [
                        'label' => 'button.save',
                    ],
                ],
            ],
        ]);

        $formEvent = new FormEvent($form);
        $eventDispatcher->dispatch($formEvent, Events::LOAD_CONFIGS);

        $form = $formEvent->getForm();

        // Load model data
        $entities = $configRepository->findAll();
        $data = [];
        foreach ($entities as $entity) {
            $configManager->formatFormData($entity, $data);
        }

        // var_dump($data);

        $form->setData($data);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*foreach ($configBundles as $bundle => $configs) {
                foreach ($configs as $name => $value) {
                    $entity = $this->searchConfig($entities, $bundle, $name);

                    if (!$entity instanceof Config) { // New config
                        $entity = (new Config())
                            ->setBundle($bundle)
                            ->setName($name)
                        ;

                        $entityManager->persist($entity);
                    }

                    if ($form->has($bundle) && $form->get($bundle)->has($name)) {
                        $dataType = $form->get($bundle)->get($name)->getConfig()->getType()->getInnerType()->getBlockPrefix();
                        $entity
                            ->setValue($value)
                            ->setType($dataType)
                        ;

                        // $entityManager->merge($entity);
                    }
                }
            }*/
            $configManager->saveFromFormData($form);
            $entityManager->flush();

            $this->addFlash(
                'success',
                $translator->trans('config.edit.success')
            );

            return $this->redirectToRoute('tigris_base_admin_config_index');
        }

        return $this->render("@TigrisBase\admin\config\index.html.twig", ['form' => $form]);
    }
}
