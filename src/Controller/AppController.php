<?php

namespace Tigris\BaseBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Event\CronEvent;
use Tigris\BaseBundle\Event\Event;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\SourceEvent;
use Tigris\BaseBundle\Security\SecurityButton;

#[Route('/app')]
class AppController extends BaseController
{
    #[Route('/cron')]
    public function cron(EventDispatcherInterface $eventDispatcher): JsonResponse
    {
        $event = new CronEvent();
        $eventDispatcher->dispatch($event, Events::CRON);

        return new JsonResponse([]);
    }

    #[Route('/ajax/cron', options: ['expose' => true])]
    public function ajaxCron(Request $request, EventDispatcherInterface $eventDispatcher, SessionInterface $session, EntityManagerInterface $em): JsonResponse
    {
        $user = $this->getUser();
        $lastCronDate = clone $session->get('lastCronDate', new \DateTime());
        $timestamp = (int) substr($request->query->get('t', 0), 0, 10);

        if (0 !== $timestamp) {
            $date = new \DateTime();
            $date->setTimestamp($timestamp);
            $session->set('lastCronDate', $date);
        } else {
            $date = new \DateTime();
            $session->set('lastCronDate', new \DateTime());
        }

        $event = new Event(['date' => $lastCronDate, 'user' => $user], ['lastCronDate' => $lastCronDate, 'nextCronDate' => $date]);

        if ($this->isGranted('ROLE_USER') && $user instanceof User) {
            $eventDispatcher->dispatch($event, Events::AJAX_CRON);

            // Refresh user activity date
            if ($user->getOnlineAt() instanceof \DateTime) {
                $onlineAt = clone $user->getOnlineAt();
                $onlineAt->add(new \DateInterval('PT1M'));

                if ($onlineAt < (new \DateTime())) {
                    $user->setOnlineAt(new \DateTime());
                    $em->persist($user);
                    $em->flush();
                }
            } else {
                $user->setOnlineAt(new \DateTime());
                $em->persist($user);
                $em->flush();
            }
        }

        return new JsonResponse($this->generateJsonEntities($event->getData()));
    }

    #[Route('/switch-theme', options: ['expose' => true], methods: ['GET'])]
    public function switchTheme(Request $request): JsonResponse
    {
        $cookies = $request->cookies;
        $response = new JsonResponse([]);
        $cookieTheme = Cookie::create('theme', $cookies->get('theme', 'light'));
        $askedTheme = $request->get('theme', null);
        $oldValue = $cookieTheme->getValue();

        $newValue = $askedTheme ?? ('dark' == $oldValue ? 'light' : 'dark');
        if (empty($newValue)) {
            $newValue = 'light';
        }

        $cookieTheme = $cookieTheme->withValue($newValue);

        $response->headers->setCookie($cookieTheme);
        $response->setData(['status' => 'success', 'theme' => $newValue]);

        return $response;
    }

    #[Route('/security-token', options: ['expose' => true], methods: ['GET'])]
    public function securityToken(Request $request, SecurityButton $securityButton): JsonResponse
    {
        $token = $securityButton->generateToken($request->get('key', ''));

        return new JsonResponse(['token' => $token]);
    }

    #[Route('/event-stream', options: ['expose' => true], methods: ['GET'])]
    public function eventStream(Request $request, EventDispatcherInterface $eventDispatcher, SessionInterface $session, EntityManagerInterface $em): StreamedResponse
    {
        $user = $this->getUser();
        $lastCronDate = clone $session->get('lastCronDate', new \DateTime());
        $timestamp = (int) substr($request->query->get('t', 0), 0, 10);

        if (0 !== $timestamp) {
            $date = new \DateTime();
            $date->setTimestamp($timestamp);
            $session->set('lastCronDate', $date);
        } else {
            $date = new \DateTime();
            $session->set('lastCronDate', new \DateTime());
        }

        $event = new SourceEvent($lastCronDate, ['user' => $user]);

        $response = new StreamedResponse(function () use ($event, $eventDispatcher): void {
            //$eventDispatcher->dispatch($event, Events::AJAX_CRON);
            echo 'data: '.json_encode($this->generateJsonEntities($event->getData()))."\n\n";
            
            @ob_end_flush();
            flush();

            if (connection_aborted()) {
                return;
            }
            sleep(5);
            
        });
        $response->headers->set('Cache-Control', 'no-cache');
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Connection', 'keep-alive');

        return $response;
    }
}
