<?php

namespace Tigris\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\SitemapEvent;
use Tigris\BaseBundle\Repository\SitemapUrlRepository;

class SitemapController extends BaseController
{
    #[Route('/sitemap.xml')]
    public function index(EventDispatcherInterface $eventDispatcher, SitemapUrlRepository $sitemapUrlRepository): Response
    {
        $event = new SitemapEvent();
        $eventDispatcher->dispatch($event, Events::GENERATE_SITEMAP);

        $entities = $sitemapUrlRepository->findAll();

        return $this->render('@TigrisBase/sitemap/index.xml.twig', [
            'urls' => $event->getUrls(),
            'entities' => $entities,
        ], new Response(headers: ['Content-Type' => 'text/xml']));
    }
}
