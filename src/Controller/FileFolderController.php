<?php

namespace Tigris\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Entity\FileFolder;
use Tigris\BaseBundle\Form\Type\FileFolderType;
use Tigris\BaseBundle\Repository\FileFolderRepository;
use Tigris\BaseBundle\Traits\FormTrait;

#[Route(path: '/basefile/folder')]
#[IsGranted('ROLE_USER')]
class FileFolderController extends BaseController
{
    use FormTrait;

    #[Route(path: '/list/{parent}', options: ['expose' => true], requirements: ['parent' => '\d+'], defaults: ['parent' => 0])]
    public function list(FileFolderRepository $fileFolderRepository, ?FileFolder $parent = null): JsonResponse
    {
        $user = $this->getUser();
        if ($this->isGranted('ROLE_ADMIN')) {
            $user = null;
        }
        
        // If user is an admin, show all folders else show user's folder only
        if (!$this->isGranted('ROLE_FILE_ADMIN') && !$this->isGranted('ROLE_ADMIN') && null == $parent) {
            $parent = $fileFolderRepository->findUserFolder($user);
        }

        if ($parent instanceof FileFolder) {
            $folders = $fileFolderRepository->getUserChildren($parent, $user);
        } else {
            $folders = $fileFolderRepository->getUserRootNodes($user);
        }

        $sessionName = 'file_data';
        $session = new Session();
        $selectedFolder = null;
        if ($session->has($sessionName)) {
            $savedCriteria = $session->get($sessionName);
            if (isset($savedCriteria['folder'])) {
                $selectedFolder = $fileFolderRepository->find($savedCriteria['folder'])?->getId();
            }
        }

        return new JsonResponse(['entities' => $this->generateJsonEntities($folders), 'selectedFolder' => $selectedFolder]);
    }

    #[Route(path: '/new/{parent}', options: ['expose' => true], requirements: ['parent' => '\d+'], defaults: ['parent' => 0], methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, ?FileFolder $parent = null): JsonResponse|RedirectResponse|Response
    {
        $entity = (new FileFolder())
            ->setUser($this->getUser())
        ;

        if (null != $parent) {
            $entity->setParent($parent);
        }

        $form = $this->createForm(FileFolderType::class, $entity, [
            'action' => $this->generateUrl('tigris_base_filefolder_new'),
            'method' => 'POST',
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_base_file_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => '']);
            }

            return $this->redirectToRoute('tigris_base_file_index');
        }

        return $this->render('@TigrisBase/file-folder/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}/edit', options: ['expose' => true], requirements: ['id' => '\d+'], methods: ['GET', 'PUT'])]
    public function edit(Request $request, FileFolder $entity, EntityManagerInterface $entityManager): JsonResponse|Response
    {
        $form = $this->createForm(FileFolderType::class, $entity, [
            'action' => $this->generateUrl('tigris_base_filefolder_edit', ['id' => $entity->getId()]),
            'method' => 'PUT',
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_base_file_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return new JsonResponse(['status' => 'success', 'message' => '']);
        }

        return $this->render('@TigrisBase/file-folder/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}/delete', options: ['expose' => true], requirements: ['id' => '\d+'], methods: ['GET', 'DELETE'])]
    public function delete(Request $request, FileFolder $entity, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('tigris_base_filefolder_delete', ['id' => $entity->getId()]))
            ->setMethod(Request::METHOD_DELETE)
            ->getForm()
        ;

        $this->addFormActions($form, false, 'button.delete');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($entity);
            $entityManager->flush();

            return new JsonResponse(['status' => 'success', 'message' => '']);
        }

        return $this->render('@TigrisBase/file-folder/delete.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }
}
