<?php

namespace Tigris\BaseBundle\Controller;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Form\Type\UserEmailType;
use Tigris\BaseBundle\Manager\UserManager;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Security\Voter\UserVoter;

#[Route('/user')]
class UserController extends BaseController
{
    #[Route('/search/{search}', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function search(string $search, UserRepository $userRepository): JsonResponse
    {
        $entities = $userRepository->search($search);

        return new JsonResponse($entities);
    }

    #[Route('/my-account', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function myAccount(Request $request, UserRepository $userRepository): Response
    {
        $user = $request->query->has('user') ? $userRepository->findOneById($request->query->get('user')) : $this->getUser();

        if (null === $user) {
            throw new UserNotFoundException();
        }

        $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

        return $this->render('@TigrisBase/user/my_account.html.twig', [
            'entity' => $user,
        ]);
    }

    #[Route('/my-account/email', methods: ['GET', 'PUT'])]
    public function updateEmail(Request $request, UserManager $userManager, TranslatorInterface $translator): Response
    {
        if ($request->query->has('user')) {
            $user = $userManager->findUserBy(['id' => $request->query->get('user')]);
        } else {
            $user = $this->getUser();
        }

        if (!$user instanceof User) {
            throw new UserNotFoundException();
        }

        $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

        $form = $this->createForm(UserEmailType::class, $user, [
            'method' => Request::METHOD_PUT,
            'action' => $this->generateUrl('tigris_base_user_updateemail'),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $success = $userManager->updateEmail($user);

            if ($success) {
                $this->addSuccessFlash($translator->trans('user_account.email_success'));

                return $this->redirectToRoute('tigris_base_user_updateemail');
            }

            $form->addError(new FormError($translator->trans('user_account.bad_password')));
        }

        return $this->render('@TigrisBase/user/update_email.html.twig', [
            'form' => $form,
            'entity' => $user,
        ]);
    }

    #[Route('/{identifier}', requirements: ['indentifier' => '[\w\s]'], methods: ['GET'], options: ['expose' => true])]
    public function profile(string $identifier, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneByUsername($identifier);

        if (!$user instanceof User) {
            throw new UserNotFoundException();
        }

        $this->denyAccessUnlessGranted(UserVoter::VIEW, $user);

        return $this->render('@TigrisBase/user/profile.html.twig', [
            'entity' => $user,
        ]);
    }
}
