<?php

namespace Tigris\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Tigris\BaseBundle\Utils\Utils;

trait ChartControllerTrait
{
    /**
     * Prepare stats chart with start and end dates.
     */
    protected function getStatsBoard(\DateTime $dateStart, \DateTime $dateEnd, bool $isDays = false, bool $multiple = false): array
    {
        $interval = $dateStart->diff($dateEnd);
        $monthCount = ($interval->y * 12) + $interval->m; // Calculate how many months
        $dayCount = $interval->days; // How many days

        // Set graphic range
        $stats = [];
        $labelsYear = (int) $dateStart->format('Y'); // Première année
        $labelsMonth = (int) $dateStart->format('n');
        /**
         * @todo use translation for month names
         */
        $monthLabels = explode('|', 'Jan|Fév|Mar|Avr|Mai|Juin|Jui|Aou|Sept|Oct|Nov|Déc');
        $labelsDay = (int) $dateStart->format('d');
        $labels = [];

        if ($dayCount > 30 || !$isDays) { // Range by month
            for ($i = 1; $i <= $monthCount + 1; ++$i) {
                $stats[$labelsYear.'-'.$labelsMonth] = ($multiple ? [] : 0);

                $labels[] = $monthCount > 12 ? $labelsMonth.'/'.$labelsYear : $monthLabels[$labelsMonth - 1].' '.$labelsYear;

                if (12 == $labelsMonth) {
                    ++$labelsYear;
                }

                $labelsMonth = ($labelsMonth % 12) + 1;
            }
        } else { // Range by day
            for ($i = 1; $i <= $dayCount + 1; ++$i) {
                $stats[$labelsYear.'-'.$labelsMonth.'-'.$labelsDay] = ($multiple ? [] : 0);
                $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $labelsMonth, $labelsYear);

                $labels[] = $labelsDay.'/'.$labelsMonth.'/'.$labelsYear;

                if (0 == $labelsDay % $daysInMonth) { // Next month
                    if (12 == $labelsMonth) {
                        ++$labelsYear;
                    }
                    
                    $labelsMonth = ($labelsMonth % 12) + 1;
                }
                
                $labelsDay = ($labelsDay % $daysInMonth) + 1;
            }
        }

        return [
            'stats' => $stats,
            'labels' => $labels,
            'dates' => [
                'start' => $dateStart,
                'end' => $dateEnd,
            ],
        ];
    }

    protected function getDateCriteria(Request $request, array $criteria = []): array
    {
        $start = $request->query->get('dateStart', null);
        $end = $request->query->get('dateEnd', null);

        if (null === $start) {
            $start = (new \DateTime('first day of 11 months ago'))->format('Y-m-d');
        } else {
            $start = Utils::convertJsDateTime($start, 'Y-m-d');
        }

        if (null === $end) {
            $end = (new \DateTime())->format('Y-m-t');
        } else {
            $end = Utils::convertJsDateTime($end, 'Y-m-t');
        }

        $criteria['startDate'] = \DateTime::createFromFormat('Y-m-d', $start);
        $criteria['endDate'] = \DateTime::createFromFormat('Y-m-d', $end);

        return $criteria;
    }
}
