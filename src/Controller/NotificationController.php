<?php

namespace Tigris\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Manager\NotificationManager;

#[Route('/notification')]
#[IsGranted('ROLE_USER')]
class NotificationController extends BaseController
{
    #[Route('/last', options: ['expose' => true])]
    public function last(NotificationManager $notificationManager): Response
    {
        $user = $this->getUser();
        $entities = $notificationManager->findLast($user);
        $unReadCount = $notificationManager->unreadCount($user);

        return new JsonResponse(['notifications' => $this->generateJsonEntities($entities), 'unreadCount' => $unReadCount]);
    }

    #[Route('/read', options: ['expose' => true])]
    public function read(NotificationManager $notificationManager): JsonResponse
    {
        $user = $this->getUser();
        $notificationManager->read($user);

        return new JsonResponse([]);
    }

    #[Route('/list.{_format}', defaults: ['_format' => 'html'], options: ['expose' => true])]
    public function list(Request $request, string $_format, NotificationManager $notificationManager): Response
    {
        $this->generateBreadcrumbs([
            'notification.yours' => [
                'route' => 'tigris_base_notification_list',
            ],
        ]);

        if ('json' === $_format) {
            $criteria = $this->getCriteria($request);
            $criteria['user'] = $this->getUser();
            $results = $notificationManager->findData($criteria);

            return $this->json($results);
        }

        return $this->render('@TigrisBase/notification/list.html.twig');
    }
}
