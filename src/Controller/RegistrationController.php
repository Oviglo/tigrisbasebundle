<?php

namespace Tigris\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;
use Tigris\BaseBundle\Event\RegistrationEvent;
use Tigris\BaseBundle\Event\RegistrationEvents;
use Tigris\BaseBundle\Form\Type\RegistrationFormType;
use Tigris\BaseBundle\Mailing\EmailVerifyEmail;
use Tigris\BaseBundle\Manager\UserManager;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Security\LoginFormAuthenticator;

#[Route('/register')]
class RegistrationController extends BaseController
{
    #[Route('')]
    public function register(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        EntityManagerInterface $entityManager,
        UserAuthenticatorInterface $userAuthenticator,
        LoginFormAuthenticator $authenticator,
        EventDispatcherInterface $eventDispatcher,
        UserManager $userManager,
        VerifyEmailHelperInterface $verifyEmailHelper,
        EmailVerifyEmail $emailVerifyEmail,
        TranslatorInterface $translator
    ): Response|null|RedirectResponse {
        if (!(bool) $this->getParameter('tigris_base')['active_user_registration']) {
            throw new NotFoundHttpException();
        }

        $user = $userManager->createUser();

        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            if ((bool) $this->getParameter('tigris_base')['registration_mail_checking']) {
                $user->setEnabled(false);
                $user->setVerified(false);
            }

            $entityManager->persist($user);
            $entityManager->flush();

            $signatureComponents = $verifyEmailHelper->generateSignature(
                'tigris_base_registration_verifyuseremail',
                $user->getId(),
                $user->getEmail(),
                ['id' => $user->getId()]
            );

            if ($user->isEnabled()) {
                return $userAuthenticator->authenticateUser(
                    $user,
                    $authenticator,
                    $request
                );
            }
            
            $response = $this->redirectToRoute('homepage');

            $event = new RegistrationEvent($user, $request, $response);
            $eventDispatcher->dispatch($event, RegistrationEvents::SUCCESS);

            if ((bool) $this->getParameter('tigris_base')['registration_mail_checking']) {
                $this->addFlash('info', $translator->trans('registration.verify_email'));

                $emailVerifyEmail($user, $signatureComponents->getSignedUrl());

                return $this->redirectToRoute('tigris_base_security_login');
            }

            return $event->getResponse();
        }

        $response = $this->render('@TigrisBase/registration/register.html.twig', [
            'registrationForm' => $form,
            'entity' => $user,
        ]);

        $preLoadEvent = new RegistrationEvent($user, $request, $response);

        $eventDispatcher->dispatch($preLoadEvent, RegistrationEvents::PRE_LOAD);

        return $preLoadEvent->getResponse();
    }

    #[Route('/verify')]
    public function verifyUserEmail(Request $request, VerifyEmailHelperInterface $verifyEmailHelper, UserRepository $userRepository, EntityManagerInterface $em): Response
    {
        $user = $userRepository->findOneById($request->query->get('id', 0));

        if (!$user) {
            throw new NotFoundHttpException();
        }

        if ($user->isVerified()) {
            $this->addFlash('info', 'registration.already_verified');

            return $this->redirectToRoute('tigris_base_security_login');
        }

        try {
            $verifyEmailHelper->validateEmailConfirmationFromRequest(
                $request,
                $user->getId(),
                $user->getEmail(),
            );
        } catch (VerifyEmailExceptionInterface $verifyEmailException) {
            $this->addFlash('error', $verifyEmailException->getReason());

            return $this->redirectToRoute('tigris_base_registration_register');
        }

        $user->setVerified(true);
        $user->setEnabled(true);
        
        $em->flush();

        $this->addFlash('success', 'registration.mail_verified');

        return $this->redirectToRoute('tigris_base_security_login');
    }
}
