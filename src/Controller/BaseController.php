<?php

namespace Tigris\BaseBundle\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class BaseController extends AbstractController
{
    protected $breadcrumbs;

    public const FORMAT_BOOLEAN_NULL = 'boolean_null';

    public function generateBreadcrumbs(array $items = []): void
    {
        $breadcrumbs = $this->container->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('menu.home', $this->generateUrl('homepage'));
        foreach ($items as $label => $route) {
            if (is_array($route)) {
                $params = $route['params'] ?? [];
                $url = $this->generateUrl($route['route'], $params);
            } elseif (is_string($route)) {
                $url = $route;
            } else {
                $url = '';
            }

            if (isset($route['label'])) {
                $label = $route['label'];
                $translate = $route['translate'] ?? true;
            } else {
                $translate = !ctype_upper(substr($label, 0, 1));
            }

            $breadcrumbs->addItem($label, $url, [], $translate);
        }
    }

    public static function getSubscribedServices(): array
    {
        return array_merge(AbstractController::getSubscribedServices(), [
            'white_october_breadcrumbs' => '?'.Breadcrumbs::class,
            'jms_serializer' => '?'.SerializerInterface::class,
        ]);
    }

    public function getCriteria(Request $request, string $sessionName = null): array
    {
        $savedCriteria = [
            'count' => 30,
            'order' => 'id',
            'rev' => true,
            'begin' => 0,
        ];

        if (null !== $sessionName) {
            $session = new Session();
            if ($session->has($sessionName)) {
                $savedCriteria = $session->get($sessionName);
            }
        }

        $criteria = [
            'count' => (int) $request->query->get('count', $savedCriteria['count']),
            'order' => $request->query->get('order', $savedCriteria['order']),
            'rev' => 'true' == $request->query->get('rev', $savedCriteria['rev']),
            'begin' => (int) $request->query->get('begin', $savedCriteria['begin']),
        ];

        if (null !== $sessionName) {
            $this->saveCriteria($criteria, $sessionName);
        }

        return $criteria;
    }

    public function formatCriteria(string $value, string $format): mixed
    {
        return match ($format) {
            self::FORMAT_BOOLEAN_NULL => '' === $value ? null : 1 === (int) $value,
            'default' => $value,
        };
    }

    public function saveCriteria(array $criteria, string $sessionName): void
    {
        $session = new Session();

        $session->set($sessionName, $criteria);
    }

    /**
     * Return formated array from Paginator object to json.
     */
    public function paginatorToJsonResponse(Paginator|\Iterator $data, array $groups = ['Default']): JsonResponse
    {
        if ($data instanceof Paginator) {
            $count = $data->count();
            $data = $data->getIterator();
        } else {
            $count = is_countable($data) ? count($data) : 0;
        }

        $jsonData = [
            'totalCount' => $count,
            'entities' => $this->generateJsonEntities($data, $groups),
        ];

        return new JsonResponse($jsonData);
    }

    public function getSerializer(): SerializerInterface
    {
        return $this->container->get('jms_serializer');
    }

    public function generateJsonEntities(array|object $data, array $groups = ['Default']): mixed
    {
        return json_decode($this->getSerializer()->serialize($data, 'json', SerializationContext::create()->setGroups($groups)->setSerializeNull(true)), true, 512, JSON_THROW_ON_ERROR);
    }

    public function addSuccessFlash(string $message): void
    {
        $this->addFlash('success', $message);
    }

    public function addErrorFlash(string $message): void
    {
        $this->addFlash('error', $message);
    }

    public function addWarningFlash(string $message): void
    {
        $this->addFlash('warning', $message);
    }
}
