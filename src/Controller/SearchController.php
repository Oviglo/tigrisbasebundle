<?php

namespace Tigris\BaseBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\SearchEvent;

#[Route('/search')]
class SearchController extends BaseController
{
    #[Route('/.{_format}', defaults: ['_format' => 'html'], options: ['expose' => true])]
    public function index(Request $request, string $_format, EventDispatcherInterface $eventDispatcher, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $optionEvent = new GenericEvent(null, []);
        $eventDispatcher->dispatch($optionEvent, Events::LOAD_SEARCH_OPTIONS);

        $filters = [
            'search' => $request->query->get('search', ''),
            'options' => $request->query->all('options'),
        ];

        if ('json' === $_format) {
            $event = new SearchEvent($filters['search'], $filters['options'], $em, $this->container->get('router'));
            if (strlen($filters['search']) > 2) {
                $eventDispatcher->dispatch($event, Events::SEARCH);
            }

            $results = $event->getResults();
            $totalCount = count($results);

            if (0 == $totalCount) {
                $results[] = [
                    'title' => $translator->trans('search.no_result'),
                    'content' => '',
                    'id' => uniqid(),
                ];
            }

            return new JsonResponse([
                'totalCount' => $totalCount,
                'entities' => $results,
            ]);
        }

        return $this->render('@TigrisBase/search/index.html.twig', [
            'options' => $optionEvent->getArguments(),
            'search' => $filters['search'],
        ]);
    }
}
