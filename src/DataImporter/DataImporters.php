<?php

namespace Tigris\BaseBundle\DataImporter;

class DataImporters
{
    private array $importers = [];

    public function addImporter(AbstractImporter $importer, string $name): self
    {
        if (!isset($this->importers[$name])) {
            $this->importers[$name] = $importer;
        }

        return $this;
    }

    public function getImporter(string $name): ?AbstractImporter
    {
        return $this->importers[$name] ?? null;
    }

    /**
     * @return array<AbstractImporter>
     */
    public function getImporters(): array
    {
        return $this->importers;
    }
}
