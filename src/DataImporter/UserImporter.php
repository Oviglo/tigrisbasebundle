<?php

namespace Tigris\BaseBundle\DataImporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Repository\UserRepository;

class UserImporter extends AbstractImporter
{
    public function __construct(
        protected readonly TokenStorageInterface $token,
        protected readonly EntityManagerInterface $entityManager,
        private readonly UserRepository $userRepository
    ) {
    }

    public function createEntity(array $data): null|object
    {
        return (new User())
            ->setUsername($data['username'])
            ->setEmail($data['email'])
            ->setEnabled($data['enabled'])
            ->setCreatedAt($data['createdAt'])
            ->setPassword(md5(uniqid()));
    }

    public function getEntity(string $primaryKey, mixed $primaryKeyValue, array $data): ?object
    {
        try {
            return $this->userRepository->findOneBy([$primaryKey => $primaryKeyValue]);
        } catch (\Exception) {
            return null;
        }
    }

    public function getMapping(): array
    {
        return [
            'username' => 'string',
            'email' => 'string',
            'enabled' => 'bool',
            'createdAt' => 'datetime',
        ];
    }
}
