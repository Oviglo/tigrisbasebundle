<?php

namespace Tigris\BaseBundle\DataImporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Tigris\BaseBundle\Entity\DataImport;
use Tigris\BaseBundle\Entity\File as EntityFile;
use Tigris\BaseBundle\Utils\Utils;

abstract class AbstractImporter
{
    protected const FILE_DIR = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'data_import'.DIRECTORY_SEPARATOR;

    public function __construct(protected readonly TokenStorageInterface $token, protected readonly EntityManagerInterface $entityManager)
    {
    }

    protected File $file;

    protected array $entities;

    protected DataImport $importEntity;

    public function setImportEntity(DataImport $importEntity): self
    {
        $this->importEntity = $importEntity;

        return $this;
    }

    public function getFile(): File
    {
        return $this->file;
    }

    public function setFile(File $file): self
    {
        $this->file = $file;

        return $this;
    }

    protected function createEntities(array $data): void
    {
        foreach ($data as $datum) {
            $this->entities[] = $this->createEntity($datum);
        }
    }

    protected function hydrateEntity($entity, $data): void
    {
        $importMapping = $this->importEntity->getMapping();

        foreach ($importMapping as $map) {
            if (null != $map['from']) {
                $m = 'set'.ucfirst((string) $map['to']);
                try {
                    if (isset($data[$map['to']])) {
                        $entity->$m($this->formatData($data[$map['to']], $map['format']));
                    }
                } catch (\Exception) {
                }
            }
        }
    }

    protected function formatData(mixed $data, string $format): mixed
    {
        switch ($format) {
            case 'string':
                return trim((string) $data);
            case 'int':
                return (int) $data;
            case 'float':
                return (float) $data;
            case 'lower_string':
                return strtolower((string) $data);
            case 'upper_string':
                return strtoupper((string) $data);
            case 'url':
                $urls = Utils::extractUrls($data);

                return $urls[0] ?? '';
            case 'file_url':
                $urls = Utils::extractUrls($data);

                return $this->loadFileFromUrl($urls[0]);
        }

        return $data;
    }

    protected function loadFileFromUrl(string $url): null|EntityFile
    {
        $filePath = self::FILE_DIR.basename($url);
        if (file_put_contents($filePath, file_get_contents($url))) {
            $file = new File($filePath);
            $user = $this->token->getToken()->getUser();
            if (!is_object($user)) {
                $user = null;
            }
            
            $fileEntity = (new EntityFile())
                ->setUser($user)
                ->setFile($file);

            $folder = $this->getFileFolder();
            if (null !== $folder) {
                $fileEntity->setFolder($folder);
            }

            $this->entityManager->persist($fileEntity);
            // $this->entityManager->flush();

            return $fileEntity;
        }

        return null;
    }

    public function getFileFolder()
    {
        return null;
    }

    /**
     * @return array<ImportOption>
     */
    public function getOptions(): array
    {
        return [];
    }

    abstract public function createEntity(array $data): null|object;

    abstract public function getEntity(string $primaryKey, mixed $primaryKeyValue, array $data): ?object;

    abstract public function getMapping(): array;
}
