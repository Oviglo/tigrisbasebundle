<?php

namespace Tigris\BaseBundle\DataImporter;

use Symfony\Component\Form\Form;

class ImportOption
{
    public function __construct(public string $name, public string $type, public array $options)
    {
    }

    public function addFormType(Form $form): void
    {
        $form->add($this->name, $this->type, $this->options);
    }
}
