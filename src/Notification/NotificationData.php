<?php

namespace Tigris\BaseBundle\Notification;

/**
 * @Deprecated use NotificationBuilder instead
 */
final class NotificationData
{
    private string $title;

    private array $buttons = [];

    private string $message = '';

    private array $messageParams = [];

    public function getButtons(): array
    {
        return $this->buttons;
    }

    public function setButtons(array $buttons): self
    {
        $this->buttons = $buttons;

        return $this;
    }

    public function addButton(NotificationButton $button): self
    {
        $this->buttons[] = $button->toArray();

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getMessageParams(): array
    {
        return $this->messageParams;
    }

    public function setMessageParams(array $messageParams): self
    {
        $this->messageParams = $messageParams;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'message' => $this->message,
            'messageParams' => $this->messageParams,
            'title' => $this->title,
            'buttons' => $this->buttons,
        ];
    }
}
