<?php

namespace Tigris\BaseBundle\Notification;

interface NotificationBuilderInterface
{
    public function setTitle(string $title): self;

    public function getTitle(): string;

    public function setMessage(string $message): self;

    public function setMessageParams(array $messageParams): self;

    public function setButtons(array $buttons): self;

    public function addButton(NotificationButton $button): self;

    public function build(): array;
}
