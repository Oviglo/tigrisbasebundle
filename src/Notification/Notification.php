<?php

namespace Tigris\BaseBundle\Notification;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Entity\Notification as EntityNotification;
use Tigris\BaseBundle\Mailing\NotificationEmail;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\Basebundle\Notification\NotificationData;

class Notification
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly NotificationEmail $notificationEmail,
    ) {
    }

    public function send(User $user, NotificationData|NotificationBuilderInterface $data, User $owner = null, $entity = null, $sendMail = true): void
    {
        if ($user === $owner) {
            return;
        }

        if ($data instanceof NotificationData) {
            $data = (new NotificationBuilder())
                ->setTitle($data->getTitle())
                ->setMessage($data->getMessage())
                ->setMessageParams($data->getMessageParams())
                ->setButtons($data->getButtons())
            ;
        }

        $notificationEntity = (new EntityNotification())
            ->setUser($user)
            ->setOwner($owner)
            ->setData($data->build())
        ;
        if ($data instanceof NotificationBuilderInterface) {
            $notificationEntity->setData($data->build());
        } else {
            $notificationEntity->setData($data->toArray());
        }

        if (is_object($entity) && method_exists($entity, 'getId')) {
            $notificationEntity->setEntityclass($entity::class)
                ->setEntityId($entity->getId())
            ;
        }

        $this->entityManager->persist($notificationEntity);
        $this->entityManager->flush();

        if ($user->isEnableNotificationEmail() && $sendMail) {
            ($this->notificationEmail)($data, $user);
        }
    }
}
