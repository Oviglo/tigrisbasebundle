<?php

namespace Tigris\BaseBundle\Notification;

final readonly class NotificationButton
{
    public function __construct(private string $label, private string $route, private array $routeParams = [])
    {
    }

    public function toArray(): array
    {
        return [
            'label' => $this->label,
            'route' => $this->route,
            'routeParams' => $this->routeParams,
        ];
    }
}
