<?php

namespace Tigris\BaseBundle;

use ReCaptcha\ReCaptcha;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use Tigris\BaseBundle\DependencyInjection\Compiler\DashboardPass;
use Tigris\BaseBundle\DependencyInjection\DataExporterPass;
use Tigris\BaseBundle\DependencyInjection\DataImporterPass;
use Tigris\BaseBundle\DependencyInjection\FileReaderPass;
use Tigris\BaseBundle\DependencyInjection\FileWriterPass;
use Tigris\BaseBundle\Entity\Wording;
use Tigris\BaseBundle\Form\Type\RecaptchaType;
use Tigris\BaseBundle\Service\Meta\GraphAPI\FacebookGraphAPI;
use Tigris\BaseBundle\Service\Meta\GraphAPI\InstagramGraphAPI;

class TigrisBaseBundle extends AbstractBundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new FileReaderPass());
        $container->addCompilerPass(new DataImporterPass());
        $container->addCompilerPass(new DataExporterPass());
        $container->addCompilerPass(new FileWriterPass());
        $container->addCompilerPass(new DashboardPass());
    }

    public function loadExtension(array $config, ContainerConfigurator $containerConfigurator, ContainerBuilder $builder): void
    {
        $parameters = $containerConfigurator->parameters()
            ->set('tigris_base', $config)
        ;

        foreach ($config as $key => $value) {
            $parameters->set('tigris_base.' . $key, $value);
        }

        $containerConfigurator->import(__DIR__.'/../config/services.yaml');

        $containerConfigurator->services()
            ->get(FacebookGraphAPI::class)
            ->args([
                '$appId' => $config['facebook']['app_id'],
                '$appSecret' => $config['facebook']['app_secret'],
                '$accessToken' => $config['facebook']['access_token'],
                '$pageId' => $config['facebook']['page_id'],
            ])
            ->get(InstagramGraphAPI::class)
            ->args([
                '$appId' => $config['facebook']['app_id'],
                '$appSecret' => $config['facebook']['app_secret'],
                '$accessToken' => $config['facebook']['access_token'],
                '$accountId' => $config['facebook']['instagram_account_id'],
            ])
            ->set(ReCaptcha::class)
            ->args([
                '$secret' => $config['recaptcha']['secret_key'],
            ])
            ->get(RecaptchaType::class)
            ->args([
                '$siteKey' => $config['recaptcha']['site_key'],
                '$host' => $config['recaptcha']['host'],
                '$enabled' => $config['recaptcha']['enabled'],
            ])
        ;
    }

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
            ->booleanNode('active_user_notification')->defaultFalse()->end()
            ->booleanNode('active_user_registration')->defaultTrue()->end()
            ->booleanNode('registration_mail_checking')->defaultTrue()->end()
            ->booleanNode('active_online_counter')->defaultFalse()->end()
            ->booleanNode('enable_statistics')->defaultTrue()->end()

            ->arrayNode('data_export')
            ->useAttributeAsKey('id')
            ->arrayPrototype()
            ->children()
            ->arrayNode('attr')
            ->requiresAtLeastOneElement()
            ->useAttributeAsKey('value')
            ->prototype('scalar')->end()
            ->end()
            ->end()
            ->end()
            ->end()

            ->arrayNode('recaptcha')
            ->canBeDisabled()
            ->children()
            ->scalarNode('site_key')->defaultValue('')->end()
            ->scalarNode('secret_key')->defaultValue('')->end()
            ->scalarNode('host')->defaultValue('www.google.com')->end()
            ->scalarNode('enabled')->defaultTrue()->end()
            ->end()
            ->end()

            ->arrayNode('shibboleth')
            ->canBeDisabled()
            ->children()
            ->scalarNode('login_path')->defaultValue('Shibboleth.sso/Login')->end()
            ->scalarNode('logout_path')->defaultValue('Shibboleth.sso/Logout')->end()
            ->scalarNode('login_target')->defaultValue('')->end()
            ->scalarNode('logout_target')->defaultValue('')->end()
            ->scalarNode('session_id')->defaultValue('Shib-Session-ID')->end()
            ->scalarNode('username')->defaultValue('username')->end()
            ->arrayNode('attributes')
            ->scalarPrototype()->end()
            ->end()
            ->end()
            ->end()

            ->arrayNode('image_styles')
            ->useAttributeAsKey('id')
            ->arrayPrototype()
            ->children()
            ->arrayNode('crop')
            ->children()
            ->integerNode('width')->min(5)->max(5000)->isRequired()->end()
            ->integerNode('height')->min(5)->max(5000)->isRequired()->end()
            ->integerNode('type')->defaultValue(1)->min(1)->max(6)->end()
            ->end()
            ->end()
            ->arrayNode('resize')
            ->children()
            ->integerNode('width')->min(5)->max(5000)->end()
            ->integerNode('height')->min(5)->max(5000)->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()

            ->arrayNode('facebook')
            ->canBeDisabled()
            ->children()
            ->scalarNode('pixel_id')->defaultValue('')->end()
            ->scalarNode('app_id')->defaultValue('%env(string:META_APP_ID)%')->end()
            ->scalarNode('app_secret')->defaultValue('%env(string:META_APP_SECRET)%')->end()
            ->scalarNode('access_token')->defaultValue('%env(string:META_ACCESS_TOKEN)%')->end()
            ->scalarNode('page_id')->defaultValue('%env(string:META_PAGE_ID)%')->end()
            ->scalarNode('instagram_account_id')->defaultValue('%env(string:META_INSTAGRAM_ACCOUNT_ID)%')->end()
            ->end()
            ->end()

            ->arrayNode('wording')
            ->useAttributeAsKey('page')
            ->arrayPrototype()
            ->children()
            ->arrayNode('wordings')
            ->arrayPrototype()
            ->children()
            ->scalarNode('name')->end()
            ->enumNode('type')->values(Wording::getTypes())->end()
            ->scalarNode('default')->end()
            ->scalarNode('description')->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
        ;
    }

    public function getAlias(): string
    {
        return 'tigris_base';
    }
}
