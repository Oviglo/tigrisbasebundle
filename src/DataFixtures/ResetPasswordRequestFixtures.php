<?php

namespace Tigris\BaseBundle\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Entity\ResetPasswordRequest;

class ResetPasswordRequestFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $user = $this->getReference(UserFixtures::REFERENCE.'obelix', User::class);
        $resetPasswordRequest = new ResetPasswordRequest($user, new \DateTimeImmutable('+1 hour'), 'selector', 'hashedToken');
        $manager->persist($resetPasswordRequest);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
