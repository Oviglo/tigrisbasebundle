<?php

namespace Tigris\BaseBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Attribute\When;
use Tigris\BaseBundle\Entity\Config;
use Tigris\BaseBundle\Manager\ConfigManager;

#[When(env: 'dev')]
#[When(env: 'test')]
class ConfigFixtures extends Fixture
{
    private function data(): \Iterator
    {
        yield [
            'bundle' => 'TigrisBaseBundle',
            'name' => 'TigrisBaseBundle.title',
            'value' => 'Tigris',
            'type' => ConfigManager::TYPE_TEXT,
        ];

        yield [
            'bundle' => 'TigrisBaseBundle',
            'name' => 'TigrisBaseBundle.description',
            'value' => 'Application',
            'type' => ConfigManager::TYPE_TEXT,
        ];

        yield [
            'bundle' => 'TigrisBaseBundle',
            'name' => 'TigrisBaseBundle.noreply_mail',
            'value' => 'notification@oviglo.fr',
            'type' => ConfigManager::TYPE_EMAIL,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data() as $data) {
            $entity = (new Config())
                ->setBundle($data['bundle'])
                ->setName($data['name'])
                ->setValue($data['value'])
                ->setType($data['type'])
            ;

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
