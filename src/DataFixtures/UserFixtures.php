<?php

namespace Tigris\BaseBundle\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Attribute\When;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Tigris\BaseBundle\Utils\Utils;

#[When(env: 'dev')]
#[When(env: 'test')]
class UserFixtures extends Fixture
{
    public const REFERENCE = 'user-';

    private array $users = [
        'OVIGLO' => [
            'username' => 'Oviglo',
            'password' => '01Azerty',
            'email' => 'ovigne.loic@gmail.com',
            'super_admin' => true,
            'date' => 'now',
        ],
        // Users from Asterix
        'ASTERIX' => [
            'username' => 'Asterix',
            'password' => '01Azerty',
            'email' => 'asterix@yopmail.com',
            'groups' => ['ADMINISTRATEUR'],
            'date' => 'first day of this month',
        ],
        'OBELIX' => [
            'username' => 'Obelix',
            'password' => '01Azerty',
            'email' => 'obelix@yopmail.com',
            'date' => '20 days ago 1 year ago 5 hours ago 23 minutes ago',
        ],
        'ASSURANCETOURIX' => [
            'username' => 'Assurancetourix',
            'password' => '01Azerty',
            'email' => 'assurancetourix@yopmail.com',
            'date' => '1 month ago 2 hours ago',
        ],
        'IDEFIX' => [
            'username' => 'Idéfix',
            'password' => '01Azerty',
            'email' => 'idefix@yopmail.com',
            'enabled' => false,
            'date' => '4 days ago 1 hour ago 32 minutes ago',
        ],
        'PANORAMIX' => [
            'username' => 'Panoramix',
            'password' => '01Azerty',
            'email' => 'panoramix@yopmail.com',
            'date' => '80 years ago 3 months ago 1 day ago 5 hours ago 12 minutes ago',
        ],
        'BONNEMINE' => [
            'username' => 'Bonnemine',
            'password' => '01Azerty',
            'email' => 'bonnemine@yopmail.com',
            'enabled' => false,
            'date' => '3 years ago 4 months ago 4 days ago 1 hour ago 48 minutes ago',
        ],
        'ABRARACOURCIX' => [
            'username' => 'Abraracourcix',
            'password' => '01Azerty',
            'email' => 'Abraracourcix@example.com',
            'date' => '2 years ago 3 months ago 2 days ago 2 hours ago 2 minutes ago',
            'enabled' => false,
            'verified' => false,
        ],
        'FALBALA' => [
            'username' => 'Falbala',
            'password' => '01Azerty',
            'email' => 'falbal@example.com',
            'date' => '1 year ago 2 months ago 1 day ago 1 hour ago 1 minute ago',
            'verified' => false,
            'enabled' => true,
        ],
    ];

    public function __construct(private readonly UserPasswordHasherInterface $passwordEncoder) {}

    public function load(ObjectManager $manager): void
    {
        foreach ($this->users as $user) {
            $entity = new User();
            $password = $this->passwordEncoder->hashPassword(
                $entity,
                $user['password']
            );
            $entity
                ->setUsername($user['username'])
                ->setPassword($password)
                ->setEmail($user['email'])
            ;

            if (isset($user['enabled'])) {
                $entity->setEnabled($user['enabled']);
            } else {
                $entity->setEnabled(true);
            }

            if (isset($user['super_admin']) && $user['super_admin']) {
                $entity->addRole('ROLE_SUPER_ADMIN');
            }

            if (!empty($user['date'])) {
                $entity->setCreatedAt(new \DateTime($user['date']));
            }

            if (isset($user['verified'])) {
                $entity->setVerified($user['verified']);
            }

            $this->addReference(self::REFERENCE.Utils::slugify($entity->getUsername()), $entity);

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
