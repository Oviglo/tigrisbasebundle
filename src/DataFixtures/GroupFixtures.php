<?php

namespace Tigris\BaseBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Entity\Model\Group;

class GroupFixtures extends Fixture implements DependentFixtureInterface
{
    public const REFERENCE = 'group-';

    private array $groups = [
        'ADMINISTRATEUR' => [
            'name' => 'Administrateur',
            'roles' => ['ROLE_ADMIN'],
        ],
        'UTILISATEUR' => [
            'name' => 'Utilisateur',
            'roles' => ['ROLE_USER'],
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->groups as $data) {
            $group = new Group();
            $group->setName($data['name']);
            $group->setRoles($data['roles']);

            $manager->persist($group);

            // $this->addReference(self::REFERENCE.$key, $group);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
