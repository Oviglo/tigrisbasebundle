<?php

namespace Tigris\BaseBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Entity\Wording;

class WordingFixtures extends Fixture
{
    private function data(): array
    {
        return [
            [
                'page' => 'home',
                'name' => 'main title',
                'type' => Wording::TYPE_TEXT,
                'content' => 'Bienvenue sur Oviglo',
            ],
            [
                'page' => 'home',
                'name' => 'contact',
                'type' => Wording::TYPE_TEXT,
                'content' => 'Contactez-nous',
            ],
            [
                'page' => 'contact',
                'name' => 'title',
                'type' => Wording::TYPE_TEXT,
                'content' => 'Contactez-nous',
            ],
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach (self::data() as $data) {
            $entity = (new Wording())
                ->setPage($data['page'])
                ->setName($data['name'])
                ->setContent($data['content'])
                ->setType($data['type'])
            ;

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
