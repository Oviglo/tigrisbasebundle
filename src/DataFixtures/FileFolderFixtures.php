<?php

namespace Tigris\BaseBundle\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Attribute\When;
use Tigris\BaseBundle\Entity\FileFolder;

#[When(env: 'dev')]
#[When(env: 'test')]
class FileFolderFixtures extends Fixture implements DependentFixtureInterface
{
    public const REFERENCE = 'file-folder-';

    private function data(): \Generator
    {
        yield [
            'reference' => 0,
            'name' => "Images d'article",
            'parent' => null,
            'user' => 'oviglo',
        ];

        yield [
            'reference' => 1,
            'name' => 'Fichiers divers',
            'parent' => null,
            'user' => 'oviglo',
        ];

        yield [
            'reference' => 2,
            'name' => 'Photo',
            'parent' => 1,
            'user' => 'oviglo',
        ];

        yield [
            'reference' => 3,
            'name' => 'Oviglo',
            'parent' => null,
            'user' => 'oviglo',
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data() as $data) {
            $entity = (new FileFolder())
                ->setName($data['name'])
                ->setUser($this->getReference(UserFixtures::REFERENCE.$data['user'], User::class))
            ;

            if (null !== $data['parent']) {
                $entity->setParent($this->getReference(self::REFERENCE.$data['parent'], FileFolder::class));
            }

            $manager->persist($entity);

            $this->addReference(self::REFERENCE.$data['reference'], $entity);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
