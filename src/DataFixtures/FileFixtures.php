<?php

namespace Tigris\BaseBundle\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Entity\File;

class FileFixtures extends Fixture implements DependentFixtureInterface
{
    public static function getData(): \Iterator
    {
        yield ['file1', 'file1.txt', 'text/plain', 'This is a test file 1', 'asterix'];

        yield ['file2', 'file2.txt', 'text/plain', 'This is a test file 2', 'asterix'];

        yield ['file3', 'file3.txt', 'text/plain', 'This is a test file 3', 'asterix'];
    }

    public function load(ObjectManager $manager): void
    {
        foreach (self::getData() as [$name, $filename, $mimeType, $content, $username]) {
            $file = new File();
            $file->setName($name);
            $file->setUser($this->getReference(UserFixtures::REFERENCE.$username, User::class));

            $manager->persist($file);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
