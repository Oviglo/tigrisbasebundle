<?php

namespace Tigris\BaseBundle\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Entity\Notification;
use Tigris\BaseBundle\Notification\NotificationData;

class NotificationFixtures extends Fixture implements DependentFixtureInterface
{
    private function data(): \Generator
    {
        $notif = (new NotificationData())
            ->setTitle('Nouvelle notification')
            ->setMessage('Une nouvelle notification')
        ;
        yield [
            'user' => 'oviglo',
            'readed' => false,
            'data' => $notif->toArray(),
        ];

        yield [
            'user' => 'oviglo',
            'readed' => true,
            'data' => $notif->toArray(),
        ];

        yield [
            'user' => 'oviglo',
            'readed' => false,
            'data' => $notif->toArray(),
            'entity' => UserFixtures::REFERENCE.'asterix',
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data() as $data) {
            $entity = (new Notification())
                ->setReaded($data['readed'])
                ->setUser($this->getReference(UserFixtures::REFERENCE.$data['user'], User::class))
                ->setData($data['data'])
            ;

            if (isset($data['entity'])) {
                $entityRef = $this->getReference($data['entity'], User::class);
                $entity->setEntityClass($entityRef::class);
                $entity->setEntityId($entityRef->getId());
            }

            $manager->persist($entity);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
