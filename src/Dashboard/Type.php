<?php

namespace Tigris\BaseBundle\Dashboard;

enum Type: string
{
    case ICON_COUNTER = 'icon-counter';
    case LIST = 'list';
    case CHART = 'chart';
}
