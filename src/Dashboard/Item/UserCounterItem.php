<?php

namespace Tigris\BaseBundle\Dashboard\Item;

use Symfony\Component\DependencyInjection\Attribute\Autoconfigure;
use Tigris\BaseBundle\Contracts\Dashboard\DashboardItemInterface;
use Tigris\BaseBundle\Dashboard\ItemConfigBuilder;
use Tigris\BaseBundle\Dashboard\ItemDataBuilder;
use Tigris\BaseBundle\Dashboard\Location;
use Tigris\BaseBundle\Dashboard\Type;
use Tigris\BaseBundle\Repository\UserRepository;

#[Autoconfigure(public: true, autowire: true)]
class UserCounterItem implements DashboardItemInterface
{
    public function __construct(private readonly UserRepository $userRepository) {}

    public function configure(): array
    {
        return (new ItemConfigBuilder())
            ->id('user_counter')
            ->icon('users')
            ->title('dashboard.users')
            ->route('tigris_base_admin_user_index')
            ->placement(Location::HEADER, 0)
            ->type(Type::ICON_COUNTER)
            ->toArray()
        ;
    }

    public function data(): array
    {
        $userData = $this->userRepository->dashboardData();

        $idb = (new ItemDataBuilder())
            ->value($userData['enabled'])
        ;

        if ($userData['thisMonth'] > 0) {
            $idb->extra('dashboard.user.this_month', textParams: ['#' => $userData['thisMonth']]);
        }

        return $idb->toArray();
    }
}
