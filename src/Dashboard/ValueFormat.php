<?php

namespace Tigris\BaseBundle\Dashboard;

enum ValueFormat
{
    case NUMERIC;
    case CURRENCY;
}