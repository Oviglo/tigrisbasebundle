<?php

namespace Tigris\BaseBundle\Dashboard;

enum Location
{
    case HEADER;
    case SECTION_1;
    case SECTION_2;
    case SECTION_3;
}
