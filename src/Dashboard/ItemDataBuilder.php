<?php

namespace Tigris\BaseBundle\Dashboard;

class ItemDataBuilder
{
    private array $data = [
        'value' => '',
        'valueFormat' => ValueFormat::NUMERIC,
        'extra' => [],
        'list' => [
            'items' => [],
        ],
    ];

    public function value(string $value, ?ValueFormat $valueFormat = null): self
    {
        $this->data['value'] = $value;

        if ($valueFormat instanceof ValueFormat) {
            $this->valueFormat($valueFormat);
        }

        return $this;
    }

    public function valueFormat(ValueFormat $valueFormat): self
    {
        $this->data['valueFormat'] = $valueFormat;

        return $this;
    }

    public function extra(string $text, string $type = 'success', array $textParams = []): self
    {
        if (!isset($this->data['extra'])) {
            $this->data['extra'] = [];
        }

        $this->data['extra'][] = [
            'text' => $text,
            'textParams' => $textParams,
            'type' => $type,
        ];

        return $this;
    }

    public function addListItem(string $label, string $value): self
    {
        $this->data['list']['items'][] = [
            'label' => $label,
            'value' => $value,
        ];

        return $this;
    }

    public function toArray(): array
    {
        return $this->data;
    }
}
