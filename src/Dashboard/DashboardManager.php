<?php

namespace Tigris\BaseBundle\Dashboard;

use Tigris\BaseBundle\Contracts\Dashboard\DashboardItemInterface;

class DashboardManager
{
    private array $items = [];

    public function addItem(DashboardItemInterface $item): void
    {
        $this->items[] = $item;
    }

    public function itemsByLocation(Location $location): array
    {
        $items = array_filter($this->items, fn (DashboardItemInterface $item): bool => $item->configure()['placement']['location'] === $location);
        usort($items, fn (DashboardItemInterface $a, DashboardItemInterface $b): int => $a->configure()['placement']['position'] <=> $b->configure()['placement']['position']);

        return $items;
    }

    public function getItems(): array
    {
        return [
            'header' => $this->itemsByLocation(Location::HEADER),
            'section_1' => $this->itemsByLocation(Location::SECTION_1),
            'section_2' => $this->itemsByLocation(Location::SECTION_2),
            'section_3' => $this->itemsByLocation(Location::SECTION_3),
        ];
    }
}
