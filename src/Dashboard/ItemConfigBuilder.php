<?php

namespace Tigris\BaseBundle\Dashboard;

class ItemConfigBuilder
{
    private array $data = [
        'id' => '',
        'title' => '',
        'route' => [],
        'icon' => '',
        'placement' => [
            'location' => Location::HEADER,
            'position' => -1,
        ],
        'type' => Type::ICON_COUNTER,
    ];

    public function id(string $id): self
    {
        $this->data['id'] = trim($id);

        return $this;
    }

    public function title(string $title): self
    {
        $this->data['title'] = trim($title);

        return $this;
    }

    public function route(string $route, array $params = []): self
    {
        $this->data['route'] = [
            'name' => $route,
            'params' => $params,
        ];

        return $this;
    }

    public function icon(string $icon): self
    {
        if (!str_contains('.icon-', $icon)) {
            $icon = '.icon-' . $icon;
        }

        $this->data['icon'] = $icon;

        return $this;
    }

    public function placement(Location $location, int $position = -1): self
    {
        $this->data['placement'] = [
            'location' => $location,
            'position' => $position,
        ];

        return $this;
    }

    public function type(Type $type): self
    {
        $this->data['type'] = $type;

        return $this;
    }

    public function toArray(): array
    {
        return $this->data;
    }
}
