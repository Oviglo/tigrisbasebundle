<?php

namespace Oviglo\TigrisBaseBundle\Tests\Integration\Manager;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Config;
use Tigris\BaseBundle\Manager\ConfigManager;

/**
 * @internal
 */
#[CoversClass(ConfigManager::class)]
class ConfigManagerTest extends KernelTestCase
{
    private ConfigManager $configManager;

    public function setUp(): void
    {
        $this->configManager = static::getContainer()->get(ConfigManager::class);
    }

    #[Test]
    #[DataProvider('formatFormDataProvider')]
    public function testFormatFormData(Config $config, array $expected): void
    {
        $data = [];
        $this->configManager->formatFormData($config, $data);

        self::assertEquals($expected, $data);
    }

    public static function formatFormDataProvider(): \Generator
    {
        $config1 = (new Config())
            ->setType(ConfigManager::TYPE_TEXT)
            ->setName('TigrisBaseBundle.test')
            ->setValue('test')
        ;

        yield [$config1, ['TigrisBaseBundle' => ['test' => 'test']]];

        $config2 = (new Config())
            ->setType(ConfigManager::TYPE_TEXT)
            ->setName('TigrisBaseBundle.sub.test')
            ->setValue('subtest')
        ;

        yield [$config2, ['TigrisBaseBundle' => ['sub' => ['test' => 'subtest']]]];

        $config3 = (new Config())
            ->setType(ConfigManager::TYPE_CHECKBOX)
            ->setName('TigrisBaseBundle.sub.checked')
            ->setValue(true)
        ;

        yield [$config3, ['TigrisBaseBundle' => ['sub' => ['checked' => true]]]];

        $config4 = (new Config())
            ->setType(ConfigManager::TYPE_DATE)
            ->setName('TigrisBaseBundle.lvl1.lvl2.at')
            ->setValue(new \DateTime('2021-01-01 00:00:00'))
        ;

        yield [$config4, ['TigrisBaseBundle' => ['lvl1' => ['lvl2' => ['at' => new \DateTime('2021-01-01 00:00:00')]]]]];
    }

    #[Test]
    public function testUnknowParameter(): void
    {
        $value = $this->configManager->getValue('TigrisBaseBundle.unknown');

        self::assertNull($value);

        $value = $this->configManager->getValue('TigrisBaseBundle.unknown.unknown');

        self::assertNull($value);
    }
}
