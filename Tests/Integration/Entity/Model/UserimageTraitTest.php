<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity\Model;

use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Entity\Model\UserimageTrait;

class UserimageTraitTest extends KernelTestCase
{
    #[Test]
    public function validEntity(): void
    {
        $entity = $this->getEntity();
        $file = $this->createMock(File::class);

        $entity->setUserimage($file);

        self::assertNotNull($entity->getUserimage());
        self::assertTrue($entity->hasUserimage());
    }

    private function getEntity(): object
    {

        return new class {
            use UserimageTrait;
        };
    }
}
