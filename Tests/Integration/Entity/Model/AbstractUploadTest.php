<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity\Model;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Model\AbstractUpload;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AbstractUploadTest extends KernelTestCase
{
    #[Test]
    public function validEntity(): void
    {
        $entity = $this->getEntity();
        $entity->preUpload();

        self::assertTrue($entity->isValid());
        self::assertStringContainsString('.jpg', $entity->getFilePath());
        self::assertStringContainsString('.jpg', $entity->getAbsolutePath());
        self::assertStringContainsString('.jpg', $entity->getWebPath());
        self::assertNotNull($entity->getFile());
        self::assertFalse($entity->getDeleteFile());

        $entity->upload();
        self::assertFalse($entity->isValid());
        $entity->removeFile();
    }

    private function getEntity(): AbstractUpload
    {
        $uploadedFile = $this->createMock(UploadedFile::class);
        $uploadedFile
            ->method('guessExtension')
            ->willReturn('jpg')
        ;

        $entity = new class extends AbstractUpload {
            protected function getUploadDir(): string
            {
                return 'uploads';
            }
        };

        $entity
            ->setFile($uploadedFile)
            ->setDeleteFile(false)
        ;

        return $entity;
    }
}