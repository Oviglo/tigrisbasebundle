<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity\Model;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Model\Group;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;

/**
 * @internal
 */
#[CoversClass(User::class)]
class UserTest extends KernelTestCase
{
    use EntityTestTrait;

    #[Test]
    public function validEntity(): void
    {
        $entity = self::getEntity();

        $this->validateEntity($entity, 0);
    }

    #[Test]
    public function validAccessors(): void
    {
        $entity = self::getEntity();

        self::assertEquals('Tester', $entity->getUsername());
        self::assertTrue($entity->isEnabled());
        self::assertTrue($entity->isVerified());
        self::assertTrue($entity->isEnableNotificationEmail());
        self::assertTrue($entity->getEnableNotificationEmail());
        self::assertFalse($entity->isOnline());
        
        self::assertEquals('Test User', $entity->getFullName());
        self::assertEquals('Tester', (string) $entity);
        self::assertNull($entity->getId());

        $entity->setOnlineAt(new \DateTime());
        self::assertEqualsWithDelta(new \DateTime(), $entity->getOnlineAt(), 10);
        self::assertTrue($entity->isOnline());
    }

    #[Test]
    public function validRoles(): void
    {
        $entity = self::getEntity();

        self::assertTrue($entity->hasRole('ROLE_TEST'));

        $entity->removeRole('ROLE_TEST');

        self::assertFalse($entity->hasRole('ROLE_TEST'));
    }

    #[Test]
    public function addAndRemoveGroup(): void
    {
        $entity = self::getEntity();

        $group = new Group();
        $group->setName('Test');

        $entity->addGroup($group);

        self::assertTrue($entity->hasGroup($group));

        $entity->removeGroup($group);

        self::assertFalse($entity->hasGroup($group));
    }

    #[Test]
    public function cannotDisableSuperAdmin(): void
    {
        $entity = self::getEntity();

        $entity->addRole('ROLE_SUPER_ADMIN');

        self::expectException(\LogicException::class);
        $entity->setEnabled(false); 
    }

    #[Test]
    public function cannotUnverifiedSuperAdmin(): void
    {
        $entity = self::getEntity();

        $entity->addRole('ROLE_SUPER_ADMIN');
        
        self::expectException(\LogicException::class);
        $entity->setVerified(false);
    }

    #[Test] 
    public function setSuperAdmin(): void
    {
        $entity = self::getEntity();

        $entity->setSuperAdmin(true);

        self::assertTrue($entity->isSuperAdmin());

        $entity->setSuperAdmin(false);

        self::assertFalse($entity->isSuperAdmin());
    }

    private static function getEntity(): User
    {
        $entity = new User();
        $entity
            ->setUsername('Tester')
            ->setEmail('test@example.com')
            ->setEnabled(true)
            ->setPassword('nopass_123')
            ->setRoles([User::ROLE_DEFAULT])
            ->addRole('ROLE_TEST')
            ->setSuperAdmin(false)
            ->setPlainPassword('nopass_123')
            ->setVerified(true)
            ->setEnableNotificationEmail(true)
            ->setLastLogin(new \DateTime('-1 minute'))
            ->setFullName('Test User')
        ;

        return $entity;
    }
}
