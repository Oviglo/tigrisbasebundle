<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity\Model;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Model\Group;
use Tigris\BaseBundle\Entity\Model\User;

/**
 * @internal
 */
#[CoversClass(Group::class)]
class GroupTest extends KernelTestCase
{
    #[Test]
    public function validEntity(): void
    {
        $entity = self::getEntity();

        self::assertEquals('User', $entity->getName());
        self::assertEquals('User', (string) $entity);
        self::assertContains('ROLE_SIMPLE_USER', $entity->getRoles());
        self::assertTrue($entity->hasRole('ROLE_USER'));

        $entity->removeRole('ROLE_USER');
        self::assertFalse($entity->hasRole('ROLE_USER'));
        self::assertNull($entity->getId());
    }

    private static function getEntity(): Group
    {
        $entity = new Group();
        $entity
            ->setName('User')
            ->setRoles(['ROLE_SIMPLE_USER'])
            ->addRole('ROLE_USER')    
        ;

        return $entity;
    }
}