<?php

namespace Tigris\BaseBundle\Test\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tigris\BaseBundle\Entity\DataExport;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;

#[CoversClass(DataExport::class)]
class DataExportTest extends KernelTestCase
{
    use EntityTestTrait;

    private static function createEntity(): DataExport
    {
        $entity = new DataExport();
        $entity
            ->setCreatedAt(new \DateTime('1 month ago'))
            ->setStatus(DataExport::STATUS_PENDING)
            ->setType('user')
            ->setCriteria(['exportType' => 'user'])
            ->setFormat('csv')
            ->setSendToMail('dummy@oviglo.fr')
            ->setFilePath('dummy.csv')
            ->setStatusLabel('pending')
            ->setFormatLabel('CSV')
            ->setTypeLabel('User')
        ;

        return $entity;
    }

    #[Test]
    public function valid(): void
    {
        $entity = self::createEntity();

        $this->validateEntity($entity, 0);

        self::assertEquals('dummy@oviglo.fr', $entity->getSendToMail());
        self::assertEquals('user', $entity->getType());
        self::assertEquals('csv', $entity->getFormat());
        self::assertEquals(DataExport::STATUS_PENDING, $entity->getStatus());
        self::assertEquals(['exportType' => 'user'], $entity->getCriteria());
        self::assertStringContainsString('data_export', $entity->getAbsoluteFolder());
        self::assertStringContainsString('data_export', $entity->getAbsolutePath());
        self::assertStringContainsString('dummy.csv', $entity->getFilePath());
        self::assertStringContainsString('pending', $entity->getStatusLabel());
        self::assertStringContainsString('CSV', $entity->getFormatLabel());
        self::assertStringContainsString('User', $entity->getTypeLabel());
        self::assertNull($entity->getId());
    }
}