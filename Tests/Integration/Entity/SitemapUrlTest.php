<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\SitemapUrl;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;

#[CoversClass(SitemapUrl::class)]
class SitemapUrlTest extends KernelTestCase
{
    use EntityTestTrait;

    private static function createEntity(): SitemapUrl
    {
        $entity = new SitemapUrl();
        $entity
            ->setLoc('http://www.oviglo.fr')
            ->setPriority(1)
            ->setLastMod(new \DateTime('1 month ago'))
        ;

        return $entity;
    }

    #[Test]
    public function valid(): void
    {
        $entity = self::createEntity();

        $this->validateEntity($entity, 0);
    }

    #[Test]
    public function unvalid(): void
    {
        $entity = self::createEntity();
        $entity
            ->setLastMod(new \DateTime('now + 1 day'))
            ->setPriority(2)
        ;

        $this->validateEntity($entity, 2);
    }

    #[Test]
    public function convertToString(): void
    {
        $entity = self::createEntity();

        self::assertEquals('http://www.oviglo.fr', (string) $entity);
    }
}
