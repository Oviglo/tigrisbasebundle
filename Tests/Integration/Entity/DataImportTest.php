<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\DataImport;
use Tigris\BaseBundle\Entity\DataImportField;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;

#[CoversClass(DataImport::class)]
class DataImportTest extends KernelTestCase
{
    use EntityTestTrait;

    public static function getEntity(): DataImport
    {
        return (new DataImport())
            ->setOptions(['test' => 'test'])
            ->setType('user')
            ->setTypeLabel('User')
            ->setStatus(DataImport::STATUS_PENDING)
            ->setStatusLabel('pending')
            ->setMapping(['test' => 'test'])
            ->setFields(new ArrayCollection(['test' => 'test']))
            ->setPrimaryKey('username')
        ;
    }

    #[Test]
    public function validEntity(): void
    {
        $entity = self::getEntity();

        $this->validateEntity($entity, 0);

        self::assertNull($entity->getId());
        self::assertEquals('user', $entity->getType());
        self::assertEquals('User', $entity->getTypeLabel());
        self::assertEquals(DataImport::STATUS_PENDING, $entity->getStatus());
        self::assertEquals('pending', $entity->getStatusLabel());
        self::assertEquals(['test' => 'test'], $entity->getOptions());
        self::assertEquals(['test' => 'test'], $entity->getMapping());
        self::assertEquals(['test' => 'test'], $entity->getFields()->toArray());
        self::assertEquals('username', $entity->getPrimaryKey());
        self::assertStringContainsString('data_import', $entity->getWebPath());
    }

    #[Test]
    public function addAndRemoveField(): void
    {
        $entity = self::getEntity();

        $field = new DataImportField();
        $field->setData(['test' => 'test']);

        $entity->addField($field);

        self::assertCount(2, $entity->getFields()->toArray());

        $entity->removeField($field);

        self::assertCount(1, $entity->getFields()->toArray());
    }


}