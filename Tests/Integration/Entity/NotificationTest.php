<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Notification;
use Tigris\BaseBundle\Notification\NotificationData;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;
use Tigris\BaseBundle\Tests\Helper\ReflectionTestTrait;

#[CoversClass(Notification::class)]
class NotificationTest extends KernelTestCase
{
    use EntityTestTrait;
    use ReflectionTestTrait;

    private static function createEntity(): Notification
    {
        $data = new NotificationData();
        $data
            ->setTitle('Test')
            ->setMessage('Message Notification')
        ;

        $entity = (new Notification())
            ->setData($data)
        ;

        return $entity;
    }

    #[Test]
    public function valid(): void
    {
        $entity = self::createEntity();

        $this->validateEntity($entity, 0);
    }
}
