<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Wording;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;

#[CoversClass(Wording::class)]
class WordingTest extends KernelTestCase
{
    use EntityTestTrait;

    private static function getEntity(): Wording
    {
        $entity = new Wording();
        $entity
            ->setPage('home')
            ->setName('main title')
            ->setType(Wording::TYPE_TEXT)
            ->setcontent('Bienvenue')
        ;

        return $entity;
    }

    #[Test]
    public function testValidEntity(): void
    {
        $entity = self::getEntity();

        self::assertCount(4, $entity::getTypes());

        $this->validateEntity($entity, 0);
    }

    #[Test]
    public function testGetters(): void
    {
        $entity = self::getEntity();

        self::assertEquals('home', $entity->getPage());
        self::assertEquals('main title', $entity->getName());
        self::assertEquals(Wording::TYPE_TEXT, $entity->getType());
        self::assertEquals('Bienvenue', $entity->getContent());
        self::assertEquals('Bienvenue', (string) $entity);
    }
}
