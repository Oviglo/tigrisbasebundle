<?php

namespace Tigris\BaseBundle\Tests\Integration\Entity;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Config;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;
use Tigris\BaseBundle\Tests\Helper\ReflectionTestTrait;
use Tigris\BaseBundle\Utils\Address;

class DummyClass implements \Stringable
{
    public function __toString(): string
    {
        return 'OK';
    }
}

#[CoversClass(Config::class)]
class ConfigTest extends KernelTestCase
{
    use EntityTestTrait;
    use ReflectionTestTrait;

    private static function createEntity(): Config
    {
        $entity = new Config();
        $entity
            ->setBundle('TigrisBaseBundle')
            ->setName('dummy')
            ->setValue('Test')
            ->setType('text')
        ;

        return $entity;
    }

    #[Test]
    public function valid(): void
    {
        $entity = self::createEntity();
        self::setProperty($entity, 'id', 12);

        $this->validateEntity($entity, 0);
        self::assertEquals(12, $entity->getId());
    }

    #[Test]
    #[DataProvider('formatValueProvider')]
    public function formatValue(mixed $value, mixed $expected): void
    {
        $entity = self::createEntity();
        $entity->setValue($value);

        self::assertEquals($entity->getValue(), $expected);
    }

    public static function formatValueProvider(): \Generator
    {
        yield [new \DateTime('2024-05-21 21:46:20'), '2024-05-21 21:46:20'];
        yield [123, 123];
        yield ['Test', 'Test'];

        $user1 = new User();
        self::setProperty($user1, 'id', 12);
        $user2 = new User();
        self::setProperty($user2, 'id', 46);
        $users = new ArrayCollection([$user1, $user2]);
        yield [$users, [12, 46]];

        $user = new User();
        self::setProperty($user, 'id', 58);
        yield [$user, 58];

        $obj = new Address();
        yield [$obj, ''];

        yield ['{"value":"test", "value2": "test 2"}', ['value' => 'test', 'value2' => 'test 2']];

        yield [['value' => 'test', 'value2' => 'test 2'], ['value' => 'test', 'value2' => 'test 2']];

        $obj = new DummyClass();
        yield [$obj, 'OK'];
    }
}
