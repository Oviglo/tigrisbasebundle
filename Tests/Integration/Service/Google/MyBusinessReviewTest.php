<?php

namespace Oviglo\TigrisBaseBundle\Tests\Integration\Service\Google;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Service\Google\MyBusinessReview;

/**
 * @internal
 */
#[CoversClass(MyBusinessReview::class)]
class MyBusinessReviewTest extends KernelTestCase
{
    #[Test]
    public function getReviews(): void
    {
        $client = $this->getService();
        $reviews = $client->getReviews('right_place_id');

        self::assertGreaterThanOrEqual(1, count($reviews));
    }

    #[Test]
    public function getRating(): void
    {
        $client = $this->getService();
        $rating = $client->getRating('right_place_id');

        self::assertGreaterThanOrEqual(1, count($rating));
    }

    #[Test]
    public function getWrongPlaceIdReviews(): void
    {
        $client = $this->getService();
        $reviews = $client->getReviews('wrong_place_id');

        self::assertEmpty($reviews);
    }

    #[Test]
    public function getWrongPlaceIdRating(): void
    {
        $client = $this->getService();
        $rating = $client->getRating('wrong_place_id');

        self::assertEmpty($rating);
    }

    private function getService(): MockObject&MyBusinessReview
    {
        $mockObject = $this->getMockBuilder(MyBusinessReview::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getReviews', 'getRating'])
            ->getMock()
        ;

        $mockObject
            ->method('getReviews')
            ->with($this->logicalOr('right_place_id', 'wrong_place_id'))
            ->willReturnCallback(
                function (string $value) {
                    if ('right_place_id' === $value) {
                        return [
                            [
                                'author_name' => 'John Doe',
                                'rating' => 5,
                                'text' => 'Great place!',
                            ],
                        ];
                    }

                    return [];
                }
            )
        ;

        $mockObject
            ->method('getRating')
            ->with($this->logicalOr('right_place_id', 'wrong_place_id'))
            ->willReturnCallback(
                function (string $value) {
                    if ('right_place_id' === $value) {
                        return [
                            'rating' => 4.5,
                            'total_reviews' => 100,
                        ];
                    }

                    return [];
                }
            )
        ;

        return $mockObject;
    }
}
