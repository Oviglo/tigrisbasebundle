<?php

namespace Tigris\BaseBundle\Tests\Integration\Service;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Service\WordingService;

#[CoversClass(WordingService::class)]
class WordingServiceTest extends KernelTestCase
{
    private WordingService $wordingService;

    public function setUp(): void
    {
        $this->wordingService = static::getContainer()->get(WordingService::class);
    }

    #[Test]
    public function getByPage(): void
    {
        $wordings = $this->wordingService->getByPage('home');

        self::assertGreaterThanOrEqual(2, count($wordings));
    }

    #[Test]
    public function content(): void
    {
        $wording = $this->wordingService->content('home', 'main title');

        self::assertEquals('Bienvenue sur Oviglo', $wording);

        $wording = $this->wordingService->content('home', 'unknow');

        self::assertEquals('', $wording);
    }

    #[Test]
    public function getFirstPage(): void
    {
        $pageName = $this->wordingService->getFirstPage();

        self::assertEquals('home', $pageName);
    }

    #[Test]
    public function getPages(): void
    {
        $pages = $this->wordingService->getPages();

        self::assertGreaterThanOrEqual(1, count($pages));
    }
}
