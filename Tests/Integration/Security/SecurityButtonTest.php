<?php

namespace Tigris\BaseBundle\Tests\Integration\Security;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage;
use Tigris\BaseBundle\Security\SecurityButton;

/**
 * @internal
 */
#[CoversClass(SecurityButton::class)]
class SecurityButtonTest extends WebTestCase
{
    private SecurityButton $securityButton;

    public function setUp(): void
    {
        $session = new Session(new MockFileSessionStorage());
        $request = new Request();
        $request->setSession($session);
        $stack = self::getContainer()->get(RequestStack::class);
        $stack->push($request);

        $this->securityButton = static::getContainer()->get(SecurityButton::class);
    }

    #[Test]
    public function generateToken(): void
    {
        $token = $this->securityButton->generateToken();

        self::assertNotEmpty($token);
    }

    #[Test]
    public function getToken(): void
    {
        $token = $this->securityButton->generateToken();

        self::assertEquals($token, $this->securityButton->getToken());
    }

    #[Test]
    public function isTokenValid(): void
    {
        $token = $this->securityButton->generateToken();

        self::assertTrue($this->securityButton->isTokenValid($token));
    }
}
