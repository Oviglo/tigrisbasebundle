<?php

namespace Tigris\BaseBundle\Tests\Integration\Validator;

use PHPUnit\Framework\Attributes\CoversClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Tigris\BaseBundle\Validator\StopForumSpamValidator;

#[CoversClass(StopForumSpamValidator::class)]
class StopForumSpamValidatorTest extends KernelTestCase
{
    private const VALID_EMAIL = 'contact@oviglo.fr';
    private const SPAM_EMAIL = 'spam@webolowitz.com';

    public function testNonSpamMail(): void
    {
        $validator = self::getContainer()->get(StopForumSpamValidator::class);

        self::assertTrue($validator->testEmail(self::VALID_EMAIL));
    }

    public function testSpamMail(): void
    {
        $validator = self::getContainer()->get(StopForumSpamValidator::class);

        self::assertFalse($validator->testEmail(self::SPAM_EMAIL));
    }

    /*public function testApiNotAvailable(): void
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willThrowException(new TransportException());
        $validator = new StopForumSpamValidator($httpClient, self::getContainer()->get('logger'));

        $validator->testEmail(self::VALID_EMAIL);
    }*/
}
