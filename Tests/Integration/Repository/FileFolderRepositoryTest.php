<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\FileFolder;
use Tigris\BaseBundle\Repository\FileFolderRepository;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(FileFolderRepository::class)]
class FileFolderRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var FileFolderRepository */
        $repository = $em->getRepository(FileFolder::class);

        $data = $repository->findData([]);

        self::assertGreaterThanOrEqual(0, count($data));
    }

    #[Test]
    public function findUserFolder(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var FileFolderRepository */
        $repository = $em->getRepository(FileFolder::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $data = $repository->findUserFolder($user);

        self::assertNotNull($data);
    }

    #[Test]
    public function getUserChildren(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var FileFolderRepository */
        $repository = $em->getRepository(FileFolder::class);

        $parent = self::getContainer()->get(FileFolderRepository::class)->findOneBy(['name' => 'Fichiers divers']);
        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $data = $repository->getUserChildren($parent, $user);

        self::assertCount(1, $data);
    }

    #[Test]
    public function getUserRootNodes(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var FileFolderRepository */
        $repository = $em->getRepository(FileFolder::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $data = $repository->getUserRootNodes($user);

        self::assertCount(3, $data);
    }
}
