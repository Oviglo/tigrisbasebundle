<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\DataImport;
use Tigris\BaseBundle\Repository\DataImportRepository;

#[CoversClass(DataImportRepository::class)]
class DataImportRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var DataImportRepository */
        $repository = $em->getRepository(DataImport::class);

        $data = $repository->findData([]);

        self::assertGreaterThanOrEqual(0, count($data));
    }

    #[Test]
    public function findPending(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var DataImportRepository */
        $repository = $em->getRepository(DataImport::class);

        $data = $repository->findPending();

        self::assertGreaterThanOrEqual(0, count($data));
    }
}
