<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Notification;
use Tigris\BaseBundle\Repository\NotificationRepository;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(NotificationRepository::class)]
class NotificationRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var NotificationRepository */
        $repository = $em->getRepository(Notification::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $data = $repository->findData(['user' => $user]);

        self::assertGreaterThan(0, count($data));
    }

    #[Test]
    public function findNew(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var NotificationRepository */
        $repository = $em->getRepository(Notification::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $data = $repository->findNew($user, new \DateTime());

        self::assertGreaterThanOrEqual(0, count($data));
    }

    #[Test]
    public function findUnread(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var NotificationRepository */
        $repository = $em->getRepository(Notification::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $data = $repository->findUnread($user);

        self::assertGreaterThan(0, count($data));
    }

    #[Test]
    public function unreadCount(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var NotificationRepository */
        $repository = $em->getRepository(Notification::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $data = $repository->unreadCount($user);

        self::assertGreaterThan(0, $data);
    }

    #[Test]
    public function findLast(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var NotificationRepository */
        $repository = $em->getRepository(Notification::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $data = $repository->findLast($user);

        self::assertGreaterThan(0, $data);
    }

    #[Test]
    public function findByEntity(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var NotificationRepository */
        $repository = $em->getRepository(Notification::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::USER_EMAIL]);

        $data = $repository->findByEntity($user::class, $user->getId());

        self::assertGreaterThan(0, $data);
    }

    #[Test]
    public function read(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var NotificationRepository */
        $repository = $em->getRepository(Notification::class);

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::ADMIN_EMAIL]);

        $repository->read($user);

        $data = $repository->findUnread($user);

        self::assertCount(0, $data);
    }
}
