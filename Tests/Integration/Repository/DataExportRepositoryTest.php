<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\DataExport;
use Tigris\BaseBundle\Repository\DataExportRepository;

#[CoversClass(DataExportRepository::class)]
class DataExportRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var DataExportRepository */
        $repository = $em->getRepository(DataExport::class);

        $data = $repository->findData([]);

        self::assertGreaterThanOrEqual(0, count($data));
    }
}
