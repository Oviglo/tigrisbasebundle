<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Config;
use Tigris\BaseBundle\Repository\ConfigRepository;

#[CoversClass(ConfigRepository::class)]
class ConfigRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var ConfigRepository */
        $repository = $em->getRepository(Config::class);

        $data = $repository->findData(0, 10);

        self::assertGreaterThan(0, count($data));
    }

    #[Test]
    public function getByName(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var ConfigRepository */
        $repository = $em->getRepository(Config::class);

        $data = $repository->getByName('TigrisBaseBundle.title');

        self::assertEquals('Tigris', $data->getValue());
    }
}
