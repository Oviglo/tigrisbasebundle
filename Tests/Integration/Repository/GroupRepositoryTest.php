<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Model\Group;
use Tigris\BaseBundle\Repository\GroupRepository;

#[CoversClass(GroupRepository::class)]
class GroupRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var GroupRepository */
        $repository = $em->getRepository(Group::class);

        $data = $repository->findData([]);

        self::assertGreaterThanOrEqual(0, count($data));
    }
}
