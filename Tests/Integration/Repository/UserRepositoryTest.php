<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Repository\UserRepository;

/**
 * @internal
 */
#[CoversClass(UserRepository::class)]
class UserRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var UserRepository */
        $repository = $em->getRepository(User::class);

        $data = $repository->findData(['search' => 'Asterix', 'type' => 'enabled']);

        self::assertGreaterThan(0, $data->count());
    }

    #[Test]
    public function findSuperAdmins(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var UserRepository */
        $repository = $em->getRepository(User::class);

        $data = $repository->findSuperAdmins();

        self::assertCount(1, $data);
    }

    #[Test]
    public function findOnline(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var UserRepository */
        $repository = $em->getRepository(User::class);

        $data = $repository->findOnline();

        self::assertCount(0, $data);
    }

    #[Test]
    public function findValidUser(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var UserRepository */
        $repository = $em->getRepository(User::class);

        $data = $repository->findValidUser();

        self::assertCount(6, $data);
    }

    #[Test]
    public function search(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var UserRepository */
        $repository = $em->getRepository(User::class);

        $data = $repository->search('asterix');

        self::assertCount(1, $data);
    }

    #[Test]
    public function dashboardData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var UserRepository */
        $repository = $em->getRepository(User::class);

        $data = $repository->dashboardData();

        self::assertGreaterThanOrEqual(5, $data['enabled']);
    }
}
