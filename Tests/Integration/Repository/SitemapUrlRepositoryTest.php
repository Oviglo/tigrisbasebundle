<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\SitemapUrl;
use Tigris\BaseBundle\Repository\SitemapUrlRepository;

#[CoversClass(SitemapUrlRepository::class)]
class SitemapUrlRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var SitemapUrlRepository */
        $repository = $em->getRepository(SitemapUrl::class);

        $data = $repository->findData([]);

        self::assertGreaterThanOrEqual(0, count($data));
    }
}
