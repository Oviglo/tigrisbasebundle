<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Repository\FileRepository;

#[CoversClass(FileRepository::class)]
class FileRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var FileRepository */
        $repository = $em->getRepository(File::class);

        $folder = self::getContainer()->get(FileRepository::class)->findOneBy([]);

        $data = $repository->findData(['folder' => $folder]);

        self::assertGreaterThanOrEqual(0, $data->count());
    }
}
