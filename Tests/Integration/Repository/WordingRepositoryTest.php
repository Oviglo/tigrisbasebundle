<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Wording;
use Tigris\BaseBundle\Repository\WordingRepository;

#[CoversClass(WordingRepository::class)]
class WordingRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findByPage(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var WordingRepository */
        $repository = $em->getRepository(Wording::class);

        $data = $repository->findByPage('home', true);

        self::assertGreaterThanOrEqual(2, count($data));
    }

    #[Test]
    public function findOne(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var WordingRepository */
        $repository = $em->getRepository(Wording::class);

        $data = $repository->findOne('home', 'main title');

        self::assertNotNull($data);
    }
}
