<?php

namespace Tigris\BaseBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\OnlineUser;
use Tigris\BaseBundle\Repository\OnlineUserRepository;

#[CoversClass(OnlineUserRepository::class)]
class OnlineUserRepositoryTest extends KernelTestCase
{
    #[Test]
    public function onlineCount(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var OnlineUserRepository */
        $repository = $em->getRepository(OnlineUser::class);

        $data = $repository->onlineCount();

        self::assertGreaterThanOrEqual(0, $data);
    }
}
