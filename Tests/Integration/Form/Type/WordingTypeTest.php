<?php

namespace Tigris\BaseBundle\Tests\Integration\Form\Type;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Tigris\BaseBundle\Entity\Wording;
use Tigris\BaseBundle\Form\Type\WordingType;
use Tigris\BaseBundle\Form\Type\WysiwygType;

#[CoversClass(WordingType::class)]
class WordingTypeTest extends KernelTestCase
{
    #[Test]
    #[DataProvider('fieldTypeProvider')]
    public function fieldType(Wording $entity, null|string $expected): void
    {
        /** @var FormFactoryInterface */
        $formFactory = static::getContainer()->get(FormFactoryInterface::class);

        $form = $formFactory->create(WordingType::class, $entity);

        if (null === $expected) {
            self::assertFalse($form->has('content'));
        } else {
            $field = $form->get('content')->getConfig()->getType()->getInnerType();

            self::assertInstanceOf($expected, $field);
        }
    }

    public static function fieldTypeProvider(): \Generator
    {
        $entity = (new Wording())
            ->setPage('home')
            ->setName('title')
            ->setType(Wording::TYPE_LONG_TEXT)
        ;

        yield [$entity, TextareaType::class];

        $entity = (new Wording())
            ->setPage('home')
            ->setName('title')
            ->setType(Wording::TYPE_TEXT)
        ;

        yield [$entity, TextType::class];

        $entity = (new Wording())
            ->setPage('home')
            ->setName('title')
            ->setType(Wording::TYPE_HTML)
        ;

        yield [$entity, WysiwygType::class];

        $entity = (new Wording())
            ->setPage('home')
            ->setName('title')
            ->setType(Wording::TYPE_NUMBER)
        ;

        yield [$entity, NumberType::class];

        $entity = (new Wording())
            ->setPage('home')
            ->setName('title')
            ->setType('nothing')
        ;

        yield [$entity, null];
    }
}
