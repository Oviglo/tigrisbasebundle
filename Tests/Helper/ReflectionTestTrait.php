<?php

namespace Tigris\BaseBundle\Tests\Helper;

trait ReflectionTestTrait
{
    protected static function setProperty(object $object, string $name, $value): \ReflectionProperty
    {
        $class = new \ReflectionClass($object);
        $property = $class->getProperty($name);
        $property->setAccessible(true);
        $property->setValue($object, $value);

        return $property;
    }
}
