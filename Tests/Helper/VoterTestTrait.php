<?php

namespace Tigris\BaseBundle\Tests\Helper;

use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Tigris\BaseBundle\Entity\Model\User;

trait VoterTestTrait
{
    abstract protected function createMock(string $originalClassName): MockObject;

    protected TokenInterface&MockObject $token;
    protected AuthorizationCheckerInterface&MockObject $authorizationChecker;

    private function createVoterMocks(): void
    {
        $this->token = $this->createMock(TokenInterface::class);
        $this->authorizationChecker = $this->createMock(AuthorizationCheckerInterface::class);
    }

    private function setAuthenticatedUser(null|User $user, array $baseRoles = ['ROLE_ADMIN', 'ROLE_USER']): void
    {
        if ($user instanceof User) {
            $this->token->method('getUser')->willReturn($user);
            $roleMap = [];
            foreach ($baseRoles as  $baseRole) {
                $roleMap[$baseRole] = [$baseRole, null, false];
            }
            
            foreach ($user->getRoles() as $role) {
                $roleMap[$role] = [$role, null, true];
            }

            $this->authorizationChecker->method('isGranted')->willReturnMap($roleMap);
        }
    }
}
