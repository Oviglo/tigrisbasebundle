<?php

namespace Tigris\BaseBundle\Tests\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait EntityTestTrait
{
    abstract public static function getContainer(): ContainerInterface;

    abstract public static function assertCount(int $expectedCount, \Countable|iterable $haystack, string $message = ''): void;

    /**
     * @return array<ConstraintViolation>
     */
    protected function validateEntity(object $entity, int $expectedErrorCount = 0)
    {
        $errors = self::getContainer()->get(ValidatorInterface::class)->validate($entity);
        self::assertCount($expectedErrorCount, $errors);
    }
}
