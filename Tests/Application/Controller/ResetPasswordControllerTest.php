<?php

namespace Tigris\BaseBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Controller\ResetPasswordController;
use Tigris\BaseBundle\Repository\ResetPasswordRequestRepository;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(ResetPasswordController::class)]
class ResetPasswordControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function request(): void
    {
        $this->client->request('GET', '/reset-password/');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function postRequest(): void
    {
        $crawler = $this->client->request('GET', '/reset-password/');
        $form = $crawler->selectButton('reset-request-form-submit')->form();

        $this->client->submit($form, [
            'reset_password_request_form[email]' => 'asterix@yopmail.com',
        ]);

        self::assertResponseRedirects();

        $this->client->followRedirect();
        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function reset(): void
    {
        $entity = self::getContainer()->get(ResetPasswordRequestRepository::class)->find(1);
        $this->client->request('GET', '/reset-password/reset/'.$entity->getHashedToken());

        self::assertResponseRedirects();
    }
}
