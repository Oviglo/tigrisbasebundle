<?php

namespace Tigris\BaseBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tigris\BaseBundle\Controller\SitemapController;

#[CoversClass(SitemapController::class)]
class SitemapControllerTest extends WebTestCase
{
    #[Test]
    public function index(): void
    {
        $client = static::createClient();
        $client->request('GET', '/sitemap.xml');

        self::assertResponseIsSuccessful();
    }
}