<?php

namespace Tigris\BaseBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tigris\BaseBundle\Controller\SecurityController;

#[CoversClass(SecurityController::class)]
class SecurityControllerTest extends WebTestCase
{
    #[Test]
    public function testLoginPage(): void
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/login');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    #[Test]
    public function testLoginRight(): void
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', '/login');

        $client->submitForm('login-form-submit', [
            'email' => 'asterix@yopmail.com',
            'password' => '01Azerty',
        ]);

        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());
    }

    #[Test]
    public function testLoginWrong(): void
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/login');
        $buttonCrawlerNode = $crawler->selectButton('login-form-submit');

        $form = $buttonCrawlerNode->form([
            'email' => 'asterix@yopmail.com',
            'password' => 'WrongPassword',
        ]);

        $responseCrawler = $client->submit($form);

        $this->assertEquals(1, $responseCrawler->filter('div.alert-danger')->count());
    }

    #[Test]
    public function testResetPassword(): void
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/reset-password');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $form = $crawler->filter('form[name="reset_password_request_form"]')->first()->form();
        $responseCrawler = $client->submit($form, [
            'reset_password_request_form[email]' => 'ovigne.loic@gmail.com',
        ]);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertEquals(1, $responseCrawler->filter('#password_request')->count());
    }
}
