<?php

namespace Tigris\BaseBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;
use Tigris\BaseBundle\Controller\RegistrationController;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Security\SecurityButton;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\BaseBundle\Tests\Helper\SessionHelper;

/**
 * @internal
 */
#[CoversClass(RegistrationController::class)]
class RegistrationControllerTest extends AbstractLoginWebTestCase
{
    use SessionHelper;

    public function setUp(): void
    {
        parent::setUp();
    }

    #[Test]
    public function register(): void
    {
        $session = $this->getSession($this->client);
        $crawler = $this->client->request('GET', '/register');

        self::assertResponseIsSuccessful();

        $form = $crawler->filter('form[name=registration_form]')->form();
        $session->set(SecurityButton::SESSION_NAME, bin2hex(random_bytes(32)));
        $session->save();

        $this->client->submit($form, [
            'registration_form[username]' => 'test',
            'registration_form[email]' => 'test@oviglo.fr',
            'registration_form[plainPassword][first]' => '01Azerty',
            'registration_form[plainPassword][second]' => '01Azerty',
            'registration_form[securityButton]' => $session->get(SecurityButton::SESSION_NAME),
        ]);

        self::assertResponseRedirects();

        $this->client->followRedirect();
        self::assertResponseIsSuccessful();
    }

    public function registrationDisabled(): void
    {
        self::getContainer()->setParameter('tigris_base.active_user_registration', false);
        $this->client->request('GET', '/register');

        self::assertResponseStatusCodeSame(404);
    }

    #[Test]
    public function verifyUserMailNotFound(): void
    {
        $this->client->request('GET', '/register/verify');

        $this->assertResponseStatusCodeSame(404);
    }

    #[Test]
    public function verifyUserMailAlreadyVerified(): void
    {
        $translator = self::getContainer()->get(TranslatorInterface::class);
        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['username' => 'Asterix']);
        $this->client->request('GET', '/register/verify?id='.$user->getId());

        self::assertResponseRedirects();
        $crawler = $this->client->followRedirect();

        $crawler->filter('.messages .alert')->each(function ($node) use ($translator) {
            self::assertStringContainsString($translator->trans('registration.already_verified'), $node->text());
        });
    }

    #[Test]
    public function verifyUserMailWithWrongUrl(): void
    {
        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['username' => 'Abraracourcix']);
        $this->client->request('GET', '/register/verify?id='.$user->getId());

        self::assertResponseRedirects();
        $crawler = $this->client->followRedirect();

        self::assertCount(1, $crawler->filter('.messages .alert-danger'));
    }

    #[Test]
    public function verifyUserMail(): void
    {
        $translator = self::getContainer()->get(TranslatorInterface::class);
        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['username' => 'Abraracourcix']);

        $verifyMailHelper = self::getContainer()->get(VerifyEmailHelperInterface::class);
        $signature = $verifyMailHelper->generateSignature('tigris_base_registration_verifyuseremail', $user->getId(), $user->getEmail(), ['id' => $user->getId()]);

        $this->client->request('GET', $signature->getSignedUrl());

        self::assertResponseRedirects();
        $crawler = $this->client->followRedirect();

        $crawler->filter('.messages .alert')->each(function ($node) use ($translator) {
            self::assertStringContainsString($translator->trans('registration.mail_verified'), $node->text());
        });
    }
}
