<?php

namespace Tigris\BaseBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Tigris\BaseBundle\Controller\FileController;
use Tigris\BaseBundle\Repository\FileRepository;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

/**
 * @internal
 */
#[CoversClass(FileController::class)]
class FileControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function denyAccessIndex(): void
    {
        $client = static::getClient();
        $client->request('GET', '/basefile/index');

        self::assertResponseRedirects();
    }

    #[Test]
    public function index(): void
    {
        $this->loginUser();
        $this->client->request('GET', '/basefile/index');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function list(): void
    {
        $this->loginUser();
        $this->client->request('GET', '/basefile/list');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function upload(): void
    {
        $this->loginUser();
        // Create a copy of file test.txt
        copy(__DIR__.'/../../data/test.txt', __DIR__.'/../../data/test_copy.txt');

        $uploadedFile = new UploadedFile(__DIR__.'/../../data/test_copy.txt', 'test.txt', 'text/plain', null, true);
        $crawler = $this->client->request('POST', '/basefile/upload', files: [
            'file' => $uploadedFile,
        ]);

        self::assertResponseIsSuccessful();

        $response = $this->client->getResponse();
        $content = $response->getContent();
        $json = json_decode($content, true);
        $path = $json['path'];

        // Remove the file
        $file = $this->client->getContainer()->get('kernel')->getProjectDir().'/public/'.$path;
        if (file_exists($file)) {
            unlink($file);
        }
    }

    #[Test]
    public function edit(): void
    {
        $this->loginUser();
        $fileRepository = $this->client->getContainer()->get(FileRepository::class);
        $file = $fileRepository->findOneBy([]);
        $this->client->request('GET', '/basefile/'.$file->getId().'/edit');

        self::assertResponseIsSuccessful();
    }
}
