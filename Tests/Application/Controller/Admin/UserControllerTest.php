<?php

namespace Tigris\BaseBundle\Tests\Application\Controller\Admin;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\HttpFoundation\Response;
use Tigris\BaseBundle\Controller\Admin\UserController;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(UserController::class)]
class UserControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function accessDenied(): void
    {
        $this->loginUser();

        $this->client->request('GET', '/admin/user/');

        self::assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    #[Test]
    public function index(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/admin/user/');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function data(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/admin/user/data');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function new(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/admin/user/new');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('user_actions_save')->form();

        $this->client->submit($form, [
            'user[username]' => 'user_test',
            'user[plainPassword]' => 'password',
            'user[email]' => 'test@example.com',
        ]);

        self::assertResponseRedirects();
    }

    #[Test]
    public function newInvalid(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/admin/user/new');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('user_actions_save')->form();

        $this->client->submit($form, [
            'user[username]' => 'user_test',
            'user[plainPassword]' => 'password',
            'user[email]' => '',
        ]);

        self::assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    #[Test]
    public function edit(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/admin/user/1/edit');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('user_actions_save')->form();

        $this->client->submit($form, [
            'user[username]' => 'user_test_edit',
        ]);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(UserRepository::class)->findOneBy(['username' => 'user_test_edit']);

        self::assertNotNull($entity);
    }

    #[Test]
    public function editInvalid(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/admin/user/1/edit');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('user_actions_save')->form();

        $this->client->submit($form, [
            'user[email]' => 'wrongmail',
        ]);

        self::assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    #[Test]
    public function delete(): void
    {
        $this->loginAdmin();
        $crawler = $this->client->request('GET', '/admin/user/1/remove');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();

        $this->client->submit($form);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(UserRepository::class)->find(1);

        self::assertNull($entity);
    }
}
