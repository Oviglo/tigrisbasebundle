<?php

namespace Tigris\BaseBundle\Tests\Application\Controller\Admin;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Controller\Admin\WordingController;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(WordingController::class)]
class WordingControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/admin/wording/');

        self::assertResponseIsSuccessful();
    }
}
