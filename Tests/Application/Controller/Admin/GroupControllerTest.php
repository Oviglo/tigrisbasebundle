<?php

namespace Tigris\CalendarBundle\Tests\Application\Controller\Admin;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Controller\Admin\GroupController;
use Tigris\BaseBundle\Repository\GroupRepository;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(GroupController::class)]
class GroupControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/admin/group/');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function data(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/admin/group/data');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function new(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/admin/group/new');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('group_actions_save')->form();

        $this->client->submit($form, [
            'group[name]' => 'Group test',
        ]);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(GroupRepository::class)->findOneBy(['name' => 'Group test']);

        self::assertNotNull($entity);
    }

    #[Test]
    public function edit(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/admin/group/1/edit');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('group_actions_save')->form();

        $this->client->submit($form, [
            'group[name]' => 'Group test',
        ]);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(GroupRepository::class)->find(1);

        self::assertEquals('Group test', $entity->getName());
    }
}
