<?php

namespace Tigris\BaseBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Controller\Admin\UICommandController;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(UICommandController::class)]
class UICommandControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/admin/ui-command/');

        self::assertResponseIsSuccessful();
    }
}
