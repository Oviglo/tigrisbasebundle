<?php

namespace Tigris\BaseBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Tigris\BaseBundle\Controller\UserController;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(UserController::class)]
class UserControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function search(): void
    {
        $this->login('asterix@yopmail.com');

        $this->client->request('GET', '/user/search/asterix');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function myAccount(): void
    {
        $this->login('asterix@yopmail.com');
        $this->client->request('GET', '/user/my-account');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function myAccountWithoutLogin(): void
    {
        $this->login('asterix@yopmail.com');
        
        $this->client->request('GET', '/user/my-account?user=999');

        self::assertResponseRedirects();
    }

    #[Test]
    public function myAccountEmail(): void
    {
        $this->login('asterix@yopmail.com');
        $crawler = $this->client->request('GET', '/user/my-account/email');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('Enregistrer')->form();
        $this->client->submit($form, [
            'user_email[email]' => 'asterix_up@yopmail.com',
            'user_email[plainPassword]' => '01Azerty',
        ]);

        self::assertResponseRedirects();
    }

    #[Test]
    public function myAccountEmailWithWrongPassword(): void
    {
        $this->login('asterix@yopmail.com');
        $crawler = $this->client->request('GET', '/user/my-account/email');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('Enregistrer')->form();
        $this->client->submit($form, [
            'user_email[email]' => 'asterix_up@yopmail.com',
            'user_email[plainPassword]' => 'wrongPassword',
        ]);

        self::assertResponseStatusCodeSame(422);
    }

    #[Test]
    public function profile(): void
    {
        $user = $this->login('asterix@yopmail.com');
        $this->client->request('GET', '/user/'. $user->getUsername());

        self::assertResponseIsSuccessful();
    }
}
