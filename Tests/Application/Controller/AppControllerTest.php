<?php
namespace Tigris\BaseBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tigris\BaseBundle\Controller\AppController;

#[CoversClass(AppController::class)]
class AppControllerTest extends WebTestCase
{
    #[Test]
    public function securityToken(): void
    {
        $client = static::createClient();
        $client->request('GET', '/app/security-token');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function switchTheme(): void
    {
        $client = static::createClient();
        $client->request('GET', '/app/switch-theme');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function cron(): void
    {
        $client = static::createClient();
        $client->request('GET', '/app/cron');

        self::assertResponseIsSuccessful();
    }

    #[Test] 
    public function ajaxCron(): void
    {
        $client = static::createClient();
        $client->request('GET', '/app/ajax/cron');

        self::assertResponseIsSuccessful();
    }
}