<?php

namespace Tigris\BaseBundle\Tests;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tigris\BaseBundle\Entity\Model\User as ModelUser;
use Tigris\BaseBundle\Repository\UserRepository;

abstract class AbstractLoginWebTestCase extends WebTestCase
{
    public const ADMIN_EMAIL = 'ovigne.loic@gmail.com';
    public const USER_EMAIL = 'asterix@yopmail.com';

    protected KernelBrowser $client;
    protected ContainerInterface $container;
    protected EntityManagerInterface $em;

    protected function setUp(): void
    {
        self::ensureKernelShutdown();
        $this->client = self::createClient();
        $this->container = $this->client->getContainer();
        $this->em = $this->container->get(EntityManagerInterface::class);

        parent::setUp();
    }

    protected function login(string $email): null|ModelUser
    {
        $userRepository = $this->client->getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail($email);
        $this->client->loginUser($testUser);

        return $testUser;
    }

    protected function getUser(string $email): User
    {
        $userRepository = $this->client->getContainer()->get(UserRepository::class);

        return $userRepository->findOneByEmail($email);
    }

    protected function loginAdmin(): null|ModelUser
    {
        return $this->login(self::ADMIN_EMAIL);
    }

    protected function loginUser(): null|ModelUser
    {
        return $this->login(self::USER_EMAIL);
    }
}
