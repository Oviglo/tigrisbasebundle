<?php

namespace Tigris\BaseBundle\Tests\Units\Search;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tigris\BaseBundle\Search\SearchResult;

#[CoversClass(SearchResult::class)]
class SearchResultTest extends TestCase
{
    #[Test]
    public function constructor(): void
    {
        $searchResult = new SearchResult('Test', 'This is a test', 'tigris_base_user_show', ['id' => 1]);

        self::assertCount(6, $searchResult->toArray());
    }

    #[Test]
    #[DataProvider('provideData')]
    public function toArray(SearchResult $searchResult, array $expected): void
    {
        $resultArray = $searchResult->toArray();

        foreach ($expected as $key => $value) {
            self::assertArrayHasKey($key, $resultArray);
            self::assertEquals($value, $resultArray[$key]);
        }
    }

    public static function provideData(): \Iterator
    {
        yield [new SearchResult('Test', 'This is a test', 'tigris_base_user_show', ['id' => 1]), [
            'title' => 'Test',
            'content' => 'This is a test',
            'route' => 'tigris_base_user_show',
            'routeParams' => ['id' => 1],
        ]];
        
        yield [new SearchResult('Test 2   ', 'This is a test 2', 'tigris_base_user_show   ', ['id' => 2]), [
            'title' => 'Test 2',
            'content' => 'This is a test 2',
            'route' => 'tigris_base_user_show',
            'routeParams' => ['id' => 2],
        ]];

        yield [new SearchResult('Test 3', 'This is a test 3', 'tigris_base_user_show', ['id' => 3]), [
            'title' => 'Test 3',
            'content' => 'This is a test 3',
            'route' => 'tigris_base_user_show',
            'routeParams' => ['id' => 3],
        ]];
    }
}