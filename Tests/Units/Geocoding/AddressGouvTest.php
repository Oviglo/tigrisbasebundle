<?php

namespace Tests\Geocoding;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;
use Tigris\BaseBundle\Service\Geocoding\AddressGouv;

#[CoversClass(AddressGouv::class)]
class AddressGouvTest extends TestCase
{
    #[DataProvider('addressProvider')]
    public function testSearchAddress(string $address, int $expectedResultCount): void
    {
        $service = new AddressGouv(HttpClient::create());

        $response = $service->searchAddress($address);

        static::assertEquals($expectedResultCount, count($response->addresses));
    }

    public static function addressProvider(): \Iterator
    {
        yield ['88100', 5];
        yield ['05120', 3];
        yield ['Rue charles de gaulle', 2];
    }
}
