<?php

namespace Tigris\BaseBundle\Tests\Units\Security\Voter;

use Tigris\BaseBundle\Entity\File;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Security\Voter\FileVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use App\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[CoversClass(FileVoter::class)]
class FileVoterTest extends TestCase
{
    protected TokenInterface|MockObject $token;
    protected AuthorizationCheckerInterface $authorizationChecker;

    protected function setUp(): void
    {
        $this->token = $this->createMock(TokenInterface::class);
    }

    public static function getTests(): \Iterator
    {
        yield [new File(), [FileVoter::VIEW], Voter::ACCESS_GRANTED, [], "", true, "Pour le moment il est toujours possible de voir un fichier"];

        yield [new File(), [FileVoter::EDIT], Voter::ACCESS_DENIED, [], "", false, "On ne peut pas modifier un fichier d'un autre utilisateur"];
        yield [new File(), [FileVoter::EDIT], Voter::ACCESS_GRANTED, ['ROLE_ADMIN'], "", true, "Un ROLE_ADMIN peut modifier n'importe quel fichier"];
        yield [new File(), [FileVoter::EDIT], Voter::ACCESS_GRANTED, ['ROLE_FILE_ADMIN'], "", true, "Un role ROLE_FILE_ADMIN peut modifier n'importe quel fichier"];
        yield [new File(), [FileVoter::EDIT], Voter::ACCESS_DENIED, ['ROLE_USER'], "", false, "Un role ROLE_USER ne peut pas modifier n'importe quel fichier"];
        yield [new File(), [FileVoter::EDIT], Voter::ACCESS_GRANTED, ['ROLE_USER'], "", true, "Un role ROLE_USER peut pas modifier ses propres fichiers"];

        yield [new File(), [FileVoter::UPLOAD], Voter::ACCESS_DENIED, ['ROLE_USER'], "text/x-php", true, "Inderdit d'uploader un fichier php"];
        yield [new File(), [FileVoter::UPLOAD], Voter::ACCESS_DENIED, ['ROLE_ADMIN'], "text/x-php", true, "Inderdit d'uploader un fichier php même pour un admin"];
        yield [new File(), [FileVoter::UPLOAD], Voter::ACCESS_DENIED, ['ROLE_USER'], "text/html", true, "Inderdit d'uploader un fichier html"];
        yield [new File(), [FileVoter::UPLOAD], Voter::ACCESS_GRANTED, ['ROLE_USER'], "image/jpeg", true, "Les fichiers jpeg sont autorisés"];
        yield [new File(), [FileVoter::UPLOAD], Voter::ACCESS_GRANTED, ['ROLE_USER'], 'video/x-msvideo', true, "Les fichiers vidéo sont autorisés"];
        yield [new File(), [FileVoter::UPLOAD], Voter::ACCESS_DENIED, [], 'video/x-msvideo', false, "Les fichiers vidéo ne sont autorisés pour les non connectés"];
        yield [new File(), [FileVoter::UPLOAD], Voter::ACCESS_DENIED, [], 'application/octet-stream', false, "Les fichiers exe ne sont autorisés pour les non connectés"];
        yield [new File(), [FileVoter::UPLOAD], Voter::ACCESS_GRANTED, [], 'image/jpeg', false, "Les fichiers jpeg sont autorisés pour les non connectés"];
    }

    #[DataProvider('getTests')]
    public function testVote(object $subject, array $attributes, int $expectedVote, array $roles, string $mimeType, bool $connected, string $message): void
    {
        if ($connected) {
            $user = (new User())->setUsername('TokenUser');
            $this->token->method('getUser')->willReturn($user);
            $subject->setUser($user);
        } else {
            $this->token->method('getUser')->willReturn(null);
        }

        /** @var MockObject|AuthorizationCheckerInterface */
        $authorizationChecker = $this->createMock(AuthorizationCheckerInterface::class);

        $roleMap = [
            'ROLE_ADMIN' => ['ROLE_ADMIN', null, false],
            'ROLE_FILE_ADMIN' => ['ROLE_FILE_ADMIN', null, false],
        ];
        foreach ($roles as $role) {
            $roleMap[$role] = [$role, null, true];
        }

        $authorizationChecker->method('isGranted')->willReturnMap($roleMap);

        if ($mimeType !== "") {
            /** @var MockObject|UploadedFile */
            $uploadedFile = $this->createMock(UploadedFile::class);
            $uploadedFile->method('getMimeType')->willReturn($mimeType);

            $subject->setFile($uploadedFile);
        }

        $voter = new FileVoter($authorizationChecker);
        self::assertEquals($expectedVote, $voter->vote($this->token, $subject, $attributes), $message);
    }
}
