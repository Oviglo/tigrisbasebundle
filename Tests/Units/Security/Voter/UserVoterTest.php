<?php

namespace Tigris\BaseBundle\Tests\Units\Security\Voter;

use App\Entity\User;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Security\Voter\UserVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use PHPUnit\Framework\MockObject\MockObject;

#[CoversClass(UserVoter::class)]
class UserVoterTest extends TestCase
{
    protected TokenInterface $token;
    protected AuthorizationCheckerInterface $authorizationChecker;

    protected function setUp(): void
    {
        /** @var MockObject */
        $this->token = $this->createMock(TokenInterface::class);
        $this->token->method('getUser')->willReturn((new User())->setUsername('TokenUser'));
    }

    public static function getTests(): \Iterator
    {
        yield [new User, [UserVoter::EDIT], Voter::ACCESS_DENIED, false, "Un NON ADMIN ne peu pas modifier un utilisateur"];
        yield [new User, [UserVoter::VIEW], Voter::ACCESS_GRANTED, false, "Il est possible de voir un utilisateur activé par défaut"];
        $user = (new User)->setEnabled(false);
        yield [$user, [UserVoter::VIEW], Voter::ACCESS_DENIED, false, "Il est n'est pas possible pour un non admin de voir un utilisateur non activé"];

        yield [new User, [UserVoter::EDIT], Voter::ACCESS_GRANTED, true, "Il est possible de modifier un utilisateur en tant qu'administrateur"];
        $user = (new User)->setEnabled(false);
        yield [$user, [UserVoter::VIEW], Voter::ACCESS_GRANTED, true, "Il est possible de voir un utilisateur non activé en tant qu'administrateur"];
    }

    #[DataProvider('getTests')]
    public function testVote(object $subject, array $attributes, int $expectedVote, bool $isAdmin, string $message): void
    {
        /** @var MockObject|AuthorizationCheckerInterface */
        $authorizationChecker = $this->createMock(AuthorizationCheckerInterface::class);

        if ($isAdmin) {
            $authorizationChecker->method('isGranted')->willReturn(true);
        }
        $voter = new UserVoter($authorizationChecker);
        self::assertEquals($expectedVote, $voter->vote($this->token, $subject, $attributes), $message);
    }
}
