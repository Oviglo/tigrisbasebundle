<?php

namespace Tigris\BaseBundle\Tests\Utils;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tigris\BaseBundle\Utils\Utils;

#[CoversClass(Utils::class)]
class UtilsTest extends TestCase
{
    #[DataProvider('sheetAlphaFromNumberProvider')]
    public function testGetSheetAlphaFromNumber(int $value, string $expected): void
    {
        $result = Utils::getSheetAlphaFromNumber($value);
        $this->assertEquals($expected, $result);
    }

    public static function sheetAlphaFromNumberProvider(): \Iterator
    {
        yield [0, 'A'];
        yield [25, 'Z'];
        yield [27, 'AB'];
        yield [125, 'DV'];
    }

    #[DataProvider('lettersToNumProvider')]
    public function testLettersToNum(string $letters, int $expected): void
    {
        $result = Utils::lettersToNum($letters);
        $this->assertEquals($expected, $result);
    }

    public static function lettersToNumProvider(): \Iterator
    {
        yield ['A', 1];
        yield ['E', 5];
        yield ['AB', 28];
        yield ['DV', 126];
    }

    #[DataProvider('slugifyProvider')]
    public function testSlugify(string $text, string $slug): void
    {
        $result = Utils::slugify($text);

        $this->assertEquals($slug, $result);
    }

    public static function slugifyProvider(): \Iterator
    {
        yield ['test Slug 1', 'test-slug-1'];
        yield ['premier élément ', 'premier-element'];
        yield ['s#l)u@g"t\e[s/t', 's-l-u-g-t-e-s-t'];
    }

    #[DataProvider('distanceProvider')]
    public function testDistance(float $fromLat, float $fromLng, float $toLat, float $toLng, float $expectedDistance): void
    {
        $result = Utils::distance($fromLat, $fromLng, $toLat, $toLng);

        static::assertEquals($expectedDistance, $result);
    }

    public static function distanceProvider(): \Iterator
    {
        // From Disneyland to Europapark 366.84Km
        yield [48.8562795, 2.8136378, 48.2671566, 7.7196321, 366.84];
    }

    #[DataProvider('extractUrlProvider')]
    public function testExtractUrl(string $text, array $expectedUrls): void
    {
        $urls = Utils::extractUrls($text);

        foreach ($expectedUrls as $key => $url) {
            $this->assertEquals($url, $urls[$key] ?? null);
        }
    }

    public static function extractUrlProvider(): \Iterator
    {
        yield ['https://www.google.com/', ['https://www.google.com/']];
        yield ['gotohttps://my.nintendo.com/', ['https://my.nintendo.com/']];
        yield ['Hello from here: https://www.oviglo.fr, my favorite site is "https://regex101.com/"', ['https://www.oviglo.fr', 'https://regex101.com/']];
        yield ['I use http or https but not www', [null]];
        yield ['Salut.Comment ça va ?', [null]];
    }

    #[Test]
    #[DataProvider('stripAccentProvider')]
    public function stripAccent(string $content, string $expected): void
    {
        self::assertEquals($expected, Utils::stripAccent($content));
    }

    public static function stripAccentProvider(): \Generator
    {
        yield ['é', 'e'];
        yield ['à', 'a'];
        yield ['Bonjour à tous', 'Bonjour a tous'];
        yield ['àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'];
    }

    #[Test]
    #[DataProvider('underscoreProvider')]
    public function underscore(string $content, string $expected): void
    {
        self::assertEquals($expected, Utils::underscore($content));
    }

    public static function underscoreProvider(): \Generator
    {
        yield ['test', 'test'];
        yield ['test text', 'test_text'];
        yield ['testText', 'test_text'];
        yield ['bonjourATous', 'bonjour_a_tous'];
    }

    #[Test]
    #[DataProvider('truncateHtmlProvider')]
    public function truncateHtml(string $content, int $length, string $ending, string $expected): void
    {
        self::assertEquals($expected, Utils::truncateHtml($content, $length, $ending));
    }

    public static function truncateHtmlProvider(): \Generator
    {
        yield ['<p>Message</p>', 100, '...', '<p>Message</p>'];
        yield ['Lorem ipsum dolor sit amet. Id consequatur enim et explicabo voluptate ut quisquam labore eos veritatis quia At assumenda incidunt.', 50, '...', 'Lorem ipsum dolor sit amet. Id consequatur enim et...'];
    }

    #[Test]
    #[DataProvider('arrayFlattenProvider')]
    public function arrayFlatten(array $source, array $expected): void
    {
        self::assertEquals($expected, Utils::arrayFlatten($source));
    }

    public static function arrayFlattenProvider(): \Generator
    {
        yield [['first' => 'first', 'seconde' => 'second'], ['first' => 'first', 'seconde' => 'second']];
        yield [
            [
                'first' => [
                    'sub1' => 'sub1',
                ],
                'second' => [
                    'sub1' => 'sub1',
                    'sub2' => 'sub2',
                ],
            ],
            [
                'sub1' => 'sub1',
                'sub2' => 'sub2',
            ],
        ];
    }

    #[Test]
    #[DataProvider('formatUrlProvider')]
    public function formatUrl(string $source, string $expected): void
    {
        self::assertEquals($expected, Utils::formatUrl($source));
    }

    public static function formatUrlProvider(): \Generator
    {
        yield ['Test message without link', 'Test message without link'];
        yield ['Bonjour.à.tous', 'Bonjour.à.tous'];
        yield [
            'Go to the https://www.oviglo.fr website',
            'Go to the <a href="https://www.oviglo.fr" target="_blank" title="https://www.oviglo.fr">https://www.oviglo.fr</a> website',
        ];

        yield [
            'Go to the https://www.oviglo.fr website or https://www.google.com',
            'Go to the <a href="https://www.oviglo.fr" target="_blank" title="https://www.oviglo.fr">https://www.oviglo.fr</a> website or <a href="https://www.google.com" target="_blank" title="https://www.google.com">https://www.google.com</a>',
        ];
    }

    #[Test]
    #[DataProvider('removeStyleAttrProvider')]
    public function removeStyleAttr(string $source, string $expected): void
    {
        self::assertEquals($expected, Utils::removeStyleAttr($source));
    }

    public static function removeStyleAttrProvider(): \Generator
    {
        yield ['Test message without link', 'Test message without link'];
        yield ['<div style="color: red;">Red text</div>', '<div>Red text</div>'];
    }

    #[Test]
    #[DataProvider('HTMLToRGBProvider')]
    public function HTMLToRGB(string $source, int $expected): void
    {
        self::assertEquals($expected, Utils::HTMLToRGB($source));
    }

    public static function HTMLToRGBProvider(): \Generator
    {
        yield ['#FF0000', 16711680];
        yield ['#4CB276', 5026422];
    }

    #[Test]
    #[DataProvider('RGBToHSLProvider')]
    public function RGBToHSL(int $source, object $expected): void
    {
        self::assertEquals($expected, Utils::RGBToHSL($source));
    }

    public static function RGBToHSLProvider(): \Generator
    {
        yield [Utils::HTMLToRGB('#FF0000'), (object) ['hue' => 0, 'saturation' => 255, 'lightness' => 128]];
        yield [Utils::HTMLToRGB('#6BAF58'), (object) ['hue' => 76, 'saturation' => 90, 'lightness' => 132]];
    }
}
