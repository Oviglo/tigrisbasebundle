<?php

namespace Oviglo\TigrisBaseBundle\Tests\Units\Reader;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tigris\BaseBundle\Factory\PhpSpreadsheetFactory;
use Tigris\BaseBundle\Reader\CsvReader;
use Tigris\BaseBundle\Reader\FileReaders;
use Tigris\BaseBundle\Reader\XlsReader;

#[CoversClass(FileReaders::class)]
class FileReadersTest extends TestCase
{
    #[Test]
    public function addReader(): void
    {
        $reader = new XlsReader(new PhpSpreadsheetFactory());
        $fileReaders = new FileReaders();
        $fileReaders->addReader($reader, 'xls');

        self::assertSame($reader, $fileReaders->getReader('xls'));
    }

    #[Test]
    public function getXlsReader(): void
    {
        $reader = new XlsReader(new PhpSpreadsheetFactory());
        $fileReaders = new FileReaders();
        $fileReaders->addReader($reader, 'xls');

        self::assertNull($fileReaders->getReader('xlsx'));
    }

    #[Test]
    public function getNullReader(): void
    {
        $fileReaders = new FileReaders();

        self::assertNull($fileReaders->getReader('xls'));
    }

    #[Test]
    public function getCSVReader(): void
    {
        $reader = new CsvReader(new PhpSpreadsheetFactory());
        $fileReaders = new FileReaders();
        $fileReaders->addReader($reader, 'csv');

        self::assertSame($reader, $fileReaders->getReader('csv'));
    }
}
