<?php

namespace Tigris\BaseBundle\Tests\Units\Notification;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tigris\BaseBundle\Notification\NotificationBuilder;
use Tigris\BaseBundle\Notification\NotificationBuilderInterface;

class NotificationBuilderTest extends TestCase
{
    #[Test]
    public function createBuilder(): void
    {
        $builder = new NotificationBuilder();
        $builder->setTitle('title');
        $builder->setMessage('message');
        $builder->setMessageParams(['param1' => 'value1']);

        $this->assertInstanceOf(NotificationBuilderInterface::class, $builder);
        $this->assertEquals('title', $builder->getTitle());
        $this->assertEquals('message', $builder->getMessage());
        $this->assertEquals(['param1' => 'value1'], $builder->getMessageParams());
    }
}
