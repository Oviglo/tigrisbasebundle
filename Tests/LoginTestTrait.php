<?php

namespace Tigris\BaseBundle\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Repository\UserRepository;

trait LoginTestTrait
{
    protected function login(string $email): KernelBrowser
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail($email);

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        return $client;
    }

    protected function getUser(string $email): User
    {
        $userRepository = static::$container->get(UserRepository::class);

        return $userRepository->findOneByEmail($email);
    }
}
